//sub of two ptr

#include <stdio.h>

void main(){

	int arr[5] = {10,20,30,40,50};

	int *iptr1 = &arr[0];
	int *iptr2 = &arr[3];

	printf("%d\n",*iptr1);
	printf("%d\n",*iptr2);

	printf("%ld\n",iptr1-iptr2);
	printf("%ld\n",iptr2-iptr1);         //add - add / DTP
}
