//syntax type of 2D array

#include <stdio.h>

void main(){

	int arr[][3] = {{1,2,3},{4},{5,6}};

	printf("%d\n",*(*(arr+0)+2));

	int arr1[][3] = {1,2,3,4,{5,6}};
}
