//ptr to an arr of array

#include <stdio.h>

void main(){

	int arr1[] = {1,2,3,4,5};

	int arr2[] = {6,7,8,9,10};

	int (*ptr3[2])[5] = {&arr1,&arr2};

	printf("%d\n",(*ptr3[0])[0]);   //accessing array element
}
