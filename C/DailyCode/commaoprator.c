#include <stdio.h>

void main(){

//	int x = 10,20,30;        error becouse () are not given
//	printf("%d\n",x);

	int y = {10,20,30};   // warning
	printf("%d\n",y);     //10 

	int z = (10,20,30);       // perfect comma oprator
	printf("%d\n",z);        //30
}
