//ptr to arr

#include <stdio.h>

void main(){

	int arr[] = {10,20,30,40,50};
	int arr1[] = {60,70,80,90,100};

	int *ptr = arr+1;
	int *ptr1 = &arr+1;

	ptr++;
	ptr1++;

	printf("%d\n",*ptr);
	printf("%d\n",*ptr1);
}
