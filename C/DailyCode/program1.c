#include <stdio.h>

void main(){
	int age = 21;
	float petrol = 105.3;
	double gold = 10.43261723;
	char c = 'A';

	printf("%d\n", age);
	printf("%f\n", petrol);
	printf("%lf\n", gold);
	printf("%c\n", c);

	printf("%ld\n", sizeof(int));
	printf("%ld\n", sizeof(float));
	printf("%ld\n", sizeof(double));
	printf("%ld\n", sizeof(char));
	printf("%ld\n", sizeof(void));


}
