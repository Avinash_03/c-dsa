
#include <stdio.h>

void main(){

	char ch = 'X';
	char ch2 = 'Y';
	char ch3 = 'Z';

	char *cptr1 = &ch, *cptr2 = &ch2, *cptr3 = &ch3;

	printf("%p\n",cptr1);
	printf("%c\n",*cptr1);
	printf("%p\n",cptr2);
	printf("%c\n",*cptr2);
	printf("%p\n",cptr3);
	printf("%c\n",*cptr3);

}
