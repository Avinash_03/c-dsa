#include <stdio.h>

void main(){

	int x = 8;
	int y = 17;
	int ans;

	ans = ++x + x++ + ++y + y++;
	printf("%d\n",ans);    //55
	printf("%d\n",x);      //10
	printf("%d\n",y);      //19
}
