// pre and post oprator on ptr

#include <stdio.h>

void main(){

	int arr[5] = {1,2,3,4,5};
	
	int *ptr = &arr[0];

	printf("%d\n",*(ptr++)); //1
	printf("%d\n",*(++ptr)); //3
}
