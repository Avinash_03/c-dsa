// assigning an array to array

#include <stdio.h>

void main(){

	int iarr[5] = {1,2,3,4,5};
	int iarr1[5];

	//iarr = iarr1; we can not assign add to add array name means that it's first element add
	for(int i = 0; i < 5; i++){

		iarr1[i]=iarr[i];
		printf("%d\n",iarr1[i]);
	}
}
