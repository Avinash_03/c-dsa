#include <stdio.h>

void main(){

	char arr[3]={'A','B','C'};

	char *ptr1 = arr;
	char *ptr2 = &arr[1];
	char (*ptr3)[3] = &arr;

	printf("%c\n",**ptr3);

	char arr2[3] = {'D','E','F'};
	ptr3++;
	printf("%c\n",**ptr3);


}
