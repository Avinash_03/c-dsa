#include <stdio.h>

void main(){

	int x = 10;
	int y = 20;
	int ans;

	ans = x++ + y++;
	printf("%d\n",ans);    //30
	printf("%d\n",x);      //11
	printf("%d\n",y);      //21
}
