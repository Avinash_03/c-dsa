// pointer and integer add

#include <stdio.h>

void main(){

	int x = 10, y = 20;

	int *iptr = &x, *iptr1 = &y;

	printf("%d\n",*iptr);
	printf("%d\n",*(iptr+1)); // add + 1 * DTP (4)
}
