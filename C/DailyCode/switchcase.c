#include <stdio.h>

void main(){

	int x;
	printf("Enter value:\n");
	scanf("%d",&x);

	switch(x){

		case 1:
			printf("one\n");
			break;
		case 2:
			printf("two\n");
			break;
		case 3:
			printf("three\n");
			break;
		case 4:
			printf("four\n");
			break;
		case 5:
			printf("five\n");
			break;
		default:
			printf("Wrong value\n");	
	}
}
