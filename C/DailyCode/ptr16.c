// void ptr

#include <stdio.h>

void main(){

	int x = 10;

	int *iptr = &x;
	void *vptr = &x;

	printf("%p\n",iptr);
	printf("%p\n",vptr);
	printf("%d\n",*iptr);
//	printf("%d",*vptr); // in case of this we need type cast the variable
	printf("%d\n",*(int*)vptr);


}
