#include <stdio.h>
int sumOfArray(int* arr,int n){
	int sum = 0;
	for(int i=0 ; i<n ; i++){
		sum = sum + *(arr+i);
	}

	return sum;
}
void main(){
	int arr[]={10,20,30,40,50};
	int size = sizeof(arr)/sizeof(int);
	int ans = sumOfArray(arr,size);
	printf("%d\n",ans);
}
