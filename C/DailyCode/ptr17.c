// wild and null ptr

#include <stdio.h>

void main(){

	int x = 10;
	
	int *ptr1;            //wild ptr
	
	int *iptr = NULL;	

	printf("%d\n",*ptr1);    //in wild ptr we can dont derefrance the ptr
	printf("%d\n",*iptr);     //in null ptr we can dont derefrance the ptr
 
}
