#include <stdio.h>

void main(){

	const int Fnumb = 4;
	printf("Fnumb %d\n",Fnumb);

	Fnumb = 2;
	printf("Fnumb %d\n",Fnumb);      // Error assignment  read only variable Fnumb
}
