//add of two pointer

#include <stdio.h>

void main(){
	 
	int x = 10, y = 30;

	int *iptr = &x, *iptr2 = &y;

	printf("%d",*iptr+*iptr2);
	printf("%p",iptr+iptr2);    // we can not add two pointer 

}
