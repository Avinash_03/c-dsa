/*wap to print the addition of 1 to 10 with 10 to 1.
 */

#include <stdio.h>

void main(){

	int x = 1;
	int ans;

	for(int i = 10; i >= 1; i--){

		ans = x + i;
		printf(" %d + %d = %d\n",x,i,ans);
		x++;
	}
}
