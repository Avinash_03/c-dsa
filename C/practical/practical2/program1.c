/* wap to find the sum of numbers that are not divisible by 3 up to given num.
 */

#include <stdio.h>

void main(){

	int num = 0;
	int sum = 0;

	printf("Enter number:\n");
	scanf("%d",&num);

	for(int i = 1; i <= num; i++){

		if(i % 3 != 0)
			sum = sum + i;
	}
	printf(" sum of numbers not divisible by 3 is %d\n",sum);

}
