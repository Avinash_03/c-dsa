/* wap to take a num as input and print whether that is a prime num or not
 */

#include <stdio.h>

void main(){

	int num,cnt=0;

	printf("Enter num:\n");
	scanf("%d",&num);

	for(int i = 2; i < num; i++){
			if(num % i == 0)
				cnt++;
	}

	if(cnt != 0)
		printf("%d is not PRIME\n",num);
	else
		printf("%d is PRIME\n",num);
}

