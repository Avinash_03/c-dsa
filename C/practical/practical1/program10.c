	/* wap to print count of divisors of the entered num.
	 *
	 */


#include <stdio.h>

void main(){

	 int count;
	 int num;

	 printf("Enter num:\n");
	 scanf("%d",&num);

	 for(int i = 1; i <= num; i++){

		 if(num % i == 0)
			 printf(" %d\n",i);
	 }
}
