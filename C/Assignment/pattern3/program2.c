#include <stdio.h>
 
void main(){
 
        int rows;
        printf("Enter no of rows:");
        scanf("%d",&rows);
 
        for(int i = 1; i <= rows; i++){
                int x = 3;
                char ch = 'a';
 
                for(int j = 1; j <= 4; j++){
                        if(j % 2 == 1 && i % 2 == 1 || j % 2 == 0 && i % 2 == 0){
                                printf("%d ",x);
                                x--;
                                ch++;
                        } else {
                                printf("%c ",ch);
                                x--;
                                ch++;
                        }
                }
                printf("\n");
        }
}
