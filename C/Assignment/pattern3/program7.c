#include <stdio.h>

void main(){
	int rows;
	int x1 = 1;

	printf("Enter no of rows:");
	scanf("%d",&rows);

	for(int i = 1; i <= rows; i++){
		int x = x1;
		for(int j = 1; j <= rows; j++){
			if(j %  2 == 0 && i % 2 == 0 || j % 2 == 1 && i % 2 ==1){
				printf("%d\t",x*x*x);
				x++;
			} else {
				printf("%d\t",x*x);
				x++;
			}
		}
		printf("\n");
		x1++;
	}
}
