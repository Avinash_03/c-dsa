#include <stdio.h>

void main(){

	int rows;
	int cnt = 1;
	printf("Enter no of rows:");
	scanf("%d",&rows);

	for(int i = 1; i <= rows; i++){
		for(int j = 1; j <= rows; j++){
		  	if(cnt == 1){
				printf("=\t");
			} else if(cnt == 2){
				printf("$\t");
			} else if(cnt == 3) {
				printf("@\t");
			}

		}
		printf("\n");
		cnt++;
		if(cnt == 4)
			cnt = 1;

	}
}
