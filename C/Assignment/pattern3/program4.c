#include <stdio.h>

void main(){

	int rows;
	char ch = 'A';
	char ch1 = 'a';

	printf("Enter no of rows:");
	scanf("%d",&rows);

	for(int i = 1; i <= rows; i++){
		char ch2 = ch;
		char ch3 = ch1;
		for(int j = 1; j <= 4; j++){
			if(j % 2 == 1){
				printf("%c\t",ch3);
				ch3++;
				ch2++;
			} else {
				printf("%c\t",ch2);
				ch3++;
				ch2++;
			}
		}
		printf("\n");
		ch++;
		ch1++;
	}
}
