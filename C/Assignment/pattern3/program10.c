#include <stdio.h>

void main(){

	int rows;
	int x = 1;

	printf("Enter no of rows:");
	scanf("%d",&rows);

	int ch2 = 64 + rows;
	int ch3 = 96 + rows;

	for(int i = 1; i <= rows; i++){
	int ch = ch2;
	int ch1 = ch3;

		for(int j = 1; j <= rows; j++){
			if(i % 2 == 1){
				printf("%c%d\t",ch,x);
				x++;
				ch--;
				ch1++;
			} else {
				printf("%c%d\t",ch1,x);
				ch--;
				ch1++;
				x--;
				if(j == rows)
					x = x+2;
			}
		}
		printf("\n");
		ch3++;
		ch2++;
	}
}
