#include <stdio.h>

void main(){

	int rows;

	printf("Enter no of rows:");
	scanf("%d",&rows);

	int x = rows*rows;
	int ch = 64+x;


	for(int i = 1; i <= rows; i++){
		for(int j = 1; j <= rows; j++){
			if(i % 2 == 1){
				printf("%d\t",x);
				x--;
				ch--;
			} else {
				printf("%c\t",ch);
				ch--;
				x--;
			}
		}
		printf("\n");
	}
}
