#include <stdio.h>

void main(){

	int rows;
	printf("Enter no of rows:");
	scanf("%d",&rows);

	for(int i = 1; i <= rows; i++){
		int x = 4;
		char ch = 'a';
		for(int j = 1; j <= 4; j++){
			if(j % 2 ==1){
				printf("%d ",x);
				x--;
			} else {
				printf("%c ",ch);
				ch++;
			}
		}
		printf("\n");
	}
}
