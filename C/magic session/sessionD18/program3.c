//wap that dynamically allocates a 2-D array of integers,takes value from the user,and prints it

#include <stdio.h>
#include <stdlib.h>

void main(){

	int rows = 0, col = 0;
	printf("Enter rows and col:");
	scanf("%d%d",&rows,&col);
	int *ptr = (int*)malloc((rows*col)*sizeof(int*));

	for(int i = 0; i< rows*col; i++){
		scanf("%d",ptr+i);

	}
	for(int i = 0; i< rows*col; i++){
			printf("%d ",*(ptr+i));
	}
	free(ptr);
}
