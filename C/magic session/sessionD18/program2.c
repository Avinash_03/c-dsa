//wap that dynamically allocates a 1-D array of marks, takes value from the user and prints it 
#include <stdio.h>
#include <stdlib.h>

void main(){

	float size = 0;
	printf("Enter no of arr element:");
	scanf("%f",&size);
	float *mark = (float*)malloc(size*sizeof(float));

	for(int i = 0; i< size; i++){
		scanf("%f",mark+i);
	}
	for(int i = 0; i< size; i++){
		printf("%f",*(mark+i));
	}
	free(mark);

}
