//free the memmory
#include <stdio.h>
#include <stdlib.h>

void main(){

	int rows = 4,col =3;
	int **ptr = (int**)malloc(rows*sizeof(int));
	for(int i = 0; i<rows;i++){
		ptr[i]=(int*)malloc(col*sizeof(int));
	}
	for(int i = 0 ; i< rows;i++){
		free(ptr[i]);
	}
	free(ptr);
}
