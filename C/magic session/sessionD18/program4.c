//wap that dynamically allocates a 3-D array of integers,takes value from the user,and prints it
#include <stdio.h>
#include <stdlib.h>

void main(){

	int pla = 0,row =0, col =0;
	printf("Enter no of plan ,rows and column");
	scanf("%d%d%d",&pla,&row,&col);
	int *ptr = (int*)malloc(pla*row*col*sizeof(int*));

	for(int i = 0; i<pla*row*col;i++){
		scanf("%d",ptr+i);
	}
	for(int i = 0; i< pla*row*col;i++){
		printf("%d ",*(ptr+i));
	}
	free(ptr);
}
