/*	D	C	B	A
 *		e	f	g
 *			F	E
 *				g
 */
#include <stdio.h>

void main(){

	int rows = 0;
	printf("Enter no of rows:");
	scanf("%d",&rows);

	char ch = 64+rows;
	char ch1 = 96+rows;

	for(int i = 1; i <= rows; i++){
		char ch2 = ch;
		char ch3 = ch1;
		for(int spa = 1; spa < i; spa++){
			printf("  \t");
		}
		for(int j = 1; j<= rows+1-i;j++){
			if(i%2==1){
				printf(" %c\t",ch2);
				ch2--;
			}else{
				printf(" %c\t",ch3);
				ch3--;
			}
		}
		printf("\n");
		ch++;
		ch1++;
	}
}
