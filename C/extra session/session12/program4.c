/*	5	D	3	B	1
 *	D	3	B	1
 *	3	B	1
 *	B	1
 *	1	
 */

#include <stdio.h>

void main(){

	int rows = 0;
	printf("Enter no of rows:");
	scanf("%d",&rows);

	int x = rows;
	char ch = 64 + rows;

	for(int i = 1; i <= rows; i++){
		int x1 = x;
		char ch1 = ch;
		for(int j = 1; j <= rows+1-i ; j++){
			if(i%2 == 0 && j%2 == 0 || i%2 == 1 && j%2 == 1){
				printf(" %d\t",x1);
				x1--;
				ch1--;
			}else{
				printf(" %c\t",ch1);
				x1--;
				ch1--;
			}

		}
		printf("\n");
		x--;
		ch--;
	}
}
