/*					1
 *				1	b
 *			1	b	2
 *		1	b	2	d
 *	1	b	2	d	3
 */

#include <stdio.h>

void main(){

	int rows = 0;
	printf("Enter no of rows:");
	scanf("%d",&rows);

	for(int i = 1; i <= rows; i++){
		int x = 1;
		char ch = 'a';
		for(int spa = 1; spa <= rows+1-i; spa++){
			printf("  \t");
		}
		for(int j = 1; j <= i; j++){
			if(j % 2 == 1){
				printf(" %d\t",x);
				x++;
				ch++;
			}else{
				printf(" %c\t",ch);
				ch++;
			}
		}
		printf("\n");
	}

}
