/*				1
 *			4	2	4
 *		9	6	3	6	9
 *	16	12	8	4	8	12	16
 *		9	6	3	6	9
 *			4	2	4
 *				1
 */

#include <stdio.h>

void main(){

	int rows = 0;
	printf("Enter no of rows:");
	scanf("%d",&rows);
	int s = rows;
	int p = 1;
	int x = 1;
	int z = 1;

	for(int i = 1; i <= rows*2-1; i++){
		int x1 = x;
		if(rows >=i){
			s--;
		}else{
			s++;
		}
		
		for(int spa = 1; spa <= s; spa++){
			printf("  \t");
		}
		if(rows >= i){
			p = i*2-1;
		}else{
			p = p - 2;
		}
		for(int j = 1; j <= p; j++){
			 if(z > j){
				 printf(" %d\t",x1);
			 	 x1 = x1-z;
			 }else{
				 printf(" %d\t",x1);
				 x1 = x1+z;
			 }
		}
		printf("\n");
		if(rows > i){
			x = x+z*2+1;
			z++;
		}else{
			x = x - z*2+1;
			z--;
		}
	}
}
