/*	g	6	e	4	c	2	a
 *		5	d	3	b	1
 *			c	2	a
 *				1
 */

#include <stdio.h>

void main(){

	int rows = 0 ;
	printf("Enter no of rows:");
	scanf("%d",&rows);

	int x = rows*2-1;
	char ch = 96+x;

	for(int i = 1; i <= rows; i++){
		int x1 =x;
		char ch1 = ch;
		for(int spa = 1; spa < i; spa++){
			printf("  \t");
		}
		for(int j = 1; j <= (rows*2)-(2*i-1); j++){
			if(i%2==0&&j%2==0||i%2==1&&j%2==1){
				printf(" %c\t",ch1);
				ch1--;
				x1--;
			}else{
				printf("  %d\t",x1);
				ch1--;
				x1--;
			}
		}
		printf("\n");
		x = x-2;
		ch = ch -2;
	}
}
