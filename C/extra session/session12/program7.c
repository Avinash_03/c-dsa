/*				1
 *		3	2	1
 *5	4	3	2	1
 		3	2	1
				1
 */

#include <stdio.h>

void main(){

	int rows = 0;
	printf("Enter on of rows");
	scanf("%d",&rows);

	int x = 1;
	int s = rows*2;
	int p = -1;

	for(int i = 1; i <= rows*2-1; i++){
		int x1 = x;
		if(rows >= i)
			s = s - 2;
		else
			s = s + 2;
		for(int spa = 1; spa <= s; spa++ ){
			printf("  \t");
		}
		if(rows >= i)
			p = p + 2;
		else
			p = p - 2;
		for(int j = 1; j <= p; j++ ){

			printf(" %d\t",x1);
			x1--;

		}
		printf("\n");
		if(rows > i )
			x = x + 2;
		else
			x = x - 2;
	}
}
