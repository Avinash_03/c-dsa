/*			1
 *		1	b	3
 *	1	b	3	b	5
 1	b	3	d	5	f	7
 */

#include <stdio.h>

void main(){

	int rows = 0;
	printf("Enter no of rows:");
	scanf("%d",&rows);

	for(int i = 1; i <= rows; i++){
		int x = 1;
		char ch = 'a';
		for(int spa = 1; spa <= rows-i; spa++){
			printf("  \t");
		}
		for(int j = 1; j <= i*2-1; j++){
				if(j%2==0)
					printf(" %c\t",ch);
				else
					printf(" %d\t",x);
				x++;
				ch++;
			
		}
		printf("\n");
	}
}
