/*	1	4	7	10
 *	7	10	13	16
 *	13	16	19	22
 *	19	22	25	28	
 */

#include <stdio.h>

void main(){

	int rows = 0;
	printf("Enter no of rows:");
	scanf("%d",&rows);
	
	int x = 1;

	for(int i = 1; i <= rows; i++){
		for(int j = 1; j <= rows; j++){
			printf(" %d\t",x);
			x = x + 3;
		}
		printf("\n");
		x = x - 6;
	}
}
