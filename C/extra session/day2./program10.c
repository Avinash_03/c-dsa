/* take no of rows from user 
 * 	10
 * 	I	H
 * 	7	6	5
 * 	D	C	B	A
 */
#include <stdio.h>

void main(){
	int rows;
	printf("Enter no of rows:");
	scanf("%d",&rows);
	char ch = 64;
	int x = 0;

//	x = (rows*5)-10;
//	ch = 64+(rows*5)-10;

	for(int i = 1; i <= rows; i++){
		x = x +i;
		ch = ch +i;
	}

	for(int i = 1; i <= rows ; i++){
		for(int j = 1; j <= i; j++){
			if(i % 2 == 1){
				printf("%d\t",x);
				x--;
				ch--;
			}else{
				printf("%c\t",ch);
				ch--;
				x--;
			}
		}
		printf("\n");
	}
}
