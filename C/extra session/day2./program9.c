/* take no of rows from user 
 * 	3
 * 	6	9
 * 	12	15	18
 */

#include <stdio.h>

void main(){

	int rows;
	printf("Enter no of rows:");
	scanf("%d",&rows);
	int x = rows;

	for(int i = 1; i <= rows; i++){
		for(int j = 1; j <= i; j++){

			printf("%d\t",x);
			x = x+rows;
		}
		printf("\n");
	}
}
