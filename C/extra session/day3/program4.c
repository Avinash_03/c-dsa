/*	take no of rows from user 
 *		1	2	3	4
 *		5	6	7
 *		8	9
 *		10
 */
#include <stdio.h>

void main(){

	int rows,x = 1;
	printf("Enter no of rows:");
	scanf("%d",&rows);

	for(int i = 1; i <= rows ; i++){
		for(int j = rows; j >= i;j--){
			printf("%d\t",x);
			x++;
		}
		printf("\n");
	}
}
