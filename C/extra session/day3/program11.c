/*	take no of rows from user 
 *	1	2	3	4
 *	4	5	6
 *	6	7	
 *	7
 */
#include<stdio.h>

void main(){

	int rows;
	 
	printf("Enter no of rows:");
	scanf("%d",&rows);
	int x = 1;

	for(int i = 1; i <= rows; i++){
		for(int j = rows; j >= i; j--){
			printf("%d\t",x);
			x++;
		}
		printf("\n");
		x--;
	}
}
