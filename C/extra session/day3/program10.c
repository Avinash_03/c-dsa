/*	take no of rows from user
 *	4	3	2	1
 *	C	B	A
 *	2	1
 *	A
 */
#include <stdio.h>

void main(){

	int rows;

	printf("Enter no of rows:");
	scanf("%d",&rows);

	int x= rows;
	char ch = 64+rows;

	for(int i = 1; i <= rows; i++){
 		int x1 = x;
		char ch1 = ch;
		for(int j = rows; j >= i; j--){
			if(i%2==1){
				printf("%d\t",x1);
				x1--;
				ch1--;
			}else{
				printf("%c\t",ch1);
				ch1--;
				x1--;
			}
		}
		printf("\n");
		x--;
		ch--;
	}
}
