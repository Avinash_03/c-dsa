/*	take no of rows from user
 *	4	3	2	1	
 *	4	3	2
 *	4	3
 *	4
 */
#include <stdio.h>

void main(){
	int rows;

	printf("Enter no of rows:");
	scanf("%d",&rows);

	for(int i = 1; i <= rows; i++){
		int x = rows;
		for(int j = rows; j >= i; j--){

			printf("%d\t",x);
			x--;
		}
		printf("\n");
	}
}
