/*	take no of rows from user 
 *	D	C	B	A
 *	C	B	A
 *	B	A
 *	A
 */
#include<stdio.h>

void main(){

	int rows;
	printf("Enter no of rows:");
	scanf("%d",&rows);

	char ch = 64+rows;

	for(int i = 1; i <= rows; i++){
		char ch1 = ch;
		for(int j = rows; j >= i; j--){
			printf("%c\t",ch1);
			ch1--;
		}
		printf("\n");
		ch--;
	}
}
