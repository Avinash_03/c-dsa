/*	take no of rows from user 
 *		1	2	3	4	5
 *		2	3	4	5
 *		3	4	5
 *		4	5
 *		5
 */
#include <stdio.h>

void main(){
	int rows;
	printf("Entet no of rows:");
	scanf("%d",&rows);
	int x1 = 1;

	for(int i = 1; i <= rows; i++){
		int x = x1;
		for(int j = rows ; j >= i ; j--){
			printf("%d\t",x);
			x++;
		}
		printf("\n");
		x1++;
	}
}
