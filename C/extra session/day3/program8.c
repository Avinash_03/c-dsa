/* 	take no of rows from user 
 *  	d	d	d	d
 *  	C	C	C
 *  	b	b
 *  	A
 */
#include <stdio.h>

void main(){
	int rows;
	printf("Enter no of rows:");
	scanf("%d",&rows);

	char ch = 96 + rows;
	char ch1 = 64 + rows;

	for(int i = 1; i <= rows; i++){
		for(int j = rows; j >= i ; j--){
			if(i%2==1){
				printf("%c\t",ch);
			}else{
				printf("%c\t",ch1);
			}

		}
		printf("\n");
		ch--;
		ch1--;
	}
}
