/*	WAP TO PRINT THE FACTORIAL OF EACH NO BETN A GIVEN RANGE
 */
#include <stdio.h>

void main(){

	int start=0,end=0;

	printf("Enter start no and end no:");
	scanf("%d%d",&start,&end);

	for(int j = start; j<=end; j++){
		int fact =1;
		for(int i = 1; i <= j; i++){
			fact=fact*i;
		}
			printf("factorial %d is %d\n",j,fact);
	}
}
