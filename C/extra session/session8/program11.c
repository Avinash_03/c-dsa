/*	7	6	5	4	3	2	1
 *		5	4	3	2	1
 *			3	2	1
 *				1
 */

#include <stdio.h>

void main(){

	int rows = 0;

	printf("Enter no of rows:");
	scanf("%d",&rows);

	int x = rows*2-1;

	for(int i = 1; i <= rows; i++){
		int x1 = x;
		for(int spa = 1; spa < i; spa++){
			printf("  \t");
		}
		for(int j = 1; j <= rows*2-(2*i-1); j++){
			printf(" \t%d",x1);
			x1--;
		}
		printf("\n");
		x = x-2;
	}
}
