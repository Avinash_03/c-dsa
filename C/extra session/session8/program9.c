/*	A0	B1	C2	D3	E4	F5	G6
 *		H2	I3	J4	K5	L6
 *			M4	N5	O6
 *				P6
 */

#include <stdio.h>

void main(){

	int rows = 0; 
	printf("Enter no of rows:");
	scanf("%d",&rows);

	 int x = 0;
	 char ch = 'A';

	 for(int i = 1; i <= rows; i++){
		 int x1 = x;
		 for(int spa = 1; spa< i; spa++){
			 printf("  \t");
		 }
		 for(int j = 1; j <= 2*rows-(2*i-1); j++){
			 printf(" \t%c%d",ch,x1);
			 x1++;
			 ch++;
		 }
		 printf("\n");
		 x = x+2;
	 }
}
