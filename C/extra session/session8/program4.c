/*	1	2	3	4	3	2	1
 *		2	3	4	3	2
 *			3	4	3
 *				4
 */
#include <stdio.h>

void main(){

	int rows = 0;

	printf("Enter no of rowa:");
	scanf("%d",&rows);

	int x = 1;

	for(int i = 1; i <= rows; i++){
		int x1 = x;
		for(int spa = 1; spa < i; spa++){
			printf("  ");
		}
		for(int j = 1; j <= rows*2-(2*i-1); j++){
			if(rows-i<j){
				printf(" %d",x1);
				x1-- ;
			}else {
			 	printf(" %d",x1);
				x1++;
			}
		}
		printf("\n");
		x++;
	}
}
