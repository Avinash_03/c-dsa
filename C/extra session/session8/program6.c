/*	1	2	3	4	3	2	1
 *		1	2	3	2	1
 *			1	2	1
 *				1
 */
#include <stdio.h>

void main(){

	int rows = 0;
	printf("Enter no of rows:");
	scanf("%d",&rows);

	for(int i = 1; i <= rows; i++){
		int x = 1;
		for(int spa = 1; spa <i; spa++){
			printf("  ");
		}
		for(int j = 1; j <= rows*2-(2*i-1); j++){
			printf(" %d",x);
			if(rows-i<j)
				x--;
			else
				x++;
		}
		printf("\n");
	}
}
