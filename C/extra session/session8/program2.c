/*	1	2	3	4	5
 *		6	7	8
 *			9
 *
 */

#include <stdio.h>

void main(){

	int rows=0;
	printf("Enter no of rows:");
	scanf("%d",&rows);

	int x = 1;

	for(int i = 1; i <= rows; i++){
		for(int spa = 1; spa < i; spa++){
			printf(" \t");
		}
		for(int j = 1; j <= rows*2-(2*i-1); j++){
			printf("\t%d",x);
			x++;
		}
		printf("\n");

	}

	
}
