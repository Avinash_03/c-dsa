/*	*	*	*	#	*	*	*
 *		*	*	#	*	*
 *			*	#	*
 *				#
 */

#include <stdio.h>

void main(){

	int rows = 0;
	printf("Enter no of rows:");
	scanf("%d",&rows);
	int x = rows+1;

	for(int i = 1; i <= rows; i++){
		for(int spa = 1; spa < i; spa++){
			printf("  \t");
		}
		for(int j = 1; j <= 2*rows-(2*i-1); j++){
			if(x-i==j)
				printf(" \t#");
			else
		 		printf(" \t*");
		}
		printf("\n");
	}
}
