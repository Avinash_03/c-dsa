/*	*	*	*	*	*	*	*
 *		*	*	*	*	*
 *			*	*	*
 *				*
 */

#include <stdio.h>

void main(){

	int rows=0; 
	printf("Enter no of rows:");
	scanf("%d",&rows);

	for(int i = 1; i <= rows; i++){
		for(int spa = 1; spa < i; spa++){
			printf("  ");
		}
		for(int j = 1; j <= rows*2-(2*i-1); j++){
			printf(" *");
		}
		printf("\n");
	}
}
