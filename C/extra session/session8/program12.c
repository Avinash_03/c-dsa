/*	1	3	5	7	9	7	5	3	1
 *		9	7	5	3	5	7	9
 *			3	5	7	5	3	
 *				7	5	7
 *					5
 */
#include <stdio.h>

void main(){

	int rows = 0;
	printf("Enter no of rows:");
	scanf("%d",&rows);

	int r = rows+1;
	int x = 1;
	int y = rows*2-1;

	for(int i = 1; i <= rows; i++){
		for(int spa = 1; spa < i; spa++){
			printf("  \t");
		}
		for(int j = 1; j <= rows*2-(2*i-1); j++){
			if(i % 2 == 1){
				if(r-i <= j){
					printf(" \t%d",x);
					x = x-2;
				}else{
					printf(" \t%d",x);
					x = x+2;
				}
			}else {
				if(r-i <= j){
					printf(" \t%d",y);
					y = y+2;
				}else{
					printf(" \t%d",y);
					y = y-2;
				}
			}
		}
		printf("\n");
		if(i % 2 ==1 ){
			x = x+4;
		}else{
			y = y-4;
		}
	}
}
