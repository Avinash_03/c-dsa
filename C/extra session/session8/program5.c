/*	d	d	d	d	d	d	d
 *		c	c	c	c	c
 *			b	b	b
 *				a
 */
#include <stdio.h>

void main(){

	int rows = 0;

	printf("Enter no of rows:");
	scanf("%d",&rows);

	char ch = 96+rows;

	for(int i = 1; i <= rows; i++){
		for(int spa = 1; spa < i; spa++){
			printf("  ");
		}
		for(int j = 1; j <= rows*2-(2*i-1); j++){
			printf(" %c",ch);
		}
		printf("\n");
		ch--;
	}
}
