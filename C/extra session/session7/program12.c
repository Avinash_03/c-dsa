/*			4
 *		3	6	3
 *	2	4	6	4	2
 *1	2	3	4	3	2	1
 */

#include <stdio.h>

void main(){

	int rows=0;
	printf("Enter no of rows:");
	scanf("%d",&rows);

	int x =rows;

	for(int i = 1; i <= rows; i++){
		int x1 = x;
		for(int spa = 1; spa <= rows-i; spa++){
			printf("  ");
		}
		for(int j = 1; j <=2*i-1; j++){
			if(i<=j){
				printf(" %d",x1);
				x1= x1-x;
			}else{
				printf(" %d",x1);
				x1= x1+x;
			}
		}
		printf("\n");
		x--;
	}
}
