/*			*
 		*	*	*
 	*	*	*	*	*
  *	*	*	*	*	*	*
 */
#include <stdio.h>

void main(){
	
	int rows=0;
	printf("Enter no of rows:");
	scanf("%d",&rows);

	for(int i = 1; i <= rows; i++){
		for(int space = 1 ; space <= rows-i; space++){
			printf("  ");
		}
		for(int j = 1; j <= 2*i-1; j++){
			printf(" *");
		}
		printf("\n");
	}

}
