/*			A
 *		b	a	b
 *	C	E	G	E	C
 *d	c	b	a	b	c	d
 */

#include <stdio.h>

void main(){

	int rows = 0;
	printf("Enter no of rows:");
	scanf("%d",&rows);

	char ch = 'A';
	char ch1 = 'a';

	for(int i = 1; i <= rows; i++){
		char ch2 = ch;
		char ch3 = ch1;
		for(int spa = 1; spa <= rows-i; spa++){
			printf("  ");
		}
		for(int j = 1; j <= 2*i-1; j++){
			if(i%2==1){
				printf(" %c",ch2);
				if(i<=j){
					ch2 = ch2-2;
					ch3 = ch3-2;
				}else{
					ch2 = ch2+2;
					ch3 = ch3+2;
				}
			}else{
				printf(" %c",ch3);
				if(i<=j){
					ch2++;
					ch3++;
				}else{
					ch2--;
					ch3--;
				}
			}
		}
		printf("\n");
		ch++;
		ch1++;
	}
}
