/*				4
 *			4	3	4
 *		4	3	2	3	4
 *	4	3	2	1	2	3	4
 *
 */

#include <stdio.h>

void main(){

	int rows=0;

	printf("Enter no of rows:");
	scanf("%d",&rows);

	for(int i = 1; i <= rows; i++){
		int x = rows;
		for(int space = 1; space <= rows-i; space++){
			printf("  ");
		}
		for(int j = 1; j <= 2*i-1; j++){
			printf(" %d",x);
			if(i<=j)
				x++;
			else
				x--;
		}
		printf("\n");
	}
}
