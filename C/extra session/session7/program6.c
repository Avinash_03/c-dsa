/*				d
 *			C	C	C
 *		b	b	b	b	b
 *	A	A	A	A	A	A	A
 */

#include <stdio.h>

void main(){

	int rows=0;
	printf("Enter no of rows:");
	scanf("%d",&rows);

	char ch = 96+rows;
	char ch1 = 64+rows;
	
	for(int i = 1; i <= rows; i++){
		for(int spa = 1; spa <= rows-i; spa++){

			printf("  ");
		}
		for(int j = 1; j <= 2*i-1; j++){
			if(i % 2 == 0){
				printf(" %c",ch1);
			}else{
				printf(" %c",ch);
			}
		}
		printf("\n");
		ch1--;
		ch--;
	}

}
