/*				d
 *			c	c	c
 *		b	b	b	b	b
 *	a	a	a	a	a	a	a
 */

#include <stdio.h>

void main(){

	int rows = 0;

	printf("Enter no of rows:");
	scanf("%d",&rows);

	char ch = 96+rows;

	for(int i = 1; i <= rows; i++){
		for(int spa = 1; spa <= rows-i; spa++){
			printf("  ");
		}
		for(int j = 1; j <= 2*i-1; j++){
			printf(" %c",ch);
		}
		printf("\n");
		ch--;
	}
}
