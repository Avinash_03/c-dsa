/*				1
 *			4	7	4
 *		7	10	13	10	7
 *	10	13	16	19	16	13	10
 */

#include <stdio.h>

void main(){

	int rows =0;
	printf("Enter no of rows:");
	scanf("%d",&rows);

	int x =1;

	for(int i = 1; i <= rows; i++){
		int x1 = x;
		for(int spa = 1; spa <= rows-i; spa++){
			printf("    ");

		}
		for(int j = 1; j <= 2*i-1; j++){
			if(i <= j){
				printf("  %d",x1);
				x1 = x1-3;
			}else{
				printf("  %d",x1);
				x1 = x1+3;
			}
		}
		printf("\n");
		x = x+3;
	}
}
