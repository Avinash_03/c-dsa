/*			4
 *		4	5	4
 *	4	5	6	5	4
 *4	5	6	7	6	5	4
 */

#include <stdio.h>

void main(){

	int rows=0;
	fprintf(stdout,"Enter no of rows:");
	fscanf(stdin,"%d",&rows);

	for(int i = 1; i <= rows; i++){
		int x = rows;
		for(int spa = 1; spa <= rows-i; spa++){
			printf("  ");
		}
		for(int j = 1; j <= 2*i-1; j++){
			if(i <= j){
				printf(" %d",x);
				x--;
			}else{
				printf(" %d",x);
				x++;
			}
		}
		printf("\n");
	}
}
