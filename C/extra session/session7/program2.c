/* 			4
 *  		3	3	3
 *  	2	2	2	2	2
  1	1	1	1	1	1	1
 * 
 */

#include <stdio.h>

void main(){

	int rows = 0, num = 0;

	printf("Enter no of rows:");
	scanf("%d",&rows);

	num = rows;
	
	for(int i = 1; i <= rows; i++){
		for(int space = 1; space <= rows-i; space++){
			printf("  ");
		}
		for(int j = 1; j <= 2*i-1; j++){
			printf(" %d",num);
		}
		printf("\n");
		num--;
	}

}
