/*			D
 *		c	D	c
 *	B	c	D	c	B
 a	B	c	D	c	B	a
 */

#include <stdio.h>

void main(){

	int rows =0;
	printf("Enter no of rows:");
	scanf("%d",&rows);

	char ch = 64+rows;
	char ch1 = 96+rows;

	for(int i = 1; i <= rows; i++){
		for(int spa = 1; spa <= rows-i; spa++){
			printf("  ");
		}
		for(int j = 1; j <=2*i-1; j++){
			if(i%2==1 && j%2==1 || i%2==0 && j%2==0){
				printf(" %c",ch);
			}else{
				printf(" %c",ch1);
			}
			if(i<=j){
				ch--;
				ch1--;
			}else{
				ch++;
				ch1++;
			}
		}
		printf("\n");
	}
}
