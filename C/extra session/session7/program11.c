/*				1
 *			A	b	A
 *		1	2	3	2	1
 *	A	b	C	d	C	b	A
 *
 */

#include <stdio.h>

void main(){

	int rows = 0;
	printf("Enter no of rows:");
	scanf("%d",&rows);

	for(int i = 1; i <= rows; i++){
		int x = 1; 
		char ch = 'A',ch1 = 'a';
		for(int spa = 1; spa <= rows-i; spa++){
			printf("  ");
		}
		for(int j = 1; j <= 2*i-1; j++){
			if(i%2==1){
				printf(" %d",x);
				if(i<=j){
					x--;
				}else{
					x++;
				}
			}else{
				if(i%2==0 && j%2==1){
					printf(" %c",ch);
				}else{
					printf(" %c",ch1);
				}
				if(i<=j){
					ch--;
					ch1--;
				}else{
					ch++;
					ch1++;
				}
			}
		}
		printf("\n");
	}
}
