/*	take no of rows from user
 *	a	B	c	D
 *		e	F	g
 *			H	i
 *				J
 */
#include <stdio.h>

void main(){

	int rows = 0 ;
	
	printf("Enter no of rows:");
	scanf("%d",&rows);

	char ch = 'A',ch1='a';
	int x = 1;

	for(int i = 1; i <= rows; i++){
		for(int space = 1; space < i ; space++){
			printf(" \t");
		}
		for(int j = i; j <= rows; j++){
			if(x%2==0){
				printf("%c\t",ch);
				ch++;
				ch1++;
				x++;
			}else{
				printf("%c\t",ch1);
				ch++;
				ch1++;
				x++;
			}
		}
		printf("\n");
	}
}
