/*	take no of rows from user
 *	100	9	64	7
 *		36	5	16
 *			3	4
 *				1
 */
#include <stdio.h>

void main(){

	int rows = 0 ;
	
	printf("Enter no of rows:");
	scanf("%d",&rows);

	int x = 0;
	
	x = rows*((rows/2)+0.5);
/*	for(int i = 1; i <= rows; i++){
		x = x +i;
	}*/

	for(int i = 1; i <= rows; i++){
		for(int space = 1; space < i ; space++){
			printf(" \t");
		}
		for(int j = i; j<=rows;j++){
			if(x%2==0){
				printf("%d\t",x*x);
				x--;
			}else{
				printf("%d\t",x);
				x--;
			}
		}
		printf("\n");
	}
}
