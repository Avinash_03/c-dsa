/*	take no of rows from user
 *	1	3	5	7	9
 *		9	7	5	3
 *			3	5	7
 *				7	5
 *					5
 */
#include <stdio.h>

void main(){

	int rows = 0 ;
	
	printf("Enter no of rows:");
	scanf("%d",&rows);

	int x = 1;

	for(int i = 1; i <= rows; i++){
		for(int space = 1; space < i ; space++){
			printf(" \t");
		}
		for(int j = i; j <= rows; j++){
			if(i % 2 != 0){
				printf("%d\t",x);
				x = x + 2;
			}else{
				printf("%d\t",x);
				x = x - 2;
			}

		}
			if(i % 2 != 0){
				x = x-2;
			}else{
				x = x + 2;
			}
		printf("\n");
	}
}
