/*	wap to print the number whose factorial is even .take range from user
 */
#include <stdio.h>

void main(){

	int start=0,end=0;
	int fact = 1;

	printf("Enter range:");
	scanf("%d %d",&start,&end);

	for(int i = start; i<=end; i++){
		for(int j = 1; j <= i ; j++){
			fact*=j;
		}
		if(fact%2==0)
			printf("%d\t",i);
	}
	printf("\n");
}
