/*	take no of rows from user
 *	1	2	3	4
 *		1	2	3
 *			1	2
 *				1
 */
#include <stdio.h>

void main(){

	int rows=0;

	printf("Enter no of rows:");
	scanf("%d",&rows);

	for(int i = 1; i <= rows; i++){
		int x = 1;
		for(int space = 1; space < i ; space++){
			printf(" \t");
		}
		for(int j = i; j <= rows; j++){
			printf("%d\t",x);
			x++;
		}
		printf("\n");
	}
}
