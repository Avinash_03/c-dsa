/*	take no of rows from user
 *	A	b	C	d
 *		e	G	i
 *			K	n
 *				q
 */
#include <stdio.h>

void main(){

	int rows = 0 ;
	
	printf("Enter no of rows:");
	scanf("%d",&rows);

	char ch = 'A';
	char ch1 = 'a';

	for(int i = 1; i <= rows; i++){
		for(int space = 1; space < i ; space++){
			printf(" \t");
		}
		for(int j = i; j <= rows; j++){
			if(j%2==1){
				printf("%c\t",ch);
				ch = ch + i;
				ch1 = ch1 + i;
			}else{
				printf("%c\t",ch1);
				ch = ch + i;
				ch1 = ch1 + i;
			}
		}
		printf("\n");
	}
}
