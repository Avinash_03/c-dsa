/*	take no of rows from user
 *	*	*	*	*
 *		*	*	*
 *			*	*
 *				*
 */
#include <stdio.h>

void main(){

	int rows=0;
	printf("Enter no of rows:");
	scanf("%d",&rows);

	for(int i = 1; i <= rows; i++){
		for(int space = 1; space < i; space++){
			printf(" \t");
		}
		for(int j = i; j <= rows; j++){
			printf("*\t");
		}
		printf("\n");
	}
}
