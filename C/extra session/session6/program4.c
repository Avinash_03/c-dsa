/*	take no of rows from user
 *	d	d	d	d
 *		c	c	c
 *			b	b
 *				b
 */
#include <stdio.h>

void main(){

	int rows = 0;

	printf("Enter no of rows:");
	scanf("%d",&rows);

	char ch = 96+rows;

	for(int i = 1 ; i <= rows ; i++){
		for(int space = 1; space < i ; space++){
			printf(" \t");
		}
		for(int j = i ; j <= rows; j++){
			printf("%c\t",ch);
		}
		printf("\n");
		ch--;
	}
}
