/*	take no of rows from user
 *	D	D	D	D
 *		c	c	c
 *			B	B
 *				a
 */
#include <stdio.h>

void main(){

	int rows = 0 ;
	
	printf("Enter no of rows:");
	scanf("%d",&rows);

	char ch = 64+rows;
	char ch1 = 96+rows;

	for(int i = 1; i <= rows; i++){
		for(int space = 1; space < i ; space++){
			printf(" \t");
		}
		for(int j = i ; j <= rows; j++){
			if(i%2==1){
				printf("%c\t",ch);
			}else{
				printf("%c\t",ch1);
			}
		}
		printf("\n");
		ch--;
		ch1--;
	}
}
