/*	take no of rows from user
 *	1	2	3	4
 *		5	6	7
 *			8	9
 *				10
 */
#include <stdio.h>

void main(){

	int rows =0;

	printf("Enter no of rows:");
	scanf("%d",&rows);

	int x = 1;

	for(int i = 1; i <= rows; i++){
		for(int space = 1; space < i; space++){
			printf(" \t");
		}
		for(int j = i ; j <= rows; j++){
			printf("%d\t",x);
			x++;
		}
		printf("\n");
	}

}
