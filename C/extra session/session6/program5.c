/*	take no of rows from user
 *	a	B	c	D
 *		E	f	G
 *			h	I
 *				J
 */
#include <stdio.h>

void main(){

	int rows = 0;
	
	printf("Enter no of rows:");
	scanf("%d",&rows);

	char ch = 'a',ch1='A';

	for(int i = 1; i <= rows ; i++){
		for(int space = 1; space < i ; space++){
			printf (" \t");
		}
		for(int j = i ; j <= rows; j++){
			if(j%2==1){
				printf("%c\t",ch);
				ch++;
				ch1++;
			}else{
				printf("%c\t",ch1);
				ch++;
				ch1++;
			}
		}
		printf("\n");
	}
}
