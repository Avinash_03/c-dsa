/*TAKE NO OF ROWS FROM ROWS
 * 	1	2	3	4
 * 		2	3	4
 * 			3	4
 * 				4
 */
#include <stdio.h>

void main(){

	int rows = 0 ;
	
	printf("Enter no of rows:");
	scanf("%d",&rows);

	int x =1;

	for(int i = 1; i <= rows; i++){
		int x1 = x;
		for(int space = 1; space < i ; space++){
			printf(" \t");
		}
		for(int j = i ; j <= rows; j++){
			printf("%d\t",x1);
			x1++;
		}
		printf("\n");
		x++;
	}
}
