//Good pair
#include <stdio.h>

int GoodPair(int *arr,int size,int B){
	for(int i=0; i<size;i++){
		for(int j=0;j<size;j++){
			if(arr[i]+arr[j]==B && arr[i]!=arr[j]){
				return 1;
			}
		}
	}
	return 0;
}
void main(){
	int size,x;
	printf("Enter array size:");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array element:\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}

	printf("Enter element to Compair");
	scanf("%d",&x);
	int ret=GoodPair(arr,size,x);
	printf("%d\n",ret);
}
