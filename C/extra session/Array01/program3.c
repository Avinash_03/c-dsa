//rev in rng
#include <stdio.h>

void rev(int *arr,int start,int end){
	int store =0;
	for(int i=start; i<end;i++){
		store=arr[start];
		arr[start]=arr[end];
		arr[end]=store;
		start++;
		end--;
	}
}
void main(){
	int size,start,end;
	printf("Enter array size:");
	scanf("%d",&size);

	int arr1[size];

	printf("Enter array element:\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr1[i]);
	}
	printf("Enter start:\n");
	scanf("%d",&start);
	printf("Enter end:\n");
	scanf("%d",&end);
	rev(arr1,start,end);
	for(int i=0;i<size;i++){
		printf("%d\n",arr1[i]);
	}
}
