//count element
#include <stdio.h>

int CntArr(int *arr,int size){
	int store =0,cnt=0;
	for(int i=0; i<size;i++){
		if(arr[i]<store){
			store=arr[i];
		}
	}
	for(int i =0; i<size;i++){
		if(store<arr[i])
			cnt++;
	}
	return cnt;
}
void main(){
	int size;
	printf("Enter array size:");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array element:\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	int ret=CntArr(arr,size);
	printf("%d\n",ret);
}
