/*				D
 *			D	C	D
 *		D	C	B	C	D
 *	D	C	B	A	B	C	D
 *		D	C	B	C	D
 *			D	C	D
 *				D
 */

#include <stdio.h>

void main(){

	int rows = 0;
	printf("Enter no of rows:");
	scanf("%d",&rows);

	int s = rows;
	int p = 1;
	int x = 1;

	for(int i = 1; i <= rows*2-1; i++){
		char ch = 64+rows;
		if(rows >= i){
			s--;
		}else{
			s++;
		}
		for(int spa = 1; spa <= s; spa++){
			printf("  \t");
		}
		if(rows >= i){
			p = i*2-1;
		}else{
			p = p - 2;

		}
		for(int j = 1; j <= p; j++){
			if(x > j){
				printf(" \t%c",ch);
				ch--;
			}else{
				printf(" \t%c",ch);
				ch++;
			}
		}
		printf("\n");
		if(rows > i){
			x++;
		}else{
			x--;
		}
	}
}
