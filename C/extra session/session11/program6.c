/*				A
 *			b	b	b
 *		C	C	C	C	C
 *	d	d	d	d	d	d	d
 *		C	C	C	C	C
 *			b	b	b
 *				A
 */

#include <stdio.h>

void main(){

	int rows = 0;
	printf("Enter no of rows:");
	scanf("%d",&rows);

	int s = rows;
	int p = 1;
	char x = 'A';
	char y = 'a';

	for(int i = 1; i <= rows*2-1; i++){
		if(rows >= i){
			s--;
		}else{
			s++;
		}
		for(int spa = 1; spa <= s; spa++){
			printf("  \t");
		}
		if(rows >= i){
			p = i*2-1;
		}else{
			p = p - 2;

		}
		for(int j = 1; j <= p; j++){
			if(i % 2 ==1)
				printf(" \t%c",x);
			else
				printf(" \t%c",y);
		}
		printf("\n");
			if(rows > i){
				x++;
				y++;
			}else{
				x--;
				y--;
			}
	}
}
