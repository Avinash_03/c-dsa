/*				4
 *			3	3	3
 *		2	2	2	2	2
 *	1	1	1	1	1	1	1
 *		2	2	2	2	2
 *			3	3	3
 *				4
 */

#include <stdio.h>

void main(){

	int rows = 0;
	printf("Enter no of rows:");
	scanf("%d",&rows);

	int s = rows;
	int p = 1;
	int x = rows;

	for(int i = 1; i <= rows*2-1; i++){
		if(rows >= i){
			s--;
		}else{
			s++;
		}
		for(int spa = 1; spa <= s; spa++){
			printf("  \t");
		}
		if(rows >= i){
			p = i*2-1;
		}else{
			p = p - 2;

		}
		for(int j = 1; j <= p; j++){
			printf(" \t%d",x);
		}
		printf("\n");
		if(rows > i){
			x--;
		}else{
			x++;
		}
	}
}
