/*				A
 *			b	A	b
 *		C	b	A	b	C
 *	d	C	b	A	b	C	D
 *		C	b	A	b	C
 *			b	A	b
 *				A
 */

#include <stdio.h>

void main(){

	int rows = 0;
	printf("Enter no of rows:");
	scanf("%d",&rows);

	int s = rows;
	int p = 1;
	char x = 'A';
	char y = 'a';
	int z = 1;

	for(int i = 1; i <= rows*2-1; i++){
	char x1 = x;
	char y1 = y;
	
		if(rows >= i){
			s--;
		}else{
			s++;
		}
		for(int spa = 1; spa <= s; spa++){
			printf("  \t");
		}
		if(rows >= i){
			p = i*2-1;
		}else{
			p = p - 2;

		}
		for(int j = 1; j <= p; j++){
			if(i % 2 ==1 && j % 2 ==1 || i % 2 ==0 && j % 2 ==0){
				printf(" \t%c",x1);
				if(z > j){
					x1--;
					y1--;
				}else{
					x1++;
					y1++;
				}
			}else{
				printf(" \t%c",y1);
				if(z > j){
					x1--;
					y1--;
				}else{
					x1++;
					y1++;
				}
			}
		}
		printf("\n");
			if(rows > i){
				x++;
				y++;
				z++;
			}else{
				x--;
				y--;
				z--;
			}
	}
}
