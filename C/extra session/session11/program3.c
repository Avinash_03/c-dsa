/*				1
 *			1	2	1
 *		1	2	3	2	1
 *	1	2	3	4	3	2	1
 *		1	2	3	2	1
 *			1	2	1
 *				1
 */

#include <stdio.h>

void main(){

	int rows = 0;
	printf("Enter no of rows:");
	scanf("%d",&rows);

	int s = rows;
	int p = 1;
	int y = 1;

	for(int i = 1; i <= rows*2-1; i++){
	int x = 1;
		if(rows >= i){
			s--;
		}else{
			s++;
		}
		for(int spa = 1; spa <= s; spa++){
			printf("  \t");
		}
		if(rows >= i){
			p = i*2-1;
		}else{
			p = p - 2;

		}
		for(int j = 1; j <= p; j++){
			printf(" \t%d",x);
		if( y > j){
			x++;
		}else{
			x--;
		}
		}
		printf("\n");
		if( rows > i){
			y++;
		}else{
			y--;
		}
	}
}
