/*	*	*	*	*	*	*	*
 *	*	*	*		*	*	*
 *	*	*				*	*
 *	*						*
 *	*	*				*	*
 *	*	*	*		*	*	*
 *	*	*	*	*	*	*	*
 */

#include <stdio.h>

void main(){

	int rows = 0;
	printf("Enter no of rows:");
	scanf("%d",&rows);

	int s = rows+1;
	int p = 1;
	int x = 1;

	for(int i = 1; i <= rows*2-1; i++){
		if(rows >= i){
			s--;
		}else{
			s++;
		}
		for(int spa = 1; spa <= s; spa++){
			printf("  \t*");
		}
		if(rows >= i){
			p = i*2-1;
		}else{
			p = p - 2;

		}
		for(int j = 1; j <= p; j++){
			printf(" \t");
		}
		for(int sta = 1; sta <= s; sta++){
			printf(" *\t");

		}
		printf("\n");
		if(rows > i){
			x++;
		}else{
			x--;
		}
	}
}
