/*	4
 *	3	3
 *	2	2	2
 *	1	1	1	1
 *	2	2	2
 *	3	3
 *	4
 */
#include <stdio.h>

void main(){

       	int rows = 0;
	printf("Enter no of rows:");
	scanf("%d",&rows);

	int x = rows,s = 1;
	for(int i = 1; i <= rows*2-1; i++){
		int x1 = x;
		for(int j = 1; j <= s; j++){
			printf(" %d",x1);
		}
		if(rows <= i){
			s--;
			x++;
		}else{
			s++;
			x--;
		}
		printf("\n");
	}
}
