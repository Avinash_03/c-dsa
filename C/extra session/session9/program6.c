/*	*
 *	*	*	*
 *	*	*	*	*	*
 *	*	*	*
 *	*
 */
#include <stdio.h>

void main(){

       	int rows = 0;
	printf("Enter no of rows:");
	scanf("%d",&rows);
	int s = 1;

	for(int i = 1; i <= rows*2-1; i++){
		for(int j = 1; j <= s; j++){
			printf(" *");
		}
		if(rows <= i){
			s = s-2;
		}else{
			s = s+2;
		}
		printf("\n");
	}
}
