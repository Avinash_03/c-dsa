/*	D
 *	C	D
 *	B	C	D
 *	A	B	C	D
 *	B	C	D
 *	C	D
 *	D
 */
#include <stdio.h>

void main(){

       	int rows = 0;
	printf("Enter no of rows:");
	scanf("%d",&rows);
	char ch = 64+rows;

	int s = 1;
	for(int i = 1; i <= rows*2-1; i++){
		char ch1 = ch;
		for(int j = 1; j <= s; j++){
			printf(" %c",ch1);
			ch1++;
		}
		if(rows <= i){
			s--;
			ch++;
		}else{
			s++;
			ch--;
		}
		printf("\n");
	}
}
