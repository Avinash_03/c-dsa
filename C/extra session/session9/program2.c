/*	1
 *	1	2
 *	1	2	3
 *	1	2	3	4
 *	1	2	3
 *	1	2
 *	1
 */
#include <stdio.h>

void main(){

	int rows =0;
	printf("Enter no of rows:");
	scanf("%d",&rows);
	int s = 0;

	for(int i = 1; i <= rows*2-1; i++){
		int x = 1;
		if(rows < i)
			s--;
		else
			s++;

		for(int j = 1; j <= s; j++){
			printf(" %d",x);
			x++;
		}
		printf("\n");
	}
}
