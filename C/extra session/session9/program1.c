/*	*
 *	*	*
 *	*	*	*
 *	*	*	*	*
 *	*	*	*
 *	*	*	
 *	*
 */

#include <stdio.h>

void main(){

	int rows = 0;
	printf("Enter no of rows:");
	scanf("%d",&rows);
	int s = 0;

	for(int i = 1; i <= rows*2-1; i++){
		if(rows < i)
			s--;
		else
			s++;
		for(int j = 1; j <= s; j++){
			printf(" *");
		}
		printf("\n");
	}
}
