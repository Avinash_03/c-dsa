/*	1
 *	3	2	1
 *	5	4	3	2	1
 *	3	2	1
 *	1
 */
#include <stdio.h>

void main(){

       	int rows = 0;
	printf("Enter no of rows:");
	scanf("%d",&rows);
	int s = 1;
	int x = 1;

	for(int i = 1; i <= rows*2-1; i++){
		int x1 = x;
		for(int j = 1; j <= s; j++){
			printf(" %d",x1);
			x1--;
		}
		if(rows <= i){
			s = s-2;
			x = x-2;
		}else{
			s = s+2;
			x = x+2;
		}
		printf("\n");
	}
}
