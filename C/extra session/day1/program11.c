/*   1 2 3 4 
 *   a b c d
 *   5 6 7 8
 *   d e f g
 */

#include <stdio.h>

void main(){

	int x = 1;
	char ch = 'a';

	for(int i = 1; i <= 4; i++){
		for(int j = 1; j <=4; j++){
			if(i % 2 != 0){
				printf("%d ",x);
			        x++;
			} else {

				printf("%c ",ch);
				ch++;
			}
		}
		printf("\n");
	}
}
