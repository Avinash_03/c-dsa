/*  9 8 7 
    6 5 4
    3 2 1
 */

#include<stdio.h>

void main(){

	int row,col;
	printf("Enter row:");
	scanf("%d",&row);
	printf("Enter col:");
	scanf("%d",&col);

	int x = row*col;

	for(int i = 1; i <= row; i++){
		for(int j = 1; j <= col; j++){
			printf("%d ",x);
			x--;
		}
		printf("\n");
	}
}
