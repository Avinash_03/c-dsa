/*	take no of rows from user
 *	1	
 *	4	9
 *	64	125	216
 *	2401	4096	6561	10000
 */
#include <stdio.h>
#include <math.h>

void main(){

	int rows = 0;
	printf("Enter no of rows:");
	scanf("%d",&rows);
	int x = 1;
	int y ;

	for(int i = 1 ; i <= rows ; i++){
		for(int j = 1; j <=i; j++){
			y = pow(x,i);
			printf("%d\t",y);
			x++;
		}
		printf("\n");
	}
}
