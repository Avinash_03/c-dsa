/*	take no of rows from user 
 *				4
 *			3	6
 *		2	4	6
 *	1	2	3	4
 */
#include <stdio.h>

void main(){

	int rows = 0;

	printf("Enter no of user:");
	scanf("%d",&rows);

	int x = rows;

	for(int i = 1; i <= rows; i++){
		int x1 = x;
		for(int space = rows ; space > i ; space--){
			printf(" \t");
		}
		for(int j = 1; j <= i; j++){
			x1 = x*j;
			printf("%d\t",x1);

		}
		printf("\n");
		x--;
	}
}
