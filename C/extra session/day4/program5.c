/*	take no of rows from user 
 *				D
 *			c	D
 *		B	c	D
 *	a	B	c	D
 */
#include <stdio.h>

void main(){

	int rows = 0;

	printf("Enter no of rows:");
	scanf("%d",&rows);

	char ch2= 64+rows, ch3 = 96+rows;

	for(int i = 1; i <= rows ; i++){
	char ch = ch2, ch1 = ch3;

		for(int space = rows ; space > i ; space--){
			printf(" \t");
		}
		for(int j = 1; j <= i ; j++){
			if(j%2!=0 && i%2!=0 || j%2==0 && i%2==0){
				printf("%c\t",ch);
				ch1++;
				ch++;

			} else {
				printf("%c\t",ch1);
				ch++;
				ch1++;
			}
		}
		printf("\n");
		ch2--;
		ch3--;
	}
}
