/*	take no of rows from user
 *			1
 *		4	7
 *	10	13	16
 */
#include <stdio.h>

void main(){

	int rows = 0;

	printf("Enter no of rows:");
	scanf("%d",&rows);

	int x = 1;

	for(int i = 1; i <= rows ; i++){
		for(int space = rows ; space > i ; space--){
			printf(" \t");
		}
		for(int j = 1; j <= i ; j++){
			printf("%d\t",x);
			x = x + rows;
		}
		printf("\n");
	}
}
