/*	take no of rows from user
 *				4
 *			4	3
 *		4	3	2
 *	4	3	2	1
 */
#include <stdio.h>

void main(){

	int rows = 0;
	printf("Enter no of rows:");
	scanf("%d",&rows);

	for(int i = 1; i <= rows ; i++){
		int x = rows;
		for(int space = rows ; space > i ; space--){
			printf(" \t");
		}
		for(int j = 1; j <= i ; j++){
			printf("%d\t",x);
			x--;
		}
		printf("\n");
	}
}
