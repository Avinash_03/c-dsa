/*	TAKE NO OF ROWS FROM USER
 *            5
 *         5  6
 *      5  4  3
 *   5	6  7  8
 *5  4  3  2  1
 */
#include <stdio.h>

void main(){

	int rows = 0;

	printf("Enter no of rows:");
	scanf("%d",&rows);

	for(int i = 1; i <= rows; i++){
		int x = rows;
		for(int space = rows; space > i; space--){
			printf(" \t");
		}
		for(int j = 1; j <= i; j++){
			if(i%2!=0){
				printf("%d\t",x);
				x--;
			}else{
				printf("%d\t",x);
				x++;
			}
		}
		printf("\n");
	}
}


