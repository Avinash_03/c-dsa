/*	take no of rows from user
 *				1
 *			A	b
 *	  	1	2	3
 *	  A	b	C	d
 */
#include <stdio.h>

void main(){
	int rows = 0; 
	printf("Enter no of rows:");
	scanf("%d",&rows);

	for(int i = 1; i <= rows ; i++){
		int x = 1;
		char ch = 'A',ch1 = 'a';
		for(int space = rows ; space > i ; space--){
			printf(" \t");
		}
		for(int j = 1; j <= i ; j++){
			if(i%2==1){
				printf("%d\t",x);
				x++;
			}else if(i%2==0 && j%2==1 || i%2==1 && j%2==0){
				printf("%c\t",ch);
				ch++;
				ch1++;
			} else{
				printf("%c\t",ch1);
				ch++;
				ch1++;
			}
		}
		printf("\n");
	}
}
