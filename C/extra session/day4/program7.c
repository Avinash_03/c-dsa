/*	take no of rows from user
 *	          A
 *	       b  a
 *	   C   E  G
 *	d  c   b  a
 *   E  G  I   K  M
 */
#include <stdio.h>

void main(){

	int rows = 0;

	printf("Enter no of rows:");
	scanf("%d",&rows);

	char ch2 = 65, ch3 = 97;

	for(int i = 1; i <= rows ; i++){
		char ch = ch2, ch1 = ch3;

		for(int space = rows ; space > i ; space--){
			printf(" \t");
		}
		for(int j = 1; j<=i ; j++){
			if(i%2==1){
				printf("%c\t",ch);
				ch = ch + 2;

			}else{
				printf("%c\t",ch1);
				ch1--;
			}
		}
		printf("\n");
		ch2++;
		ch3++;
	}
}
