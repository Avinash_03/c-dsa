/*take no of rows from user 
 * 	1	2	3	4
 * 	a	b	c	d
 * 	#	#	#	#
 * 	5	6	7	8
 * 	e	f	g	h
 * 	#	#	#	#
 */
#include<stdio.h>

void main(){

	int rows;
	printf("Enter no of rows:");
	scanf("%d",&rows);
	int flag = 1;
	int num = 1;
	char ch ='a';
        
	for(int i = 1; i <= rows; i++){
		for(int j = 1; j<=rows; j++){
			if(flag==1){
				printf("%d\t",num);
				num++;

			}else if(flag==2){
				printf("%c\t",ch);
				ch++;

			}else{
				printf("#\t");

			}
		}
		printf("\n");
		flag++;
		if(flag==4)
			flag = 1;
	}
}
