/* take no of rows from rows
 * 	1	3	5
 * 	5	7	9	
 * 	9	11	13
 */

#include <stdio.h>

void main(){
	int rows, x = 1;
	printf("Enter no of rows:");
	scanf("%d",&rows);

	for(int i = 1; i <= rows; i++){
		for(int j = 1; j <= rows; j++){
			printf("%d\t",x);
			x = x+2;

		}
		printf("\n");
		x = x-2;
	}
}
