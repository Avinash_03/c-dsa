/*take no of rows from user 
 * 	1	2	3
 * 	2	3	4
 * 	3	4	5
 */

#include<stdio.h>

void main(){

	int rows, x = 1;

	printf("Enter no of rows:");
	scanf("%d",&rows);

	for(int i = 1; i <= rows; i++){
		int x1 = x;
		for(int j = 1; j <= rows; j++){
			printf("%d\t",x1);
			x1++;
		}
		printf("\n");
		x++;
	}
}
