//wap to print the odd number as it is and cube of even numbers btn a given range from the user.

#include <stdio.h>

void main(){

	int start,end;
	printf("Enter start value:");
	scanf("%d",&start);

	printf("Enter end value:");
	scanf("%d",&end);

	for(int i = start; i <=end; i++)
        	if(i%2==0){
			printf("%d ",i*i*i);
		} else {
			printf("%d ",i);
		}
	printf("\n");
}
