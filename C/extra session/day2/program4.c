/*take no of rows from the user 
	d	d	d	d
	c	c	c	c
	b	b	b	b
	a	a	a	a
*/
#include<stdio.h>

void main(){
	int rows;

	printf("Enter rows:");
	scanf("%d",&rows);
	char ch = 97+(rows-1);

	for(int i = 1; i <= rows; i++){
		for(int j  = 1; j <= rows; j++){
			printf("%c\t",ch);
		}
		printf("\n");
		ch--;
	}
}
