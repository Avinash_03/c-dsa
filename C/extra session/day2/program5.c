/*take no of rows from user
   	a	B	c
	d	E	f
	g	H	i
 */
#include<stdio.h>

void main(){

	int rows;
	printf("Enter no rows:");
	scanf("%d",&rows);
	char ch = 97,ch1 = 65;

	for(int i = 1; i <= rows; i++){
		for(int j = 1; j <= rows; j++){
			if(j%2==1){
				printf("%c\t",ch);
				ch++;
				ch1++;
			} else {
				printf("%c\t",ch1);
				ch++;
				ch1++;
			}
		}
		printf("\n");
	}
}
