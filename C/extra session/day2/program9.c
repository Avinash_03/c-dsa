/*take no of rows from user
 * 	A	b	C
 * 	d	E	f
 * 	G	h	I
 */
#include <stdio.h>

void main(){

	int rows;
	printf("Enter no of rows:");
	scanf("%d",&rows);
	char ch = 65, ch1 = 97;

	for(int i = 1; i <= rows; i++){
		for(int j = 1; j <= rows; j++){
			if(ch%2==1){
				printf("%c\t",ch);
				ch++;
				ch1++;
			}else{
				printf("%c\t",ch1);
				ch++;
				ch1++;
			}
		}
		printf("\n");
	}
}
