/*take no of rows from user
    1	4    7
    10  13   16
    19  22   25
*/

#include<stdio.h>

void main(){

	int x=1,rows=0;

	printf("Enter no rows:");
	scanf("%d",&rows);

	for(int i = 1; i <= rows; i++){
		for(int j = 1; j <= rows ; j++){
			printf("%d\t",x);
			x = x+3;
		}
		printf("\n");
	}
}
