/*					1
 *			1	2	3
 *	1	2	3	4	5
 *			1	2	3
 *					1
 *				
 */
#include <stdio.h>

void main(){

	int rows = 0; 
	printf("Enter no of rows:");
	scanf("%d",&rows);
	int r = rows;
	int s = 1;

	for(int i = 1; i <= rows+1; i++){
		int x = 1;
		for(int spa = 1; spa <= r; spa++){
			printf("  ");
		}
		if(rows-1 > i)
			r = r - 2;
		else
			r = r + 2;
		for(int j = 1; j <= s; j++){
			printf(" %d",x);
			x++;
		}
		if(rows-1 > i)
			s = s+2;
		else
			s = s-2;
		printf("\n");
	}
}
