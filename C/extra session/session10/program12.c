/*				1
 *			1	2
 *		2	3	4
 *	4	5	6	7
 *		7	8	9
 *			9	10
 *				10
 */

#include <stdio.h>

void main(){

	int rows = 0; 
	printf("Enter no of rows:");
	scanf("%d",&rows);
	int s = rows;
	int p = 0;
	int x = 1;

	for(int i = 1; i <= rows*2-1; i++){
		if(rows >= i)
			s--;
		else
			s++;
		for(int spa = 1; spa <= s; spa++){
			printf("  \t");
		}
		if(rows >= i)
			p++;
		else
			p--;

		for(int j = 1; j <= p; j++){
			printf(" \t%d",x);
			x = x+1;
		}
		printf("\n");
		x = x-1;
	}
}
