/*				1
 *			1	4
 *		4	7	10
 *	10	13	16	19
 *		19	22	25
 *			25	28
 *				28
 *
 */

#include <stdio.h>

void main(){

	int rows = 0; 
	printf("Enter no of rows:");
	scanf("%d",&rows);
	int s = rows;
	int p = 0;
	int x = 1;

	for(int i = 1; i <= rows*2-1; i++){
		if(rows >= i)
			s--;
		else
			s++;
		for(int spa = 1; spa <= s; spa++){
			printf("  \t");
		}
		if(rows >= i)
			p++;
		else
			p--;

		for(int j = 1; j <= p; j++){
			printf(" \t%d",x);
			x = x+3;
		}
		printf("\n");
		x = x-3;
	}
}
