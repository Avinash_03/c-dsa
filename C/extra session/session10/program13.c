/*				A
 *			a	b
 *		B	C	D
 *	d	e	f	g
 *		G	H	I
 *			i	j
 *				j
 */

#include <stdio.h>

void main(){

	int rows = 0; 
	printf("Enter no of rows:");
	scanf("%d",&rows);
	int s = rows;
	int p = 0;
	char x = 'A';
	char x1 = 'a';

	for(int i = 1; i <= rows*2-1; i++){
		if(rows >= i)
			s--;
		else
			s++;
		for(int spa = 1; spa <= s; spa++){
			printf("  \t");
		}
		if(rows >= i)
			p++;
		else
			p--;

		for(int j = 1; j <= p; j++){
			if(i%2==1){
				printf(" \t%c",x);
				x = x+1;
				x1++;
			}else{
				printf(" \t%c",x1);
				x1 = x1+1;
				x++;
			}
		}
		printf("\n");
		x = x-1;
		x1--;
	}
}
