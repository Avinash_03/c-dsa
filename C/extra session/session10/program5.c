/*					3
 *				2	3
 *			1	2	3
 *		0	1	2	3
 *			1	2	3
 *				2	3
 *					3
 */
#include <stdio.h>

void main(){

	int rows = 0; 
	printf("Enter no of rows:");
	scanf("%d",&rows);
	int r = rows-1;
	int s = 1;
	int x = rows-1;

	for(int i = 1; i <= rows*2-1; i++){
		int x1 = x;
		for(int spa = 1; spa <= r; spa++){
			printf("  ");
		}
		if(rows > i)
			r--;
		else
			r++;
		for(int j = 1; j <= s; j++){
			printf(" %d",x1);
			x1++;
		}
		if(rows > i){
			s++;
			x--;

		}else{
			s--;
			x++;
		}
		printf("\n");
	}
}
