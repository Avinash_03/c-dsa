//insert position
#include <stdio.h>

int rot(int *arr,int size,int key){
	int mid=0,start=0,end=size-1;
	int store=-1;
	while(start<=end){
		mid=(start+end)/2;
		if(arr[mid]==key){
			return mid; 
		}
		if(arr[mid]>key){
			store=mid;
			end=mid-1;
		}
		if(arr[mid]<key){
			store=mid+1;
			start=mid+1;
		}
	}
	return store;
}
int main(){
	int size,start,end,index;
	printf("Enter Array size:\n");
	scanf("%d",&size);
	int arr[size];

	printf("Enter array element:\n");
	for(int i = 0; i<= size-1;i++){
		scanf("%d",&arr[i]);
	}

	int key;
	printf("Enter Key value:\n");
	scanf("%d",&key);
		
	int ret=rot(arr,size,key);

	printf("%d is index\n",ret);
}
