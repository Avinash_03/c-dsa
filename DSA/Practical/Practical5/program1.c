//floor value
#include <stdio.h>

int floor1(int *arr,int size,int key){
	int start=0,end=size-1,mid=0;
	int store;
	while(start<=end){
		mid=(start+end)/2;
		if(arr[mid]==key){
			return arr[mid]; 
		}
		if(arr[mid]>key){
			end=mid-1;
		}
		if(arr[mid]<key){
			store=arr[mid];
			start=mid+1;
		}
	}
	return store;
}
void main(){
	int size;
	printf("Enter Array size:\n");
	scanf("%d",&size);
	int arr[size];

	printf("Enter array element:\n");
	for(int i = 0; i<= size-1;i++){
		scanf("%d",&arr[i]);
	}

	int key;
	printf("Enter Key value:\n");
	scanf("%d",&key);
	int ret=floor1(arr,size,key);

	printf("%d is floor value of %d\n",ret,key);
}
