//first occurence
#include <stdio.h>

int firstocc(int *arr,int size,int key){
	int start=0,end=size-1,mid=0;
	int store=-1;
	while(start<=end){
		mid=(start+end)/2;
		if(arr[mid]==key){
			store = arr[mid]; 
		}
		if(arr[mid]>=key){
			end=mid-1;
		}
		if(arr[mid]<key){
			store=arr[mid];
			start=mid+1;
		}
	}
	return store;
}
void main(){
	int size;
	printf("Enter Array size:\n");
	scanf("%d",&size);
	int arr[size];

	printf("Enter array element:\n");
	for(int i = 0; i<= size-1;i++){
		scanf("%d",&arr[i]);
	}

	int key;
	printf("Enter Key value:\n");
	scanf("%d",&key);
	int ret=firstocc(arr,size,key);

	printf("%d is first occurence at index %d\n",key,ret);
}
