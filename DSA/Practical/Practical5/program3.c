//rotate array value
#include <stdio.h>

int rot(int *arr,int start,int end,int key){
	int mid=0;
	while(start<=end){
		mid=(start+end)/2;
		if(arr[mid]==key){
			return mid; 
		}
		if(arr[mid]>key){
			end=mid-1;
		}
		if(arr[mid]<key){
			start=mid+1;
		}
	}
	return -1;
}
void main(){
	int size,start,end,index;
	printf("Enter Array size:\n");
	scanf("%d",&size);
	int arr[size];

	printf("Enter array element:\n");
	for(int i = 0; i<= size-1;i++){
		scanf("%d",&arr[i]);
	}

	int key;
	printf("Enter Key value:\n");
	scanf("%d",&key);

	for(int i=0; i<=size-1;i++){
		index=i;
		if(arr[i]>arr[i+1])
			break;
		
	}
	if(key>arr[size-1]){
		start=0;
		end=index;
	}else{
		start=index+1;
		end=size-1;
	}
		
	int ret=rot(arr,start,end,key);

	printf("%d is index of %d\n",ret,key);
}
