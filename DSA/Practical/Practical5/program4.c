//single element in sorted array
#include <stdio.h>

int rot(int *arr,int size){
	int start=0;
	int end=size-1;
	int mid=0;
	while(start<=end){
		mid=(start+end)/2;
		if(arr[mid] != arr[mid-1] && arr[mid] != arr[mid+1]){
			return arr[mid]; 
		}
		if(arr[mid]==arr[mid-1]){
			start=mid+1;
		}
		if(arr[mid]==arr[mid+1]){
			end=mid-1;
		}
	}
	return -1;
}
int main(){
	int size;
	printf("Enter Array size:\n");
	scanf("%d",&size);
	int arr[size];

	printf("Enter array element:\n");
	for(int i = 0; i<= size-1;i++){
		scanf("%d",&arr[i]);
	}
		
	int ret=rot(arr,size);

	printf("%d is a single value\n",ret);
}
