//peak value in a array
#include <stdio.h>

int peak(int *arr,int size){
	int large;
	for(int i=0;i<size;i++){
		if(arr[i]>large){
			large=arr[i];
		}
	}
	return large;
}
void main(){
	int size,start,end,index;
	printf("Enter Array size:\n");
	scanf("%d",&size);
	int arr[size];

	printf("Enter array element:\n");
	for(int i = 0; i<= size-1;i++){
		scanf("%d",&arr[i]);
	}
	int ret=peak(arr,size);

	printf("%d is a peak value:\n",ret);
}
