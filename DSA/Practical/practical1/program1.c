/*	wap for the linkedlist of malls consisting of its name,number of shops, & revenue; connect 3 malls in the linkedlist & printf their data
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct mall{
	char mName[20];
	int number;
	float rev;
	struct mall *next;
}mall;

mall *head = NULL;

void addNode(){
	mall *newNode = (mall*)malloc(sizeof(mall));

	printf("Enter no of mall:");
	fgets(newNode -> mName,15,stdin);

	char *change = newNode -> mName;
	int len = strlen(change);
	change[len-1] = '\0';

	printf("Enter no of shop:");
	scanf("%d",&newNode -> number);

	printf("Enter revenue:");
	scanf("%f",&newNode -> rev);

	getchar();

	if(head == NULL){
		head = newNode;
	}else{
		mall *temp = head;
		while(temp->next != NULL){
			temp = temp -> next;
		}
		temp -> next = newNode;
	}
}

void printLL(){

	mall *temp = head;
	while(temp != NULL){
		printf("|%s ->",temp -> mName);
		printf("%d ->",temp -> number);
		printf("%f| ",temp -> rev);
		temp = temp -> next;
	}
	printf("\n");

}
void main(){
	addNode();
	addNode();
	addNode();

	printLL();
}
