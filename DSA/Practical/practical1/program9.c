/*	write a demo structure consisting of integer data take the no of nodes from user & check the prime number present in the data
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void primeNo();

typedef struct demo{
	int num;
	struct demo *next;
}demo;

demo *head = NULL;

void addNode(){
	demo *newNode = (demo*)malloc(sizeof(demo));

	getchar();

	printf("Enter number:");
	scanf("%d",&newNode -> num);

	if(head == NULL){
		head = newNode;
	}else{
		demo *temp = head;
		while(temp->next != NULL){
			temp = temp -> next;
		}
		temp -> next = newNode;
	}
}

void printLL(){

	demo *temp = head;
	int max = temp -> num;
	while(temp != NULL){
		printf("|%d |",temp -> num);
		temp = temp -> next;
	}
	printf("\n");
}

void primeNo(){

	demo *temp= head;
	int count = 0;
	while(temp != NULL){
		for(int i = 2; i < temp->num ; i++){
			if(temp->num%i==0){
				count++;
			}
		}
		if(count==0)
			printf("%d is prime number:\n",temp->num);	

			count =0;
			temp = temp->next;
	}

}
void main(){

	int nodes = 0;
	printf("Enter no of nodes:");
	scanf("%d",&nodes);
	
	for(int i = 1; i <= nodes; i++){
		addNode();
	}
	printLL();
	primeNo();
}
