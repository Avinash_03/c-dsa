/*	write a demo structure consisting of integer data take the no of nodes from user & print the addition of the first and last node
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct demo{
	int num;
	struct demo *next;
}demo;

demo *head = NULL;

void addNode(){
	demo *newNode = (demo*)malloc(sizeof(demo));

	getchar();

	printf("Enter number:");
	scanf("%d",&newNode -> num);

	if(head == NULL){
		head = newNode;
	}else{
		demo *temp = head;
		while(temp->next != NULL){
			temp = temp -> next;
		}
		temp -> next = newNode;
	}
}

void printLL(){

	int add = 0;
	int count = 1;
	demo *temp = head;
	while(temp != NULL){
		printf("|%d |",temp -> num);

		if(count == 1 || (temp -> next == NULL))
			add = add + temp -> num;
		temp = temp -> next;
		count++;
	}
	printf("\naddition of integer is %d\n",add);

}
void main(){

	int nodes = 0;
	printf("Enter no of nodes:");
	scanf("%d",&nodes);
	
	for(int i = 1; i <= nodes; i++){
		addNode();
	}
	printLL();
}
