/*	write a demo structure consisting of integer data take the no of nodes from user & print the maximum integer data node
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct demo{
	int num;
	struct demo *next;
}demo;

demo *head = NULL;

void addNode(){
	demo *newNode = (demo*)malloc(sizeof(demo));

	getchar();

	printf("Enter number:");
	scanf("%d",&newNode -> num);

	if(head == NULL){
		head = newNode;
	}else{
		demo *temp = head;
		while(temp->next != NULL){
			temp = temp -> next;
		}
		temp -> next = newNode;
	}
}

void printLL(){

	int max = 0;
	demo *temp = head;
	while(temp != NULL){
		printf("|%d |",temp -> num);

		if(temp -> num >= max){
			max =  temp -> num;
		}
		temp = temp -> next;
	}
	printf("\nMaximum integer is %d\n",max);

}
void main(){

	int nodes = 0;
	printf("Enter no of nodes:");
	scanf("%d",&nodes);
	
	for(int i = 1; i <= nodes; i++){
		addNode();
	}
	printLL();
}
