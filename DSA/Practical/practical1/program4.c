/*	wap to count the number of festival  nodes.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct festivals{
	char fName[20];
	int fmonthno;
	float exp;
	struct festivals *next;
}fest;

fest *head = NULL;

void addNode(){
	fest *newNode = (fest*)malloc(sizeof(fest));
	
	printf("Enter festival name:");
//	fgets(newNode -> mName,15,stdin);
	char ch;
	int i = 0;

	while((ch = getchar()) != '\n'){

		(*newNode).fName[i]=ch;
		i++;
	}

	printf("Enter no of month:");
	scanf("%d",&newNode -> fmonthno);

	printf("Enter expense:");
	scanf("%f",&newNode -> exp);

	getchar();

	if(head == NULL){
		head = newNode;
	}else{
		fest *temp = head;
		while(temp->next != NULL){
			temp = temp -> next;
		}
		temp -> next = newNode;
	}
}

void printLL(){

	fest *temp = head;
	while(temp != NULL){
		printf("|%s ->",temp -> fName);
		printf("%d ->",temp -> fmonthno);
		printf("%f| ",temp -> exp);
		temp = temp -> next;
	}
}
void count(){

	int count = 0;
	fest *temp = head;

	while(temp != NULL){ 

		count++;
		temp = temp -> next;
	}
	printf("\n count od Node %d\n",count);

}
void main(){
	addNode();
	addNode();
	addNode();

	printLL();
	count();
}
