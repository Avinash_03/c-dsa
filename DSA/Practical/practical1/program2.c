/*	wap for the linkedlist of states in india consisting of its name,population, budget & litracy; connect 4 states in the linkedlist & printf their data
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct states{
	char sName[20];
	int pop;
	float bud;
	float lit;
	struct states *next;
}sta;

sta *head = NULL;

void addNode(){
	sta *newNode = (sta*)malloc(sizeof(sta));

	printf("Enter name of state:");
	fgets(newNode -> sName,15,stdin);

	char *change = newNode -> sName;
	int len = strlen(change);
	change[len-1] = '\0';

	printf("Enter no of population:");
	scanf("%d",&newNode -> pop);

	printf("Enter budget:");
	scanf("%f",&newNode -> bud);

	printf("Enter litracy:");
	scanf("%f",&newNode -> lit);

	getchar();

	if(head == NULL){
		head = newNode;
	}else{
		sta *temp = head;
		while(temp->next != NULL){
			temp = temp -> next;
		}
		temp -> next = newNode;
	}
}

void printLL(){

	sta *temp = head;
	while(temp != NULL){
		printf("|%s ->",temp -> sName);
		printf("%d ->",temp -> pop);
		printf("%f -> ",temp -> bud);
		printf("%f| ",temp -> lit);
		temp = temp -> next;
	}
	printf("\n");

}
void main(){
	addNode();
	addNode();
	addNode();
	addNode();

	printLL();
}
