/*	write a demo structure consisting of integer data take the no of nodes from user & print the minimum integer data node
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct demo{
	int num;
	struct demo *next;
}demo;

demo *head = NULL;

void addNode(){
	demo *newNode = (demo*)malloc(sizeof(demo));

	getchar();

	printf("Enter number:");
	scanf("%d",&newNode -> num);

	if(head == NULL){
		head = newNode;
	}else{
		demo *temp = head;
		while(temp->next != NULL){
			temp = temp -> next;
		}
		temp -> next = newNode;
	}
}

void printLL(){

	demo *temp = head;
	int min = temp -> num;
	while(temp != NULL){
		printf("|%d |",temp -> num);

		if(temp -> num <= min){
			min =  temp -> num;
		}
		temp = temp -> next;
	}
	printf("\nMinimum integer is %d\n",min);

}
void main(){

	int nodes = 0;
	printf("Enter no of nodes:");
	scanf("%d",&nodes);
	
	for(int i = 1; i <= nodes; i++){
		addNode();
	}
	printLL();
}
