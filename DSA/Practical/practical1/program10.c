/*	wap real time example for a linked list and print its data take 5 nodes from the user 
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct class{
	char cName[20];
	int Scount;
	float fees;
	struct class *next;
}class;

class *head = NULL;

void addNode(){
	class *newNode = (class*)malloc (sizeof(class));
	printf("Enter class name:");

	char ch = 0;
	int i = 0;

	while((ch = getchar()) != '\n'){
		(*newNode).cName[i]= ch;
		i++;
	}
	printf("Enter student count:");
	scanf("%d",&newNode->Scount);

	printf("Enter fees:");
	scanf("%f",&newNode->fees);

	newNode -> next = NULL;

	if(head == NULL){
		head = newNode;
	}else{
		class *temp = head;
		while(temp -> next != NULL){
			temp = temp->next;
		}
		temp -> next = newNode;
	}
	getchar();
}

void printfLL(){

	class *temp = head;
	while(temp != NULL){
		printf("|%s ",temp->cName);
		printf("%d ",temp->Scount);
		printf("%f |",temp->fees);

		temp = temp -> next;
	}
	printf("\n");
}

void main(){

	addNode();
	addNode();
	addNode();
	addNode();
	addNode();

	printfLL();
}
