//prifix sum
#include<stdio.h>

void PrifixSum(int *arr,int N){
	int prifsum=arr[0];
	for(int i= 1; i<N;i++){
		arr[i]+=prifsum;
		prifsum =arr[i];
	}
}

void main(){

	int size;
	printf("Enter Array Size:");
	scanf("%d",&size);

	int arr[size];
	printf("Enter Array Element:");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}

	PrifixSum(arr,size);
	for(int i=0;i<size;i++){
		printf("%d ",arr[i]);
	}
	printf("\n");
}
