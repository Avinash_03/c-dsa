#include<stdio.h>

int CntEle(int *arr,int N,int i,int j,int B){

	if(i > N){
		return 0;
	}
	if( i!=j && arr[i]+arr[j] == B){
		return 1;
	}
	if(j == N){
		j=-1;
		i=i+1;
	}
	 return CntEle(arr,N,i,j+1,B);
}

void main(){

	int size,B;
	printf("Enter Array Size:");
	scanf("%d",&size);

	int arr[size];
	printf("Enter Array Element:");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}

	printf("Enter Number For Campair:");
	scanf("%d",&B);

	int x = CntEle(arr,size-1,0,0,B);
	printf("Count is:%d\n",x);
}
