//Campair two matrix same or not
#include<stdio.h>

int IsMatsame(int N,int (*arr)[N],int (*arr1)[N]){
	for(int i= 0; i<N; i++){
		for(int j =0; j<N; j++){
			if(arr[i][j] != arr1[i][j])
				return 0;
		}
	}
	return 1;
}

void main(){

	int size;
	printf("Enter Array Size:");
	scanf("%d",&size);

	int arr[size][size];
	int arr1[size][size];

	printf("Enter Array1 Element:");
	for(int i=0;i<size;i++){
		for(int j=0;j<size;j++){
			scanf("%d",&arr[i][j]);
		}
		printf("\n");
	}

	printf("Enter Array2 Element:");
	for(int i=0;i<size;i++){
		for(int j=0;j<size;j++){
			scanf("%d ",&arr1[i][j]);
		}
		printf("\n");
	}
	int cheak=IsMatsame(size,arr,arr1);
	printf("%d\n",cheak);
}
