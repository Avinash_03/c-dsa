#include<stdio.h>

int CntEle(int *arr,int N){
	int cnt=0;
	for(int i= 0; i<N;i++){
		for(int j= 0; j<N; j++){
			if(arr[i]<arr[j]){
				cnt++;
				break;
			}
		}
	}
	return cnt;
}

void main(){

	int size;
	printf("Enter Array Size:");
	scanf("%d",&size);

	int arr[size];
	printf("Enter Array Element:");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	int x = CntEle(arr,size);
	printf("Count is:%d\n",x);
}
