#include<stdio.h>
//1 2 3 4 5
void ArrRot(int *arr,int N,int Rot){
	int arr1[Rot];
	int j= 0;
	for(int i=N-Rot; i!=N; i++){
		arr1[j]=arr[i];
		j++;
	}
	j=0;
	for(int i =0; i<N; i++){
		int temp=arr[i];
		arr[i]=arr1[j];
		arr1[j]=temp;
		j++;

		if(j==Rot)
			j=0;
	}

	for(int i=0; i<N; i++){
		printf("%d ",arr[i]);
	}
}

void main(){

	int size,Rot;
	printf("Enter Array Size:");
	scanf("%d",&size);

	int arr[size];
	printf("Enter Array Element:");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}

	printf("Enter Rotation Number:");
	scanf("%d",&Rot);

	if(Rot%size == 0){
		for(int i=0; i<size; i++){
			printf("%d ",arr[i]);
		}
	}else{
		int count=0;
	//	while(Rot%size!=0){
	//		Rot--;
	//		count++;
	//	}
		count=Rot/size;
		count=count*size;
		count=Rot-count;	
		ArrRot(arr,size,count);
	}
}
