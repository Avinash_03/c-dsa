//Leader in array
#include<stdio.h>
int LeaderEle(int *arr,int N){
	int arr1[N];
	int x=0;
	for(int i= 0; i<N;i++){
		int flag=0;
		for(int j= i; j<N; j++){
			if(arr[i]<arr[j]){
				flag=1;
			}
		}
		if(flag==0)
			arr1[x++]=arr[i];
	}
	for(int i= 0; i<x; i++){
		printf("%d ",arr1[i]);
	}
}

void main(){

	int size;
	printf("Enter Array Size:");
	scanf("%d",&size);

	int arr[size];
	printf("Enter Array Element:");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	LeaderEle(arr,size);
}
