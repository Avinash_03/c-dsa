//Matrix transpose
#include<stdio.h>

void RtCZero(int N,int (*arr)[N],int (*arr1)[N]){
	for(int i= 0; i<N; i++){
		for(int j =0; j<N; j++){
			if(arr[i][j] == 0){
				for(int k=0;k<N;k++){
					for(int l=0;l<N;l++){
						if(j == l)
							arr1[k][l]=0;
						if(i == k)
							arr1[k][l]=0;
					}
				}
			}
		}
	}
}

void main(){

	int size;
	printf("Enter Array Size:");
	scanf("%d",&size);

	int arr[size][size];
	int arr1[size][size];
	printf("Enter Array Element:");
	for(int i=0;i<size;i++){
		for(int j=0;j<size;j++){
			scanf("%d",&arr[i][j]);
			arr1[i][j]=arr[i][j];
		}
		printf("\n");
	}
	RtCZero(size,arr,arr1);

	for(int i=0;i<size;i++){
		for(int j=0;j<size;j++){
			printf("%d ",arr1[i][j]);

		}
		printf("\n");
	}
}
