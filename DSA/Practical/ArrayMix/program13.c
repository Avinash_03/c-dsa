//Matrix transpose
#include<stdio.h>

void MatTrans(int N,int (*arr)[N]){
	int Csum[N];
	for(int i= 0; i<N; i++){
		for(int j =i; j<N; j++){
			int temp=arr[i][j];
			arr[i][j]=arr[j][i];
			arr[j][i]=temp;
		}
	}
	for(int i=0;i<N;i++){
		for(int j=0;j<N/2;j++){
			int temp=arr[i][j];
			arr[i][j]=arr[i][N-i-1];
			arr[i][N-i-1]=temp;
		}
	}
}

void main(){

	int size;
	printf("Enter Array Size:");
	scanf("%d",&size);

	int arr[size][size];
	printf("Enter Array Element:");
	for(int i=0;i<size;i++){
		for(int j=0;j<size;j++){
			scanf("%d",&arr[i][j]);
		}
	}
	MatTrans(size,arr);

	for(int i=0;i<size;i++){
		for(int j=0;j<size;j++){
			printf("%d ",arr[i][j]);

		}
		printf("\n");
	}
}
