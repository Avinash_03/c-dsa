#include<stdio.h>

int CntEle(int *arr,int N,int i,int j){
	static int cnt=0;

	if(i > N){
		return cnt;
	}
	if(arr[i]<arr[j]){
		cnt++;
	//	return cnt;
	}
	if(j == N){
		j=-1;
		i=i+1;
	}
	 return CntEle(arr,N,i,j+1);
}

void main(){

	int size;
	printf("Enter Array Size:");
	scanf("%d",&size);

	int arr[size];
	printf("Enter Array Element:");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	int x = CntEle(arr,size-1,0,0);
	printf("Count is:%d\n",x);
}
