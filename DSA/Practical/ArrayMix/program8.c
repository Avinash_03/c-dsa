//column sum
#include<stdio.h>

void Rev(int N,int (*arr)[N]){
	int Csum[N];
	for(int i= 0; i<N; i++){
		int sum=0;
		for(int j=0; j<N; j++){
			sum+=arr[i][j];
		}
		Csum[i]=sum;
	}
	for(int i=0; i<N; i++){
		printf("%d ",Csum[i]);
	}
}

void main(){

	int size;
	printf("Enter Array Size:");
	scanf("%d",&size);

	int arr[size][size];
	printf("Enter Array Element:");
	for(int i=0;i<size;i++){
		for(int j=0;j<size;j++){
			scanf("%d",&arr[i][j]);
		}
	}

	Rev(size,arr);
}
