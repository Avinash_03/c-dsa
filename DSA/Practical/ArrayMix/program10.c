//Minor Diagonal sum
#include<stdio.h>
int DiagonalSum(int N,int (*arr)[N]){
	int Csum[N];
	int sum=0;
	for(int i= N-1; i>=0; i--){
		sum+=arr[i][i];
	}
	return sum;
}

void main(){

	int size;
	printf("Enter Array Size:");
	scanf("%d",&size);

	int arr[size][size];
	printf("Enter Array Element:");
	for(int i=0;i<size;i++){
		for(int j=0;j<size;j++){
			scanf("%d",&arr[i][j]);
		}
	}
	int sum=DiagonalSum(size,arr);
	printf("%d\n",sum);
}
