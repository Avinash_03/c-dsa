//sum of all sum array
#include<stdio.h>
int SubArrsum(int *arr,int N){
	int sum=0;
	for(int i= 0; i<N;i++){
		sum+=arr[i];
		for(int j= i; j<N; j++){
			sum+=arr[j];	
		}
	}
	return sum;
}

void main(){

	int size;
	printf("Enter Array Size:");
	scanf("%d",&size);

	int arr[size];
	printf("Enter Array Element:");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	int sum=SubArrsum(arr,size);
	printf("%d\n",sum);
}
