#include<stdio.h>

void Rev(int *arr,int N,int B,int C){
	int Store=0;
	for(int i= B; i<C; i++){
		Store= arr[B];
		arr[B]=arr[C];
		arr[C]=Store;
		B++;
		C--;
	}
	for(int i=0; i<N;i++){
		printf("%d ",arr[i]);
	}
}

void main(){

	int size,B,C;
	printf("Enter Array Size:");
	scanf("%d",&size);

	int arr[size];
	printf("Enter Array Element:");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	printf("Enter Start:");
	scanf("%d",&B);

	printf("Enter End");
	scanf("%d",&C);

	Rev(arr,size,B,C);
}
