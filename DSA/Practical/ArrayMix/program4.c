#include<stdio.h>

void Rev(int *arr,int N){
	int B=0;
	int C=N-1;
	int Store=0;
	for(int i= 0; i<N/2; i++){
		Store= arr[B];
		arr[B]=arr[C];
		arr[C]=Store;
		B++;
		C--;
	}
	for(int i=0; i<N;i++){
		printf("%d ",arr[i]);
	}
}

void main(){

	int size;
	printf("Enter Array Size:");
	scanf("%d",&size);

	int arr[size];
	printf("Enter Array Element:");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}

	Rev(arr,size);
}
