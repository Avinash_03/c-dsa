/* wap that searches all the palindrome data elements from a doubly linear linked list. and print the position of pelindrome data.
 */

#include <stdio.h>
#include <stdlib.h>

typedef struct Demo{
	struct Demo *prev;
	int data;
	struct Demo *next;
}Demo;

Demo *head = NULL;

Demo* createNode(){
	Demo *newNode = (Demo*)malloc(sizeof(Demo));

	newNode->prev=NULL;

	printf("Enter data:");
	scanf("%d",&newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	Demo *newNode = createNode();

	if(head==NULL){
		head=newNode;
	}else{
		Demo *temp = head;
		while(temp->next != NULL){
			temp=temp->next;
		}
		temp->next=newNode;
		newNode->prev=temp;
	}
}

void printLL(){
	if(head==NULL){
		printf("linke list is empty:\n");
	}else{
		Demo *temp = head;
		while(temp -> next != NULL){
			printf("|%d|-->",temp->data);
			temp=temp->next;
		}
		printf("|%d|\n",temp->data);
	}
}

void pelindrome(){

	Demo *temp = head;
	int data=1;
	int num=0;
	int prev=0;
	int pos = 1;
	while(temp != NULL){
		data=temp->data;
		while(data != 0){
		prev=data%10;
		num=num*10+prev;
		data=data/10;
		}
		if(num==temp->data)
			printf("palindrome found at %d\n",pos);
		temp=temp->next;
		 num=0;
		 prev=0;
		pos++;
	}
}

void main(){

	int pos=0;
	printf("Enter no of Nodes you want to create:");
	scanf("%d",&pos);

	for(int i = 1; i<= pos; i++){
		addNode();
	}

	printLL();

	pelindrome();
}
