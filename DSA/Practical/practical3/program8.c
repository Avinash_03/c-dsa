#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct node{
	struct node *prev;
	char str[20];
	struct node *next;
}node;

node *head=NULL;
node *createNode(){
	node *newNode = (node*)malloc(sizeof(node));
	newNode->prev=NULL;

	printf("Enter name:");
	char ch = 0;
	int i=0;
	while((ch = getchar()) != '\n'){
		(*newNode).str[i++]=ch;
	}

	newNode->next = NULL;

	return newNode;
}
void addNode(){
	node *newNode = createNode();

	if(head==NULL){
		head=newNode;
	}else{
		node *temp= head;

		while(temp->next != NULL){
			temp=temp->next;
		}
		temp->next=newNode;
		newNode->prev=temp;
	}
}
int count(){

	node *temp=head;

	int count=0;
	while(temp != NULL){
		count++;
		temp=temp->next;
	}
	return count;
}
void delfirst(){

	if(head== NULL){
		printf("node is empty:");
	}else if(head->next==NULL){
		free(head);
		head=NULL;
	}else{
		head=head->next;
		free(head->prev);
	}
}
void dellast(){
	if(head== NULL){
		printf("node is empty:");
	}else if(head->next==NULL){
		free(head);
		head=NULL;
	}else{
		node *temp= head;

		while(temp->next->next != NULL){
			temp=temp->next;
		}
		free(temp->next);
		temp->next =NULL;
	}
}
void delatpos(int pos){

	int cnt = count();
	if(pos <= 0 || pos > cnt){
		printf("invalide opration:\n");
	}else if(pos == 1){
		delfirst();
	}else if(pos == cnt){
		dellast();
	}else{

		node *temp=head;
		while(pos-2){
			temp=temp->next;
			pos--;
		}
		temp->next=temp->next->next;
		free(temp->next->prev);
		temp->next->prev=temp;
	}
}
void delnode(int data){
	//getchar();
	node *temp=head;
	int pos=1;
	while(temp != NULL){
		if(data == strlen(temp->str)){
			delatpos(pos--);
		}
		pos++;
		temp=temp->next;
	}
}
void printLL(){

	if(head==NULL){
		printf("invalide opration:");
	}else{
		node *temp = head;
		while(temp != NULL){
			printf("|%s| ",temp->str);
			temp=temp->next;
		}
		printf("\n");
	}
}
void main(){

	int node=0;
	printf("Enter Node :");
	scanf("%d",&node);

	getchar();

	for(int i = 1; i<= node; i++){
		addNode();
	}

	printLL();

	int data;
	printf("Enter node length");
	scanf("%d",&data);

	delnode(data);

	printLL();
}
