/* wap that searches for the  occurrence of a particular element from a doubly linear linked list.
 */

#include <stdio.h>
#include <stdlib.h>

typedef struct Demo{
	struct Demo *prev;
	int data;
	struct Demo *next;
}Demo;

Demo *head = NULL;

Demo* createNode(){
	Demo *newNode = (Demo*)malloc(sizeof(Demo));

	newNode->prev=NULL;

	printf("Enter data:");
	scanf("%d",&newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	Demo *newNode = createNode();

	if(head==NULL){
		head=newNode;
	}else{
		Demo *temp = head;
		while(temp->next != NULL){
			temp=temp->next;
		}
		temp->next=newNode;
		newNode->prev=temp;
	}
}

void printLL(){
	if(head==NULL){
		printf("linke list is empty:\n");
	}else{
		Demo *temp = head;
		while(temp -> next != NULL){
			printf("|%d|-->",temp->data);
			temp=temp->next;
		}
		printf("|%d|\n",temp->data);
	}
}

void occcnt(int data){

	Demo *temp = head;
	int pos=1;
	int cnt=0;

	while(temp != NULL){
		if(temp->data==data){
			cnt++;
		}
		pos++;
		temp=temp->next;
	}
	if(cnt>0)
		printf("%d\n",cnt);
	else
		printf("No data match:\n");
}

void main(){

	int pos=0;
	printf("Enter no of Nodes you want to create:");
	scanf("%d",&pos);

	for(int i = 1; i<= pos; i++){
		addNode();
	}

	printLL();

	int data=0;
	printf("Enter no you want to compair:");
	scanf("%d",&data);

	occcnt(data);
}
