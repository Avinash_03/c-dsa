/* wap a that searches for the first occurance of a particular element from a singly linear linked list.
 */
#include <stdio.h>
#include <stdlib.h>

typedef struct Demo{
	int data;
	struct Demo *next;
}Demo;

Demo *head = NULL;

Demo* createNode(){
	Demo *newNode = (Demo*)malloc(sizeof(Demo));

	printf("Enter data:");
	scanf("%d",&newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	Demo *newNode = createNode();

	if(head==NULL){
		head=newNode;
	}else{
		Demo *temp = head;
		while(temp->next != NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}

void printLL(){
	if(head==NULL){
		printf("linke list is empty:\n");
	}else{
		Demo *temp = head;
		while(temp -> next != NULL){
			printf("|%d|-->",temp->data);
			temp=temp->next;
		}
		printf("|%d|\n",temp->data);
	}
}

void firstocc(int data){

	Demo *temp = head;
	int pos=1;
	int firstocc=0;

	while(temp != NULL){
		if(temp->data==data){
			firstocc = pos;
			break;
		}
		pos++;
		temp=temp->next;
	}
	if(firstocc>0)
		printf("%d\n",firstocc);
	else
		printf("No data match:\n");
}

void main(){

	int pos=0;
	printf("Enter no of Nodes you want to create:");
	scanf("%d",&pos);

	for(int i = 1; i<= pos; i++){
		addNode();
	}

	printLL();

	int data=0;
	printf("Enter no you want to compair:");
	scanf("%d",&data);

	firstocc(data);
}
