#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct node{
	char str[20];
	struct node *next;
}node;

node *head=NULL;
node *createNode(){
	node *newNode = (node*)malloc(sizeof(node));
	printf("Enter name:");
	char ch = 0;
	int i=0;
	while((ch = getchar()) != '\n'){
		(*newNode).str[i++]=ch;
	}

	newNode->next = NULL;

	return newNode;
}
void addNode(){
	node *newNode = createNode();

	if(head==NULL){
		head=newNode;
	}else{
		node *temp= head;

		while(temp->next != NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}

void nodelen(int data){
	node *temp= head;
	int cnt =0;
	while(temp != NULL){
		if(data==strlen(temp->str)){
			printf("%s\n",temp->str);
			cnt++;
		}
		temp=temp->next;
	}
	if(cnt==0)
		printf("no string available of thise size:");

}
void printLL(){

	if(head==NULL){
		printf("invalide opration:");
	}else{
		node *temp = head;
		while(temp != NULL){
			printf("|%s| ",temp->str);
			temp=temp->next;
		}
		printf("\n");
	}
}
void main(){

	int node=0;
	printf("Enter Node :");
	scanf("%d",&node);

	getchar();

	for(int i = 1; i<= node; i++){
		addNode();
	}

	printLL();

	int data=0;
	printf("Enter data:");
	scanf("%d",&data);

	nodelen(data);
}
