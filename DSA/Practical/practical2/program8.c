#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct node{
	char str[20];
	struct node *next;
}node;

node *head=NULL;
node *createNode(){
	node *newNode = (node*)malloc(sizeof(node));
	printf("Enter name: ");
	char ch;
	int i=0;
	while((ch = getchar()) != '\n'){
		(*newNode).str[i++]=ch;
	}

	newNode->next = NULL;

	return newNode;
}
void addNode(){
	node *newNode = createNode();

	if(head==NULL){
		head=newNode;
	}else{
		node *temp= head;

		while(temp->next != NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}
int count(){

	node *temp=head;

	int count=0;
	while(temp != NULL){
		count++;
		temp=temp->next;
	}
	return count;
}

void delfirst(){

	if(head==NULL){
		printf("wrong input:\n");
	}else if(head->next==NULL){
		free(head);
		head=NULL;
	}else{
		node* temp = head;
		head=temp->next;
		free(temp);
	}
}
void dellast(){
	if(head==NULL){
		printf("wrong input:\n");
	}else if(head->next==NULL){
		free(head);
		head=NULL;
	}else{
		node *temp = head;
		while(temp->next->next != NULL){
			temp=temp->next;
		}
		free(temp->next);
		temp->next=NULL;
	}
}
void delatpos(int pos){
		int cnt=count();
		if(pos<=0 || pos>cnt){
			printf("wrong input:\n");
		}else if(pos==1){
			delfirst();
		}else if(pos==cnt){
			dellast();
		}else{
			node *temp1=head;
			node *temp =NULL;
			while(pos-2){
				temp=temp->next;
				pos--;
			}
			temp1=temp->next;
			temp->next=temp1->next;
			free(temp1);
		}
}
void delnode(int data){
	node *temp=head;
	int pos = 1;
	while(temp != NULL){
		int len = strlen(temp->str);
		if(data == len){
			delatpos(pos--);
		}
		pos++;
		temp=temp->next;
	}
}
void printLL(){

	if(head==NULL){
		printf("Empty linked list:\n");
	}else{
		node *temp = head;
		while(temp != NULL){
			printf("|%s| ",temp->str);
			temp=temp->next;
		}
		printf("\n");
	}
}
void main(){

	int node=0;
	printf("Enter Node :");
	scanf("%d",&node);
	getchar();
	for(int i = 1; i<= node; i++){
		addNode();
	}

	printLL();

	int data;
	printf("Enter node length");
	scanf("%d",&data);

	delnode(data);

	printLL();
}
