/*WAP THAT SEARCHES ALL OCCURANCE OF A PARTICULAR ELEMENRT FROM A SINGLY LINEAR LINKED LIST.
 * INPUT LINKED LIST: |10|->|20|->|30|->|40|->|30|->|30|->|70|
 * INPUT ELEMENT: 30
 * OUTPUT: 3
*/
#include <stdio.h>
#include <stdlib.h>

typedef struct Demo{
	struct Demo *prev;
	int data;
	struct Demo *next;
}Demo;

Demo *head = NULL;

Demo* createNode(){
	Demo *newNode = (Demo*)malloc(sizeof(Demo));

	newNode->prev =NULL;

	printf("Enter data:");
	scanf("%d",&newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	Demo *newNode = createNode();

	if(head==NULL){
		head=newNode;
	}else{
		Demo *temp = head;
		while(temp->next != NULL){
			temp=temp->next;
		}
		temp->next=newNode;
		newNode->prev=temp;
	}
}

void printLL(){
	if(head==NULL){
		printf("linke list is empty:\n");
	}else{
		Demo *temp = head;
		while(temp -> next != NULL){
			printf("|%d|-->",temp->data);
			temp=temp->next;
		}
		printf("|%d|\n",temp->data);
	}
}

void occcount(int data){

	Demo *temp = head;
	int cnt=0;
	while(temp != NULL){
		if(temp->data==data){
			cnt++;
		}
		temp=temp->next;
	}
	printf("%d in %d time found in linked list:\n" ,data,cnt);
}

void main(){

	int pos=0;
	printf("Enter no of Nodes you want to create:");
	scanf("%d",&pos);

	for(int i = 1; i<= pos; i++){
		addNode();
	}

	printLL();

	int data=0;
	printf("Enter no you want to compair:");
	scanf("%d",&data);

	occcount(data);
}
