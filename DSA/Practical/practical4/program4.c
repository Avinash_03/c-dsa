/* WAP that accepts two singly linear linked lists from the user and concat that last N element of the source linked list after the destination linked list.
 * input source linked list :|30|->|30|->|70|
 * input destination linked list :|10|->|20|->|30|->|40| 
 * input number of element :2
 * input destination linked list :|10|->|20|->|30|->|40|->|30|->|70| 
 */ 
#include <stdio.h>
#include <stdlib.h>

struct Demo{
	int x;
	struct Demo *next;
};

int countLL(struct Demo **head);
struct Demo *head1=NULL;
struct Demo *head2=NULL;

struct Demo *creatNode(){
	struct Demo *newNode = (struct Demo *)malloc(sizeof(struct Demo));
	printf("Enter data:");
	scanf("%d",&newNode->x);
	
	newNode->next=NULL;

	return newNode;

}
int addNode(struct Demo **head){

	struct Demo *newNode = creatNode();
	if(*head == NULL){
		*head = newNode;
	}else{
		struct Demo *temp = *head;
		while(temp->next != NULL){
			temp=temp->next;
		}
		temp->next=newNode;
		
	}
}

void ConcatLL(){

	struct Demo *temp = head2;
	if(head2==NULL){
		printf("destination LL is Empty:\n");
	}else{
		struct Demo *temp = head2;
		while(temp->next != NULL){
			temp=temp->next;
		}
		int x=0;
		printf("enter no, who many number of element concat:\n");
		scanf("%d",&x);

		struct Demo *temp1=head1;
		int cnt=countLL(&head1);
		while(cnt-x){
			temp1=temp1->next;
			x++;
		}
		temp->next=temp1;
	}
}
int countLL(struct Demo **head){
	int cnt=0;
	if(*head==NULL){
		return 0;
	}else{
		struct Demo *temp=*head;
		while(temp != NULL){
			cnt++;
			temp=temp->next;
		}
	}
	return cnt;
}
void printLL(struct Demo **head){

	if(*head==NULL){
		printf("Empty Linked List:\n");
	}else{
		struct Demo *temp= *head;
		while(temp->next != NULL ){
			printf("|%d|->",temp->x);
			temp = temp->next;
		}
		printf("|%d|\n",temp->x);
	}

}
void main(){
	char choice;
	do{
		printf("1.addNode(source linked list:)\n");
		printf("2.addNode(destination linked list:)\n");
		printf("3.printLL(source linked list:)\n");
		printf("4.printLL(destination linked list:)\n");
		printf("5.ConcatLL\n");

		int x;
		printf("Enter you'r choice :");
		scanf("%d",&x);

		switch(x){
			case 1:
				addNode(&head1);
				break;
			case 2:
				addNode(&head2);
				break;
			case 3:
				printLL(&head1);
				break;
			case 4:
				printLL(&head2);
				break;
			case 5:
				ConcatLL();
				break;
			default:
				printf("invalide input:");
		}
		getchar();
		printf("Do you want continue:");
		scanf("%c",&choice);
	}while(choice == 'Y'|| choice == 'y');
}
