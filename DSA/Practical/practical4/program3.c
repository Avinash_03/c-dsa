/*WAP THAT ACCEPTS TWO SINGLY LINEAR LINKED LISTS FROM THE USER AND CONCAT THE FIRST N ELEMENT OF THE SOURCE lINKED LIST AFTER 
 * THE DESTINATION LINKED LIST.
 * INPUT SOURCE LINKED LIST :|30|->|30|->|70|
 * INPUT DESTINATION LINKED LIST :|10|->|20|->|30|->|40|
 * INPUT NUMBER OF ELEMENT:2
 * OUTPUT DESTINATION LINKED LIST :|10|->|20|->|30|->|40|->|30|->|30|
 */
#include <stdio.h>
#include <stdlib.h>

struct node{
        int no;
        struct node *next;
};

struct node *head1=NULL;
struct node *head2=NULL;

struct node *createNode(){
        struct node *newNode=(struct node *)malloc(sizeof(struct node));

        printf("Enter No:\n");
        scanf("%d",&newNode->no);

        newNode->next=NULL;

	return newNode;
}

void addNode(struct node **head){

        struct node *newNode=createNode();

        if(*head==NULL){
                *head=newNode;
        }else{
                struct node *temp = *head;
                while(temp->next != NULL){
                        temp=temp->next;
                }
                temp->next=newNode;
        }
}

int count(struct node *head){
	int cnt=0;
	if(head==NULL){
		return cnt;
	}else{
		struct node *temp= head;
		while(temp != NULL){
			cnt++;
			temp=temp->next;
		}
		return cnt;
	}
}
void concat(int data){
        if(head2==NULL){
                head2=head1;
        }else{
                struct node *temp=head2;
                while(temp->next != NULL){
                        temp=temp->next;
                }
		struct node *temp1=head1;
		while(data-1){
			temp1=temp1->next;
			data--;
		}
		temp1->next=NULL;
                temp->next=head1;
        }
}

void printLL(struct node **head){
        if(*head==NULL){
                printf("Empty Linked List:\n");
        }else{
                struct node *temp=*head;
                while(temp->next != NULL){
                        printf("|%d|->",temp->no);
                        temp=temp->next;
                }
                printf("|%d|\n",temp->no);
        }
}
void main(){

        char choose;
        do{
                printf("1.addNode(source Linked List)\n");
                printf("2.addNode(destination Linked List)\n");
                printf("3.concat\n");
                printf("4.printLL(source Linked List)\n");
                printf("5.printLL(destination Linked List)\n");

                int ch;
                printf("Enter your choose:\n");
                scanf("%d",&ch);

                switch(ch){
                        case 1:
                                addNode(&head1);
                                break;
                        case 2:
                                addNode(&head2);
                                break;
                        case 3:{
				int cnt =count(head1);
				int pos;
				printf("ENTER NUMBER OF ELEMENT TO CONCAT:\n");
				scanf("%d",&pos);
				while(pos>cnt){
				getchar();
				printf("INVALID NUMBER OF ELEMENT TO CONCAT:\n");
				printf("ENTER NUMBER OF ELEMENT TO CONCAT:\n");
				scanf("%d",&pos);
				}
                                concat(pos);
			       }
                                break;
                        case 4:
                                printLL(&head1);
                                break;
                        case 5:
                                printLL(&head2);
                                break;
                        default :
                                printf("Wrong input:\n");
                }
                getchar();
                printf("Do you want continue:\n");
                scanf("%c",&choose);
        }while(choose=='y' || choose=='Y');
}

