#include <stdio.h>
#include <stdlib.h>

struct Node{
	struct Node *prev;
	int data;
	struct Node *next;
};

struct Node *head=NULL;
struct Node *createNode(){
	struct Node *newNode=(struct Node *)malloc(sizeof(struct Node));

	newNode->prev=NULL;

	printf("Enter Data:");
	scanf("%d",&newNode->data);

	newNode->next=NULL;

	return newNode;
}
int addNode(){
	struct Node *newNode = createNode();

	if(head==NULL){
		head=newNode;
		return -1;
	}else{
		struct Node *temp = head;

		while(temp->next != NULL){
			temp=temp->next;
		}

		temp->next=newNode;
		newNode->prev=temp;
		return 0;
	}
}

int inPlaceRev(){

	if(head==NULL){
		printf("Empty Linked List:\n");
		return -1;
	}else{
		if(head->next == NULL){
			printf("only one Node:");
		}else{
			struct Node *temp=NULL;
			while(head != NULL){
				temp=head->prev;
				head->prev=head->next;
				head->next=temp;
				head=head->prev;
			}
			head=temp->prev;
			return 0;
		}
	}
}

int printLL(){

	if(head==NULL){
		printf("Empty Linked List:\n");
		return -1;
	}else{
		struct Node *temp=head;
	
		while(temp->next != NULL){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}

		printf("|%d|\n",temp->data);

		return 0;
	}
}
void main(){
	int node=0;
	printf("Enter Node:\n");
	scanf("%d",&node);

	for(int i = 1; i<= node; i++)
		addNode();
	printLL();

	inPlaceRev();

	printLL();
}
