//midNode2
#include <stdio.h>
#include <stdlib.h>

struct Node{
	int data;
	struct Node *next;
};

struct Node *head=NULL;
struct Node *createNode(){

	struct Node *newNode=(struct Node*)malloc(sizeof(struct Node));

	printf("Enter data:\n");
	scanf("%d",&newNode->data);

	newNode->next=NULL;

	return newNode;
}

void addNode(){

	struct Node *newNode = createNode();

	if(head==NULL){
		head=newNode;
	}else{
		struct Node *temp = head;

		while(temp->next != NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}

void printLL(){
	if(head==NULL){
		printf("Empty Linked List:\n");
	}else{
		struct Node *temp = head;
		while(temp->next != NULL){
			printf("|%d|->",temp->data);
			temp= temp->next;
		}
		printf("|%d|\n",temp->data);
	}
}
int countNode(){
	int cnt=0;

	if(head==NULL){
		printf("Empty Linked List:\n");
	}else{
		struct Node *temp=head;
		while(temp != NULL){
			cnt++;
			temp=temp->next;
		}
	}

	return cnt;

}
int midNode(){
	if(head==NULL){
		printf("Empty Node:\n");
		return -1;
	}else if(head->next == NULL){
		printf("one node present :\n");
	}else{

		struct Node *slow = head;
		struct Node *fast = head;
		while(fast != NULL && fast->next != NULL){
			slow=slow->next;
			fast=fast->next->next;
		}
		return slow->data;
	}
}
void main(){
	int node;
	printf("Enter No of node :\n");
	scanf("%d",&node);

	for(int i= 1; i<= node; i++)
		addNode();

	printLL();

	int data=midNode();

	printf("%d is data of midNode:\n",data);

}
