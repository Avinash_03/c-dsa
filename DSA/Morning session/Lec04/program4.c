//pelindrome
#include <stdio.h>
#include <stdlib.h>

struct Node{
	int data;
	struct Node *next;
};

struct Node *head=NULL;
struct Node *createNode(){

	struct Node *newNode=(struct Node*)malloc(sizeof(struct Node));

	printf("Enter data:\n");
	scanf("%d",&newNode->data);

	newNode->next=NULL;

	return newNode;
}

void addNode(){

	struct Node *newNode = createNode();

	if(head==NULL){
		head=newNode;
	}else{
		struct Node *temp = head;

		while(temp->next != NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}

void printLL(){
	if(head==NULL){
		printf("Empty Linked List:\n");
	}else{
		struct Node *temp = head;
		while(temp->next != NULL){
			printf("|%d|->",temp->data);
			temp= temp->next;
		}
		printf("|%d|\n",temp->data);
	}
}
int countNode(){
	int cnt=0;

	if(head==NULL){
		printf("Empty Linked List:\n");
	}else{
		struct Node *temp=head;
		while(temp != NULL){
			cnt++;
			temp=temp->next;
		}
	}

	return cnt;
}

struct Node* midNode(){
	if(head==NULL){
		printf("Empty Linked List:\n");
	}else if(head->next==NULL){
		printf("only one Node:\n");
	}else{
		struct Node *fast=head->next;
		struct Node *slow=head;

		while(fast != NULL && fast->next != NULL){
			slow=slow->next;
			fast=fast->next->next;
		}
		return slow;
	}
}
struct Node* revNode(struct Node *head1){
	if(head==NULL){
		printf("Empty Linked List:\n");
	}else if(head->next == NULL){
		printf("one Node :\n");
	}else{
		struct Node *temp=NULL;
		struct Node *next=NULL;
		while(head1->next != NULL){
			next=head1->next;
			head1->next=temp;
			temp=head1;
			head1=next;
		}
		head1->next=temp;
		return head1;
	}
}
int isPelindrome(){
	int cnt =countNode();
	struct Node* mide=midNode();
	struct Node *head1=revNode(mide->next);
	mide->next=head1;

	struct Node*temp1=mide->next;
	struct Node*temp2=head;
	while(temp2 != mide){
		if(temp1->data != temp2->data){
			struct Node *h1=revNode(mide->next);
			mide->next=h1;
			return -1;
		}
			temp1=temp1->next;
			temp2=temp2->next;
	}
	struct Node *h=revNode(mide->next);
	mide->next=h;
	return 0;

}
void main(){
	int node;
	printf("Enter No of node :\n");
	scanf("%d",&node);

	for(int i= 1; i<= node; i++)
		addNode();

	printLL();

	int pelindrome=isPelindrome();
	printLL();

	if(pelindrome == 0){
		printf("Node pelindrome:\n");
	}else{
		printf( "Node Not pelindrome:\n");
	}

}
