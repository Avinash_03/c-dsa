//pelindrome
#include <stdio.h>
#include <stdlib.h>

struct Node{
	int data;
	struct Node *next;
};

struct Node *head=NULL;
struct Node *createNode(){

	struct Node *newNode=(struct Node*)malloc(sizeof(struct Node));

	printf("Enter data:\n");
	scanf("%d",&newNode->data);

	newNode->next=NULL;

	return newNode;
}

void addNode(){

	struct Node *newNode = createNode();

	if(head==NULL){
		head=newNode;
	}else{
		struct Node *temp = head;

		while(temp->next != NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}

void printLL(){
	if(head==NULL){
		printf("Empty Linked List:\n");
	}else{
		struct Node *temp = head;
		while(temp->next != NULL){
			printf("|%d|->",temp->data);
			temp= temp->next;
		}
		printf("|%d|\n",temp->data);
	}
}
int countNode(){
	int cnt=0;

	if(head==NULL){
		printf("Empty Linked List:\n");
	}else{
		struct Node *temp=head;
		while(temp != NULL){
			cnt++;
			temp=temp->next;
		}
	}

	return cnt;

}
int isPelindrome(){
	int cnt = countNode();

	int arr[cnt];
	int start = 0;
	int end = cnt-1;

	struct Node *temp = head;
	int i=0;
	while(cnt){
		arr[i++]=temp->data;
		cnt--;
		temp=temp->next;
	}
	
	while(start < end){
		if(arr[start] != arr[end])
			return -1;

		start++;
		end--;
	}
	return 0;
}
void main(){
	int node;
	printf("Enter No of node :\n");
	scanf("%d",&node);

	for(int i= 1; i<= node; i++)
		addNode();

	printLL();

	int pelindrome=isPelindrome();

	if(pelindrome == 0){
		printf("Node pelindrome:\n");
	}else{
		printf( "Node Not pelindrome:\n");
	}

}
