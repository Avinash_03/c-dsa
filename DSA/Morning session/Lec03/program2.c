//inplace reverce in singly circular
#include <stdio.h>
#include <stdlib.h>

struct Node{
	int data;
	struct Node *next;
};

struct Node *head=NULL;
struct Node *createNode(){
	struct Node *newNode=(struct Node *)malloc(sizeof(struct Node));

	printf("Enter Data:");
	scanf("%d",&newNode->data);

	newNode->next=NULL;

	return newNode;
}
int addNode(){
	struct Node *newNode = createNode();

	if(head==NULL){
		head=newNode;
		newNode->next=head;
		return -1;
	}else{
		struct Node *temp = head;

		while(temp->next != head){
			temp=temp->next;
		}
		temp->next=newNode;
		newNode->next=head;
		return 0;
	}
}

int revNode(){
	if(head==NULL){
		printf("Empty Linked List:\n");
		return -1;
	}else{
		struct Node *temp = head;
		struct Node *next = NULL;
		struct Node *head1 = head;
		while(head1->next != head){
			next = head1->next;
			head1 -> next = temp;
			temp = head1;
			head1 = next;
		}
		head1->next=temp;
		head->next=head1;
		head = head1;
	}
}
int printLL(){

	if(head==NULL){
		printf("Empty Linked List:\n");
		return -1;
	}else{
		struct Node *temp=head;
		while(temp->next != head){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
		printf("|%d|\n",temp->data);
		return 0;
	}
}
void main(){
	int nodes=0;
	printf("Enter Nodes:");
	scanf("%d",&nodes);

	for(int i = 1; i<= nodes; i++){
		addNode();
	}

	printLL();

	revNode();

	printLL();
}

