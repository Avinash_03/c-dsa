#include <stdio.h>
#include <stdlib.h>

struct Node{
	int data;
	struct Node *next;
};

struct Node *head=NULL;
struct Node *createNode(){
	struct Node *newNode=(struct Node *)malloc(sizeof(struct Node));

	printf("Enter Data:");
	scanf("%d",&newNode->data);

	newNode->next=NULL;

	return newNode;
}
int addNode(){
	struct Node *newNode = createNode();

	if(head==NULL){
		head=newNode;
		return -1;
	}else{
		struct Node *temp = head;

		while(temp->next != NULL){
			temp=temp->next;
		}
		temp->next=newNode;
		return 0;
	}
}

int revNode(){
	if(head==NULL){
		printf("Empty Linked List:\n");
		return -1;
	}else{
		struct Node *temp= NULL;
		struct Node *next= NULL;
		while(head->next != NULL){
			next=head->next;
			head->next=temp;
			temp=head;
			head=next;
		}
		head->next=temp;
	}
}
int printLL(){

	if(head==NULL){
		printf("Empty Linked List:\n");
		return -1;
	}else{
		struct Node *temp=head;
		while(temp->next != NULL){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
		printf("|%d|\n",temp->data);
		return 0;
	}
}
void main(){
	int nodes=0;
	printf("Enter Nodes:");
	scanf("%d",&nodes);

	for(int i = 1; i<= nodes; i++){
		addNode();
	}

	printLL();

	revNode();

	printLL();
}

