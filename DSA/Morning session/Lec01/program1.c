#include <stdio.h>
#include <stdlib.h>

struct Node{
	int data;
	struct Node *next;
};

struct Node *head1=NULL;
struct Node *head2=NULL;

struct Node *createNode(){
	struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));

	printf("Enter data:\n");
	scanf("%d",&newNode->data);

	newNode->next=NULL;

	return newNode;
}
void addNode(struct Node **head){
	struct Node *newNode = createNode();
	if(*head==NULL){
		*head=newNode;
	}else{
		struct Node *temp=*head;
		while(temp->next != NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}

void printLL(struct Node *head){

	if(head==NULL){
		printf("Empty Linked List:\n");
	}else{
		struct Node *temp = head;
		while(temp->next != NULL){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
		printf("|%d|\n",temp->data);
	}
}

void conCatNode(){

	struct Node *temp = head1;
	while(temp->next != NULL){
		temp = temp->next;
	}
	temp->next=head2;
}
void main(){
	int node=0;
	printf("Enter No of node in Linked List 1:");
	scanf("%d",&node);
	for(int i = 1;i<=node; i++)
		addNode(&head1);

	printLL(head1);

	printf("Enter No of node in Linked List 2:");
	scanf("%d",&node);
	for(int i = 1;i<=node; i++)
		addNode(&head2);

	printLL(head2);

	conCatNode();

	printLL(head1);
}
