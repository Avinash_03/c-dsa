#include <stdio.h>
#include <stdlib.h>

struct Node{
	int data;
	struct Node *next;
};

struct Node *head1=NULL;
struct Node *head2=NULL;

struct Node *createNode(){
	struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));

	printf("Enter data:\n");
	scanf("%d",&newNode->data);

	newNode->next=NULL;

	return newNode;
}
void addNode(struct Node **head){
	struct Node *newNode = createNode();
	if(*head==NULL){
		*head=newNode;
	}else{
		struct Node *temp=*head;
		while(temp->next != NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}

void printLL(struct Node *head){

	if(head==NULL){
		printf("Empty Linked List:\n");
	}else{
		struct Node *temp = head;
		while(temp->next != NULL){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
		printf("|%d|\n",temp->data);
	}
}
int countNode(struct Node *head){

	int cnt=0;
	if(head==NULL){
		printf("Empty Node:\n");
		return -1;
	}else{
		struct Node * temp =head;
		while(temp->next != NULL){
			cnt++;
			temp=temp->next;
		}
		cnt++;
		return cnt;
	}

}

int conCatNode(int pos){
	int cnt = countNode(head2);
	struct Node *temp = head1;
	struct Node *temp1 = head2;
	if(head2==NULL){
		int cnt1=countNode(head1);
		while(cnt1 - pos){
			temp=temp->next;
			cnt1--;
		}
		head2=temp;
		return 0;
	}
	if(pos<=0 || pos > cnt){
		printf("Invalide position\n");
		return -1;
	}else {
		if(head1==NULL){
			while(cnt - pos){
				temp1=temp1->next;
				cnt--;
			}
			head1=temp1;
		}else{
			while(temp->next != NULL){
				temp = temp->next;
			}
			while(cnt - pos){
				temp1=temp1->next;
				cnt--;
			}
			temp->next=temp1;
		}
		return 0;
	}
}
void main(){
	int node=0;
	printf("Enter No of node in Linked List 1:");
	scanf("%d",&node);
	for(int i = 1;i<=node; i++)
		addNode(&head1);

	printLL(head1);

	printf("Enter No of node in Linked List 2:");
	scanf("%d",&node);
	for(int i = 1;i<=node; i++)
		addNode(&head2);

	printLL(head2);

	int pos=0;
	printf("Enter position :");
	scanf("%d",&pos);
	conCatNode(pos);

	printLL(head1);
	printLL(head2);
}
