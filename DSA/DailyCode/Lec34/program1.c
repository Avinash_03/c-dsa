//bubble sort using 2 loops
#include<stdio.h>

void swap(int *arr,int i){
	int temp=arr[i];
	arr[i]=arr[i+1];
	arr[i+1]=temp;
}
void bubblesort(int *arr,int size){
	for(int i=0;i<size;i++){
		for(int j=0;j<size-i-1;j++){
			if(arr[j]>arr[j+1]){
				swap(arr,j);	
			/*	int temp=arr[j];
				arr[j]=arr[j+1];
				arr[j+1]=temp;*/
			}
		}
	}
}
void main(){
	int size;
	printf("Enter size of array:\n");
	scanf("%d",&size);

	int arr[size];
	printf("Enter aaray element:\n");
	for(int i=0;i<=size-1;i++){
		scanf("%d",&arr[i]);
	}
	bubblesort(arr,size);

	printf("\n");

	for(int i=0;i<=size-1;i++){
		printf("%d ",arr[i]);
	}
	printf("\n");
}
