//bubble sort using 1 loops
#include<stdio.h>

void swap(int *arr,int i){
	int temp=arr[i];
	arr[i]=arr[i+1];
	arr[i+1]=temp;
}
void bubblesort(int *arr,int size){
	int j=(size-1)*2;
	for(int i=0;i<size;i++){
		if(arr[i]>arr[i+1]){
			swap(arr,i);	
		/*	int temp=arr[j];
		 	arr[j]=arr[j+1];
			arr[j+1]=temp;*/
		}
		if(i==size-1){
			i=-1;
			j--;
		}
		if(j==0)
			break;
	}
}
void main(){
	int size;
	printf("Enter size of array:\n");
	scanf("%d",&size);

	int arr[size];
	printf("Enter aaray element:\n");
	for(int i=0;i<=size-1;i++){
		scanf("%d",&arr[i]);
	}
	bubblesort(arr,size);

	printf("\n");

	for(int i=0;i<=size-1;i++){
		printf("%d ",arr[i]);
	}
	printf("\n");
}
