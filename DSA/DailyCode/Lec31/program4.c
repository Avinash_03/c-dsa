//rotating array fined value's index:
#include <stdio.h>

int sil(int *arr,int start,int end,int key){
	int mid=0;
	while(start <= end){
		mid = (start+end)/2;
		if(arr[mid]==key)
			return mid;
		if(arr[mid]>key){
			end=mid-1;
		}
		if(arr[mid]<key)
			start=mid+1;		
	}
	return -1;
}

void main(){

	int size,key,index,start,end;
	printf("Enter array size:");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array element:\n");
	for(int i=0; i <= size-1; i++){
		scanf("%d",&arr[i]);
	}
	printf("Enter Key element:\n");
	scanf("%d",&key);

	for(int i=0; i<size;i++){
		index=i;
		if(arr[i]>arr[i+1])
			break;
	}

	if(arr[size-1] < key){
		start=0;
		end=index;
	}else{
		start=index+1;
		end=size-1;
	}
	int ret=sil(arr,start,end,key);

	if(ret !=-1)
		printf("%d is a present at %d index\n",key,ret);
	else
		printf("%d is a not present:\n",key);
}
