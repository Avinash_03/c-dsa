//rotating array fined value's index:
#include <stdio.h>

int search(int* nums, int numsSize, int target){
    int index,start,end,mid;
    for(int i =0; i<=numsSize-1;i++){
        index=i;
        if(nums[i]>nums[i+1]){
            break;
        }
    }
    if(target>nums[numsSize-1]){
        start=0;
        end=index;
    }else{
        start=index+1;
        end=numsSize-1;
    }
    while(start<=end){
        mid=(start+end)/2;
        if(nums[mid]==target){
            return mid;
        }
        if(nums[mid]>target){
            end=mid-1;
        }
        if(nums[mid]<target){
            start=mid+1;
        }
    }
    return -1;
}

void main(){

	int size,key,index,start,end;
	printf("Enter array size:");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array element:\n");
	for(int i=0; i <= size-1; i++){
		scanf("%d",&arr[i]);
	}
	printf("Enter Key element:\n");
	scanf("%d",&key);

	int ret=search(arr,size,key);

	if(ret !=-1)
		printf("%d is a present at %d index\n",key,ret);
	else
		printf("%d is a not present:\n",key);
}
