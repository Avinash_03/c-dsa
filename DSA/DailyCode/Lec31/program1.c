//floor value in array
#include <stdio.h>

int flor(int *arr,int size,int key){
	int start=0,end=size-1,mid=0;
	int store=-1;
	while(start <= end){
		mid = (start+end)/2;
		if(arr[mid]==key)
			return arr[mid];
		if(arr[mid]>key)
			end=mid-1;
		if(arr[mid]<key){
			store=arr[mid];
			start=mid+1;
		}
	}
	return store;
}

void main(){

	int size,key;
	printf("Enter array size:");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array element:\n");
	for(int i=0; i <= size-1; i++){
		scanf("%d",&arr[i]);
	}
	printf("Enter Key element:\n");
	scanf("%d",&key);

	int ret=flor(arr,size,key);

	if(ret !=-1)
		printf("%d is a floor value of %d\n",ret,key);
	else
		printf("%d is a not present:\n",key);
}
