//firstocc 
#include <stdio.h>

int firstocc(int *arr,int size,int key){
	int start=0,end=size-1,mid=0;
	int store=-1;
	while(start <= end){
		mid = (start+end)/2;
		if(arr[mid]==key)
			store=mid;
		if(arr[mid]>=key){
			end=mid-1;
		}
		if(arr[mid]<key)
			start=mid+1;
		
	}
	return store;
}

void main(){

	int size,key;
	printf("Enter array size:");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array element:\n");
	for(int i=0; i <= size-1; i++){
		scanf("%d",&arr[i]);
	}
	printf("Enter Key element:\n");
	scanf("%d",&key);

	int ret=firstocc(arr,size,key);

	if(ret !=-1)
		printf("%d is a siling value of %d\n",ret,key);
	else
		printf("%d is a not present:\n",key);
}
