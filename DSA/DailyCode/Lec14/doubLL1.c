#include <stdio.h>
#include <stdlib.h>

typedef struct Node{

	struct Node *prev;
	int data;
	struct Node *next;
}Node;

Node *head = NULL;

Node* createNode(){

	Node *newNode = (Node*)malloc(sizeof(Node));

	newNode->prev = NULL;

	printf("Enter data:");
	scanf("%d",&newNode->data);

	newNode->next = NULL;

	return newNode;
}
void addNode(){
	Node* newNode = createNode();

	if(head==NULL){
		head=newNode;
	}else{
		Node *temp = head;
		while(temp->next != NULL){
			temp=temp->next;
		}
		temp->next = newNode;
		newNode->prev = temp;
	}
}

void addfirst(){
	Node *newNode = createNode();

	if(head==NULL){
		head=newNode;
	}else{
		newNode->next = head;
		head->prev = newNode;
		head = newNode;
	}
}

void printDLL(){

	if(head==NULL){

		printf("No Node present:");

	}else{
		Node* temp = head;
		while(temp->next != NULL){
			printf("|%d|-->",temp->data);
			temp=temp->next;
		}
		printf("|%d|",temp->data);
		
	}
	printf("\n");

}

void addlast(){

	addNode();
}
int count(){
	Node *temp = head;
	int count=0;
	while(temp != NULL){
		temp= temp->next;
		count++;
	}
	return count;
}

int addatpos(int pos){

	int cnt = count();

	if(pos<=0 ||pos >= cnt+2){
		printf("invalid input:");
		return -1;
	}else if(pos == 1){
		addfirst();
	}else if(pos == cnt+1){
		addlast();
	}else{
		Node *temp = head;
		Node *newNode = createNode();
		while(pos-2){
			temp=temp->next;
		}
		newNode->next=temp->next;
		newNode->prev=temp;
		temp->next->prev=newNode;
		temp->next=newNode;
		return 0;
	}
}

void delfirst(){

	int cnt = count();
	if(head==NULL){
		printf("No Node present:");
	}else if(cnt==1){
		free(head);
		head = NULL;
	}else{
		head=head->next;
		free(head->prev);
	}
}

void dellast(){
	int cnt = count();

	if(head==NULL){
		printf("No Node present:");
	}else if(cnt == 1){
		free(head);
		head=NULL;
	}else{
		Node* temp = head;
		while(temp->next->next != NULL){
			temp=temp->next;
		}
		free(temp->next);
		temp->next=NULL;
	}
}

void delatpos(int pos){
	int cnt = count();
	if(pos <= 0 || pos > cnt){
		printf("invalid opration:\n");
	}else if(pos == 1){
		delfirst();
	}else{
		Node *temp = head;
		while(pos-2){
			temp=temp->next;
		}
		temp->next=temp->next->next;
		free(temp->next->prev);
		temp->next->prev=temp;
	}
}
void main(){

	int Node=0;
	printf("Enter No of Nodes:");
	scanf("%d",&Node);

	for(int i = 1; i<= Node; i++){
		addNode();
	}

	char choice=0;
	do{
		printf("1.addNode\n");
		printf("2.addfirst\n");
		printf("3.addlast\n");
		printf("4.addatpos\n");
		printf("5.delfirst\n");
		printf("6.dellast\n");
		printf("7.delatpos\n");
		printf("8.prnitDLL\n");

		int ch=0;
		printf("Enter your choice:");
		scanf("%d",&ch);

		switch(ch){
			case 1:
				addNode();
				break;
			case 2:
				addfirst();
				break;
			case 3:
				addlast();
				break;
			case 4:
				{
				int pos=0;
				printf("Enter position:");
				scanf("%d",&pos);
				addatpos(pos);
				}
				break;
			case 5:
				delfirst();
				break;
			case 6:
				dellast();
				break;
			case 7:
				{
				int pos=0;
				printf("Enter position:");
				scanf("%d",&pos);
				delatpos(pos);
				}
				break;
			case 8:
				printDLL();
				break;
			default:
				printf("wrong Input:");
		}
		getchar();
		printf("Do you want to continue:");
		scanf("%c",&choice);
	}while(choice == 'y' || choice == 'Y');
}
