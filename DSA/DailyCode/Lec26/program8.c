#include <stdio.h>

int fact(int num){

	static int facto =1;
	facto*=num;
	if(num != 1){
		fact(--num);
	}else{
		return facto;
	}
}
void main(){
	int num=0;
	printf("Enter no:");
	scanf("%d",&num);

	int facto=fact(num);

	printf("%d is factorial of %d:\n",facto,num);
}
