//define of recursion - call to function in same function
#include <stdio.h>

void fun(){
	printf("In fun:\n");
	fun();
}

void main(){
	fun();
}

