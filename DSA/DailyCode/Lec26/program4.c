//sum of number using recursion
#include <stdio.h>

void add(int x){

	static int sum=0;

	sum+=x;
	if(x != 1)
		add(--x);
	else
		printf("%d is a sum\n",sum);
}
void main(){
	int x=0;
	printf("Enter no to sum:");
	scanf("%d",&x);

	add(x);
}
