#include <stdio.h>
#include <stdlib.h>

typedef struct Node{
	struct Node *prev;
	int data;
	struct Node *next;
}node;

node *head = NULL;

node *createNode(){
	node *newNode = (node*)malloc(sizeof(node));
	newNode->prev=NULL;

	printf("Enter Data:");
	scanf("%d",&newNode->data);

	newNode->next= NULL;

	return newNode;
}
void addNode(){

	node *newNode = createNode();

	if(head==NULL){
		head=newNode;
	}else{
		node *temp = head;
		while(temp->next != NULL){
			temp=temp->next;
		}
		temp->next=newNode;
		newNode->prev=temp;
	}
}

void printLL(){

	if(head==NULL){
		printf("Empty node:");
	}else{
		node *temp=head;
		while(temp->next != NULL){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
		printf("|%d|",temp->data);
	}
	printf("\n");
}

int countNode(){
	int cnt=0;
	if(head==NULL){
		return 0;
	}else{
		node *temp=head;
		while(temp->next != NULL){
			cnt++;
			temp=temp->next;
		}
		cnt++;
		return cnt;
	}
}
void revDLL(){
	int cnt = countNode();

	node *temp1=head;
	node *temp2=head;

	while(temp2->next != NULL){
		temp2=temp2->next;
	}

	int store=0;
	for(int i = 1; i <= cnt/2; i++){
		store = temp1->data;
		temp1->data=temp2->data;
		temp2->data=store;

		temp1=temp1->next;
		temp2=temp2->prev;
	}

}
void main(){

	int node;

	printf("Enter No of Nodes:");
	scanf("%d",&node);

	for(int i = 1; i <= node; i++){
		addNode();
	}

	printLL();

	revDLL();

	printLL();
}
