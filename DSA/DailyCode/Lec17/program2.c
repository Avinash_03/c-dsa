#include <stdio.h>
#include <stdlib.h>

struct Node{
	struct Node *prev;
	int data;
	struct Node *next;
};

struct Node *head=NULL;

struct Node * createNode(){;
	struct Node * newNode = (struct Node *)malloc(sizeof(struct Node));

	newNode->prev=NULL;

	printf("Enter data:\n");
	scanf("%d",&newNode->data);

	newNode->next=NULL;

	return newNode;
}
void addNode(){
	struct Node *newNode = createNode();

	if(head==NULL){
		head=newNode;
		head->next=head;
		head->prev=head;
	}else{
		struct Node *temp = head;

		while(temp -> next != head){
			temp=temp->next;
		}
		temp->next=newNode;
		newNode->next=head;
		newNode->prev=temp;
		head->prev=newNode;
	}
}
void addFirst(){
	struct Node *newNode = createNode();

	if(head==NULL){
		head=newNode;
		head->next=head;
		head->prev=head;
	}else{
		newNode->next=head;
		newNode->prev=head->prev;
		head->prev->next=newNode;
		head->prev=newNode;
		head=newNode;
	}

}

int countNode(){

	int cnt=0;
	if(head==NULL){
		printf("Empty Linked List:\n");
		return -1;
	}else{
		struct Node *temp = head;
		while(temp->next != head){
			cnt++;
			temp=temp->next;
		}
		cnt++;
		return cnt;
	}
}

int addAtpos(int pos){
	int cnt = countNode();
	if(pos <= 0 || pos > cnt+1){
		printf("Invalide position:\n");
		return -1;
	}else{
		if(pos==1){
			addFirst();
		}else if(pos == cnt+1){
			addNode();
		}else{
			struct Node *newNode=createNode();
			struct Node *temp=head;

			while(pos-2){
				temp=temp->next;
				pos--;
			}
			newNode->next=temp->next;
			newNode->prev=temp;
			temp->next->prev=newNode;
			temp->next=newNode;
		}
	}
}

int delFirst(){
	if(head==NULL){
		printf("Empty Node:\n");
		return -1;
	}else{
		if(head->next == head){
			free(head);
			head=NULL;
		}else{
			head=head->next;
			head->prev=head->prev->prev;
			free(head->prev->next);
			head->prev->next=head;
			
		}
		return 0;
	}
}

int delLast(){
	if(head==NULL){
		printf("Empty Node:\n");
		return -1;
	}else{
		if(head->next == head){
			free(head);
			head=NULL;
		}else{
			head->prev=head->prev->prev;
			free(head->prev->next);
			head->prev->next=head;
		}
		return 0;
	}
}
int delAtpos(int pos){
	int cnt = countNode();

	if(pos <= 0 || pos > cnt){
		printf("Invalid Position:\n");
		return -1;
	}else{
		if(pos==1){
			delLast();
		}else if(pos==cnt){
			delLast();
		}else{
			struct Node *temp=head;
			while(pos-2){
				temp=temp->next;
				pos--;
			}
			temp->next=temp->next->next;
			free(temp->next->prev);
			temp->next->prev=temp;
		}
		return 0;
	}
}
int printLL(){

	if(head==NULL){
		printf("Empty Linked List:\n");
		return -1;
	}else{
		struct Node *temp=head;
		while(temp->next != head){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
		printf("|%d|\n",temp->data);
		return 0;
	}
}
void main(){

	int node=0;
	printf("Enter Node:\n");
	scanf("%d",&node);

	for(int i =1; i<= node; i++){
		addNode();
	}

	printLL();
	addFirst();
	printLL();

	int pos=0;
	printf("Enter Position:\n");
	scanf("%d",&pos);
	addAtpos(pos);
	printLL();

	delFirst();
	printLL();

	delLast();
	printLL();

	int pos1=0;
	printf("Enter Position:\n");
	scanf("%d",&pos1);
	delAtpos(pos1);
	printLL();

}
