#include <stdio.h>
#include <stdlib.h>

typedef struct Employee{
	char EName[20];
	int Eid;
	float sal;
}emp;

int size;
int top=-1; 

int push(emp **Stack){
	top++;
	if(top <= size-1){
		emp *obj=(emp *)malloc(sizeof(emp));
	
		getchar();
		printf("Ente Employee Name:\n");

		char ch=0;
		int i=0;
		while((ch=getchar())!='\n'){
			(*obj).EName[i++]=ch;
		}
		printf("Ente Employee id:\n");
		scanf("%d",&obj->Eid);

		printf("Ente Employee sal:\n");
		scanf("%f",&obj->sal);

		Stack[top]=obj;

		return 0;
	}else{
		printf("Stack overflow:\n");

		return -1;
	}
}
int pop(emp **Stack){
	int x = top;
	top--;
	return x;
}
int peek(emp **Stack){
	int x = top;
	emp *node=Stack[top];
	return x;
	
}
void main(){

	printf("Enter Stack Size:\n");	
	scanf("%d",&size);

	emp *Stack[size];
	char choice=0;
	do{
		printf("1.push\n");
		printf("2.pop\n");
		printf("3.peek\n");

		int ch=0;
		printf("enter your choice:\n");
		scanf("%d",&ch);

		switch(ch){
			case 1:
				push(Stack);
				break;
			case 2:
				{
				if(top==-1){
					printf(" Stack underflow:\n");
			        }else{
					int index=pop(Stack);
					printf("|%s->",Stack[index]->EName);
					printf("%d-> ",Stack[index]->Eid);
					printf("%f| popped\n",Stack[index]->sal);
					}
				}
				break;
			case 3:
				{
				if(top==-1){
					printf(" Stack underflow:\n");
			        }else{
					int index=peek(Stack);
					printf("|%s->",Stack[index]->EName);
					printf("%d-> ",Stack[index]->Eid);
					printf("%f|\n",Stack[index]->sal);
				     }
				}
				break;
			default:
				printf("Wrong Input:\n");	
		}
		getchar();
		printf("Do you want continue:\n");
		scanf("%c",&choice);	

	}while(choice== 'y'||choice=='Y');
}
