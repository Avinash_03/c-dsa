//Implementing stack using Linked List

#include <stdio.h>
#include <stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
}node;

node *head=NULL;
int size;

node *createNode(){
	node *newNode=(node *)malloc(sizeof(node));

	printf("Entre Data:\n");
	scanf("%d",&newNode->data);

	newNode->next=NULL;

	return newNode;
}
int countNode(){
	int cnt=0;
	if(head==NULL){
		printf("Empty stack:\n");
		return 0;
	}else{
		node *temp=head;
		while(temp != NULL){
			cnt++;
			temp=temp->next;
		}
		return cnt;
	}
}
int push(){
	if(countNode() == size){
		printf("Stack overflow:\n");
		return -1;
	}else{
		node *newNode=createNode();
		if(head==NULL){
			head=newNode;
		}else{
			node *temp=head;
			while(temp->next != NULL){
				temp=temp->next;
			}
			temp->next=newNode;
		}
		return 0;
	}
}
int pop(){
	int ret=0;
	node *temp=head;
	if(head->next ==NULL){
		ret=temp->data;
		free(head);
		head=NULL;

		return ret;
	}else{
		node *temp=head;

		while(temp->next->next != NULL){
			temp=temp->next;
		}
		ret=temp->next->data;
		free(temp->next);
		temp->next=NULL;

		return ret;
	}
}
int peek(){
	
	node *temp = head;

	while(temp->next != NULL){
		temp=temp->next;
	}
	int data=temp->data;

	return data;
}
void main(){
	printf("Enter stack size:\n");
	scanf("%d",&size);

	char choice;
	do{
		printf("1.push\n");
		printf("2.pop\n");
		printf("3.peek\n");

		int ch=0;
		printf("Enter your choice:\n");
		scanf("%d",&ch);

		switch(ch){
			case 1:
				push();
				break;
			case 2:
				{
				if(head!=NULL){
					int ret=pop();
					printf("%d popped value:\n",ret);
				}else
					printf("stack underflow:\n");
				}
				break;
			case 3:
				{
				if(head!=NULL){
					int data=peek();
					printf("%d top value:\n",data);
				}else
					printf("stack underflow:\n");
				}
				break;
			default:
				printf("Invalide input:\n");
		}
		getchar();
		printf("Do you want continue:\n");
		scanf("%c",&choice);
	}while(choice=='y' || choice=='Y');
}
