//Implementing stack using array

#include <stdio.h>
int top=-1;
int size;

int push(int *stack){
	if(top==size-1){
		printf("Stack overflow:\n");
		return -1;
	}else{
		int data;
                printf("Enter Data:\n");
                scanf("%d",&data);
		top++;
		stack[top]=data;
		return 0;
	}
}
int pop(int *stack){
	int ret = stack[top];
	top--;
	return ret;
}
int peek(int *stack){
	
	int data=stack[top];
	return data;
}
void main(){
	printf("Enter stack size:\n");
	scanf("%d",&size);

	int stack[size];

	char choice;
	do{
		printf("1.push\n");
		printf("2.pop\n");
		printf("3.peek\n");

		int ch=0;
		printf("Enter your choice:\n");
		scanf("%d",&ch);

		switch(ch){
			case 1:
				push(stack);
				break;
			case 2:
				{
				if(top>=0){
					int ret=pop(stack);
					printf("%d popped value:\n",ret);
				}else
					printf("stack underflow:\n");
				}
				break;
			case 3:
				{
				if(top>=0){
					int data=peek(stack);
					printf("%d Top value:\n",data);
				}else
					printf("stack underflow:\n");
				}
				break;
			default:
				printf("Invalide input:\n");
		}
		getchar();
		printf("Do you want continue:\n");
		scanf("%c",&choice);
	}while(choice=='y' || choice=='Y');
}
