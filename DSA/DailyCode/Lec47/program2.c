#include <stdio.h>
#include <stdlib.h>

typedef struct Node{
	int data;
	struct Node* left;
	struct Node* right;
}Node;

Node* createNode(){
	Node* newNode = (Node*)malloc(sizeof(Node));
	printf("ENter DATA : ");
	scanf("%d",&newNode->data);
	newNode->left = NULL;
	newNode->right = NULL;
	return newNode;
}

Node* buildTree(Node* root){
	if(root == NULL){
		return root;
	}else{
		char ch;
		printf("Do you want to addNode at LEFT : ");
		scanf(" %c",&ch);
		if(ch == 'Y' || ch == 'y'){
			Node* newNode = createNode();
			root->left = newNode;
			buildTree(newNode);
		}
		
		printf("Do you want to addNode at RIGHT : ");
		scanf(" %c",&ch);
		if(ch == 'Y' || ch == 'y'){
			Node* newNode = createNode();
			root->right = newNode;
			buildTree(newNode);
		}
		return root;
	}	
}

void preOrder(Node* root){
	if(root == NULL) return;
	printf(" %d ",root->data);
	preOrder(root->left);
	preOrder(root->right);
}

void main(){
	Node* root = createNode();
	buildTree(root);
	preOrder(root);
}
