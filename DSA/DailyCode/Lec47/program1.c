#include <stdio.h>
#include <stdlib.h>
struct Node{
	int data;
	struct Node *left;
	struct Node *right;
};

void preorder(struct Node* root){

	if(root == NULL)
		return;
	printf("%d\t",root->data);
	preorder(root->left);
	preorder(root->right);
}

struct Node* createNode(struct Node* root){
	char ch;
	char ch1;
	if(root==NULL){
		return root;
	}else{
	printf("you want to add node at left side (y/n)");
	scanf(" %c",&ch);
	struct Node *newNode=(struct Node*)malloc(sizeof(struct Node));
	newNode->left=NULL;
	newNode->right=NULL;
	if(ch=='Y'||ch=='y'){
		printf("Enter data :");
		scanf("%d",&newNode->data);
		root->left=createNode(newNode);
	}
	printf("you want to add node at right side (y/n)");
	scanf(" %c",&ch1);
	if(ch1=='y'||ch1=='Y'){
		struct Node *newNode=(struct Node*)malloc(sizeof(struct Node));
		printf("Enter data :");
		scanf("%d",&newNode->data);
		root->right=createNode(newNode);
	}

	return root;
	}
}

void postOrder(struct Node* root){
	if(root == NULL) return ;
	postOrder(root->left);
	postOrder(root->right);
	printf("%d\t",root->data);
}

void InOrder(struct Node* root){
	if(root == NULL) return ;
	InOrder(root->left);
	printf("%d\t",root->data);
	InOrder(root->right);
}



void main(){
	char ch;
	struct Node *root=(struct Node*)malloc(sizeof(struct Node));
	root->left=NULL;
	root->right=NULL;
	printf("Enter data of Root Node :");
	scanf("%d",&root->data);
	root = createNode(root);
	printf("pre = ");
	preorder(root);
	printf("\npost= ");
	postOrder(root);
	printf("\nIn  = ");
	InOrder(root);
	printf("\n");
}

