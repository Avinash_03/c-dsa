//Carry Forward Array right max element
#include <stdio.h>
int arr1[6];
int* CarrForwrd(int *arr,int size){
	//int arr1[size];
	arr1[size-1]=arr[size-1];
	for(int i=size-2;i>=0;i--){
		if(arr[i]>arr1[i+1]){
			arr1[i]=arr[i];
		}else{
			arr1[i]=arr1[i+1];
		}
	}
//	for(int i=0;i<size;i++)	
//		printf("%d",arr1[i]);
	return arr1;
}
void main(){
	int Arr[]={1,5,3,7,4,3};
	int size=sizeof(Arr)/sizeof(int);
	int* x = CarrForwrd(Arr,size);
	for(int i=0;i<size;i++)	
		printf("%d",x[i]);
}
