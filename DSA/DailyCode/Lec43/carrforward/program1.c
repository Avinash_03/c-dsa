//special Subsequences
#include <stdio.h>
#include <string.h>

int spcSub(char* arr,int size){
	int cnt=0;
	for(int i=0;i<size;i++){
		if(arr[i]!='G' && arr[i]!='A'){
			arr[i]='A';
		}
	}
	for(int i=0;i<size-1;i++){
		if(arr[i]=='A'&& arr[i+1]=='G'){
			cnt++;
			for(int j=i+2;j<size;j++){
				if(arr[j]=='G'){
					cnt++;
				}
			}
		}
	}
	return cnt;
}
void main(){
	char arr[]="ABCGAG";
	int size =strlen(arr);
	int x=spcSub(arr,size);
	printf("%d\n",x);

}
