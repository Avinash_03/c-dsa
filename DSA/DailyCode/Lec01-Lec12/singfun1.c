#include <stdio.h>
#include <stdlib.h>
int count();
typedef struct demo{
	int data;
	struct demo *next; 
}demo;

demo *head = NULL;

demo* createNode(){

	demo *newNode = (demo*)malloc(sizeof(demo));

	printf("Enter data:");
	scanf("%d",&newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	demo *newNode = createNode();
	if(head ==NULL){
		head = newNode;
	}else{
		demo *temp = head;

		while(temp->next != NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}

void printLL(){

	demo *temp = head;
	while(temp != NULL){
		printf("|%d| ",temp->data);
		temp=temp->next;
	}
}

void addfirst(){

	demo *newNode = createNode();
	demo *temp = head;
	if(head==NULL){
		head = newNode;
	}else{
		newNode->next=head;
		head = newNode;
	}

}
void addlast(){
	
	addNode();
}
int addatpos(int pos){

	int cnt = count();

	if(pos<=0 || pos >= cnt+2){
		printf("wrong position:\n");
		return -1;
	}else if(pos==cnt){
		addlast();
	}else if(pos==1){
		addfirst();
	}else{
		demo *newNode = createNode();
		demo *temp = head;
		while(pos-2){
			temp=temp->next;
			pos--;
		}
		newNode->next=temp->next;
		temp->next=newNode;
	}
}
int count(){

	int count=0;
	demo *temp = head;
	while(temp != NULL){
		count++;
		temp = temp->next;
	}
	return count;
}
void main(){
	char choice=0;
	do{
		printf("1.addNode\n");
		printf("2.printLL\n");
		printf("3.addfirst\n");
		printf("4.addlast\n");
		printf("5.addatpos\n");
		printf("6.count\n");

		int ch =0;
		printf("Enter choice");
		scanf("%d",&ch);

		switch(ch){
			case 1:
				addNode();
				break;
			case 2:
				printLL();
				break;
			case 3:
				addfirst();
				break;
			case 4:
				addlast();
				break;
			case 5:
			       {
				int pos=0;
				printf("Enter position");
				scanf("%d",&pos);
				addatpos(pos);
			       }
				break;
			case 6:
				count();
				break;
			default:
				printf("wrong input:");

		}
		getchar();

		printf("do you want continue:");
		scanf("%c",&choice);
	}while(choice == 'y' || choice == 'Y');
}
