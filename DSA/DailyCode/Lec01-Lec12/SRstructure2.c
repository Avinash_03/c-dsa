#include <stdio.h>
#include <string.h>

struct batsman{

	int jerNO;
	char Name[20];
	float avg;
	struct batsman *next;
};

void main(){

	struct batsman obj1,obj2,obj3;
	struct batsman *head = &obj1;

	head -> jerNO = 18;

	strcpy(head -> Name,"virat");
	head -> avg = 50.00;
	head -> next = &obj2;

	head -> next -> jerNO = 45;
	strcpy(head -> next -> Name,"Rohit");
	head -> next -> avg = 40.00;
	head -> next -> next  = &obj3;

	head -> next -> next -> jerNO = 10;
	strcpy(head -> next -> next -> Name, "sachin");
	head -> next -> next -> avg = 60.00;
	head -> next -> next -> next = NULL;

	printf("%d\n",head -> jerNO);
	printf("%s\n",head -> Name);
	printf("%f\n",head -> avg);

	printf("%d\n",head -> next -> jerNO);
	printf("%s\n",head -> next -> Name);
	printf("%f\n",head -> next ->avg);

	printf("%d\n",head -> next -> next -> jerNO);
	printf("%s\n",head -> next -> next -> Name);
	printf("%f\n",head -> next -> next -> avg);


}
