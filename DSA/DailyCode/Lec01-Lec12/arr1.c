#include <stdio.h>

void main(){

	char carr[] = {'a','b','c','d','e'};
	char *ptr1 = carr;
	int *ptr2 = carr;

	printf("%c\n",*ptr1);
	printf("%c\n",*ptr2);
	ptr1++;
	ptr2++;
	printf("%c\n",*ptr1);
	printf("%c\n",*ptr2);
}
