#include <stdio.h>
#include <string.h>

struct Company{

	int empcount;
	char CName[20];
	float rev;
	struct Company *next;
};
void main(){

	struct Company obj1,obj2,obj3;
	struct Company *head = &obj1;	

	head -> empcount = 100;
	strcpy(head -> CName ,"veritas");
	head -> rev = 5.5;
	head -> next = &obj2;

	head -> next -> empcount = 50;
	strcpy(head -> next -> CName,"infosis");
	head -> next -> rev = 4.4;
	head -> next -> next = &obj3;

	head -> next -> next -> empcount = 500;
	strcpy(head -> next -> next -> CName,"google");
	head -> next -> next-> rev = 7.7;
	head -> next -> next -> next = NULL;

	printf("%d\n",head -> empcount);
	printf("%s\n",head -> CName);
	printf("%f\n",head -> rev);

	printf("%d\n",head -> next -> empcount);
	printf("%s\n",head -> next -> CName);
	printf("%f\n",head -> next -> rev);
	
	printf("%d\n",head -> next -> next -> empcount);
	printf("%s\n",head -> next -> next -> CName);
	printf("%f\n",head -> next -> next -> rev);

}
