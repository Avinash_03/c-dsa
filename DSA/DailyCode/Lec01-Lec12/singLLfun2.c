#include <stdio.h>
#include <stdlib.h>

int count();
typedef struct demo{
	int data;
	struct demo *next;
}demo;

demo *head = NULL;

demo* createNode(){

	demo *newNode = (demo*)malloc(sizeof(demo));
	printf("Enter data:");
	scanf("%d",&newNode->data);

	newNode->next=NULL;
	return newNode;
}
void addNode(){

	demo* newNode = createNode();

	if(head==NULL){
		head=newNode;
	}else{
		demo* temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next=newNode;
	}
}
void printLL(){

	if(head==NULL){
		printf("wrong input:");
	}else{
		demo* temp = head;
		while(temp != NULL){
			printf("|%d| \n",temp->data);
			temp= temp->next;
		}
	}
}
void addfirst(){

	demo* newNode = createNode();
	demo* temp = head;
	if(head==NULL){
		head= newNode;
	}else{
		newNode->next=head;
		head=newNode;
	}
}
void addlast(){

	addNode();
}
void addatpos(int pos){

	int cnt = count();
	if(pos<=0 || pos >= cnt+2){
		printf("wrong position:");
	}else if(pos==cnt){
		addlast();
	}else if(pos==1){
		addfirst();
	}else{
		demo *newNode= createNode();
		demo *temp = head;

		while(pos-2){
			temp = temp->next; 
			pos--;
		}
		newNode->next=temp->next;
		temp->next=newNode;
	}
}
int count(){

	int count=0;
	demo *temp = head;
	while(temp != NULL){
		count++;
		temp=temp->next;
	}
	return count;
}
void delefirst(){

	if(head==NULL)
		printf("wrong input:");
	else if(head->next==NULL){
		free(head);
		head =NULL;
	}

	else{
		demo *temp =head;
		head=temp->next;
		free(temp);
	}
}
void delelast(){

	if(head==NULL)
		printf("wrong input");
	else if(head->next==NULL){
		free(head);
		head=NULL;
	}
	else{
		demo *temp = head;
		while(temp->next->next != NULL){
			temp=temp->next;
		}
		free(temp->next);
		temp->next = NULL;
	}
}
void main(){

	char choice=0;

	do{

		printf("1.addNode\n");
		printf("2.printLL\n");
		printf("3.addfirst\n");
		printf("4.addlast\n");
		printf("5.addatpos\n");
		printf("6.count\n");
		printf("7.delefirst\n");
		printf("8.deletelast\n");

		int ch=0;
		printf("enter you choice:");
		scanf("%d",&ch);

		switch(ch){

			case 1:
				addNode();
				break;
			case 2:
				printLL();
				break;
			case 3:
				addfirst();
				break;
			case 4:
				addlast();
				break;
			case 5:
				{
				int pos=0;

				printf("Enter position:");
				scanf("%d",&pos);
				addatpos(pos);
				}
				break;
			case 6:{
				int count1 = count();
				printf("count is %d",count1);
			       }
				break;
			case 7:
				delefirst();
				break;
			case 8:
				delelast();
				break;
		}
		getchar();

		printf("\ndo you want continue:");
		scanf("%c",&choice);

	}while(choice == 'y' || choice == 'Y');
}
