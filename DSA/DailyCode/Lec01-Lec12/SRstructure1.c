#include <stdio.h>
#include <string.h>

typedef struct Employee{

	int Empid;
	char empName[20];
	float sal;
	struct Employee *next;
}Emp;
void main(){

	Emp obj1,obj2,obj3;
	struct Employee *head = &obj1;

	obj1.Empid = 1;
	strcpy(obj1.empName,"kanha");
	obj1.sal = 50.00;
	obj1.next = &obj2;

	obj2.Empid = 2;
	strcpy(obj2.empName,"rahul");
	obj2.sal = 60.00;
	obj2.next = &obj3;

	obj3.Empid = 3;
	strcpy(obj3.empName,"Ashish");
	obj3.sal = 65.00;
	obj3.next = NULL;

	printf("%d\n",obj2.next -> Empid);
	printf("%s\n",obj3.empName);
	printf("%f\n",obj3.sal);

	printf("%d\n",obj1.next -> Empid);
	printf("%s\n",obj2.empName);
	printf("%f\n",obj2.sal);

	printf("%d\n",head -> Empid);
	printf("%s\n",obj1.empName);
	printf("%f\n",obj1.sal);
}
