#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Employee{
	int empid;
	char empname[20];
	float sal;
	struct Employee *next;
}emp;

void access(emp *ptr){

	printf("%d\n",ptr->empid);
	printf("%s\n",ptr->empname);
	printf("%f\n",ptr->sal);
	printf("%p\n",ptr->next);
}

void main(){

	emp *emp1=(emp*)malloc(sizeof(emp));
	emp *emp2=(emp*)malloc(sizeof(emp));
	emp *emp3=(emp*)malloc(sizeof(emp));

	emp1->empid = 1;
	strcpy(emp1->empname,"kanha");
	emp1->sal = 50.0;
	emp1->next= emp2;

	emp2->empid = 2;
	strcpy(emp2->empname,"rahul");
	emp2->sal = 60.0;
	emp2->next= emp3;

	emp3->empid = 3;
	strcpy(emp3->empname,"ashish");
	emp3->sal = 70.0;
	emp3->next= NULL;

	access(emp1);
	access(emp2);
	access(emp3);
}
