#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct movie{
	int count;
	char moviename[20];
	float rate;
	struct movie *next;
}M;

void access(M *ptr){

	printf("%d\n",ptr->count);
	printf("%s\n",ptr->moviename);
	printf("%f\n",ptr->rate);
	printf("%p\n",ptr->next);
}

void main(){

	M *m1=(M*)malloc(sizeof(M));
	M *m2=(M*)malloc(sizeof(M));
	M *m3=(M*)malloc(sizeof(M));

	m1->count = 1;
	strcpy(m1->moviename,"kantara");
	m1->rate = 9.0;
	m1->next= m2;

	m2->count = 2;
	strcpy(m2->moviename,"drishyam");
	m2->rate = 8.8;
	m2->next= m3;

	m3->count = 3;
	strcpy(m3->moviename,"FIR");
	m3->rate = 7.7;
	m3->next= NULL;

	access(m1);
	access(m2);
	access(m3);
}
