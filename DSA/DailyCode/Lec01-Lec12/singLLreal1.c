#include <stdio.h>
#include <stdlib.h>

typedef struct Car{
	char cName[20];
	int seats;
	float average;
	struct Car *next;
}Car;

Car *head = NULL;

Car *createNode(){

	Car *newNode=(Car*)malloc(sizeof(Car));

	getchar();
	printf("Enter car name:");
	char ch=0;
	int i = 0;
	while((ch=getchar()) != '\n'){
		(*newNode).cName[i]=ch;
		i++;
	}

	printf("Enter no of seats in car:");
	scanf("%d",&newNode->seats);

	printf("Enter car average:");
	scanf("%f",&newNode->average);

	newNode->next=NULL;

	return newNode;
}
void addNode(){
	Car *newNode=createNode();

	if(head==NULL){
		head=newNode;
	}else{
		Car *temp = head;
		while(temp-> next !=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}

void addfirst(){

	Car *newNode = createNode();
	if(head==NULL){
		head=newNode;
	}else{
		Car *temp = head;
		newNode->next=head;
		head=newNode;
	}
}

void addlast(){
	
	Car *newNode = createNode();
	if(head==NULL){
		head=newNode;
	}else{
		Car *temp = head;
		while(temp->next !=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}

int count(){

	Car *temp = head;
	int cnt=0;
	while(temp != NULL){
		temp= temp->next;
		cnt++;
	}
	return cnt;
}

void addatpos(int pos){

	int cnt = count();
	if(pos <= 0 || pos >= cnt+2){
		printf("invalide opration:");
	}else if(pos == 1){
		addfirst();
	}else if(pos == cnt+1){
		addlast();
	}else{
		Car *newNode = createNode();
		Car *temp = head;
		while(pos-2){
			temp = temp->next;
		}
		temp->next=newNode;
	}

}

void delfirst(){

	int cnt = count();
	if(head==NULL){
		printf("invalide oprataion:");
	}else if(cnt == 1){
		free(head);
		head=NULL;
	}else{
		Car *temp = head;
		head=head->next;
		free(temp);
	}
}

void dellast(){
	int cnt = count();
	if(head==NULL){
		printf("invalide opration:");
	}else if(cnt == 1){
		free(head);
		head=NULL;
	}else{
		Car *temp = head;
		while(temp -> next ->next != NULL){
			temp=temp->next;
		}
		free(temp->next);
		temp->next=NULL;
	}
}
void delatpos(int pos){

	int cnt = count();
	if(pos <= 0 || pos > cnt){
		printf("invalide opration:");
	}else if(pos == 1){
		delfirst();
	}else if(pos == cnt){
		dellast();
	}else{
		Car *temp1 = head;
		Car *temp = head;
		while(pos-2){
			temp=temp->next;
			temp1=temp1->next;
		}
		temp1=temp1->next;
		temp=temp->next->next;
		free(temp1);
	}

}
void printDLL(){

	if(head==NULL){
		printf("invalid opration:");
	}else{
	Car *temp = head;
	while(temp->next != NULL){
		printf("|%s-->",temp->cName);
		printf("%d-->",temp->seats);
		printf("%f|-->",temp->average);
		temp=temp->next;
	}
	printf("|%s-->",temp->cName);
	printf("%d-->",temp->seats);
	printf("%f|",temp->average);
	printf("\n");
	}
}

void main(){

	char choice=0;
	do{
		printf("1.addNode\n");
		printf("2.addfirst\n");
		printf("3.addlast\n");
		printf("4.count\n");
		printf("5.addatpos\n");
		printf("6.delfirst\n");
		printf("7.dellast\n");
		printf("8.delatpos\n");
		printf("9.printDLL\n");

		int ch=0;
		printf("Enter your choice:");
		scanf("%d",&ch);

		switch(ch){

			case 1:
				addNode();
				break;
			case 2:
				addfirst();
				break;
			case 3:
				addlast();
				break;
			case 4:
				{
				int cnt=count();
				printf("%d is Node count",cnt);
				}
				break;
			case 5:
				{
				int pos=0;
				printf("Enter position:");
				scanf("%d",&pos);
				addatpos(pos);
				}
				break;
			case 6:
				delfirst();
				break;
			case 7:
				dellast();
				break;
			case 8:
				{
				int pos=0;
				printf("Enter position:");
				scanf("%d",&pos);
				delatpos(pos);
				}
				break;
			case 9:
				printDLL();
				break;
			default:
				printf("invalid opration:\n");
		}
		getchar();
		printf("Do you want continue:");
		scanf("%c",&choice);
	}while(choice =='y'|| choice == 'Y');
}
