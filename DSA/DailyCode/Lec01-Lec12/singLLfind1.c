#include <stdio.h>
#include <stdlib.h>

typedef struct demo{
	int data;
	struct demo *next;
}demo;

demo *head = NULL;

demo *createNode(){
	demo* newNode = (demo*)malloc(sizeof(demo));
	printf("Enter data:");
	scanf("%d",&newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	demo *newNode = createNode();

	if(head==NULL){
		head=newNode;
	}else{
		demo* temp = head;
		while(temp->next != NULL){
			temp= temp->next;
		}
		temp->next= newNode;
	}
}

void printLL(){

	if(head==NULL){
		printf("No node present:\n");
	}else{
		demo *temp = head;
		while(temp != NULL){
			printf("|%d| ",temp->data);
			temp=temp->next;
		}
	}
}

int count(){

	int count=0;
	demo *temp = head;
	while(temp != NULL){
		count++;
		temp = temp->next;
	}
	return count;
}

void findNode(int num){

	int pos =1,cnt=0;
	int firstocc=0;
	int secondocc=0;
	demo *temp = head;
	while(temp != NULL){
		if(num==temp->data){
			cnt++;
			secondocc=firstocc;
			firstocc=pos;
		}
		pos++;
		temp=temp->next;
	}
	if(cnt==0){
		printf("number not found in Node:\n");
	}else if(cnt==1){
		printf("%d found one time.\n",num);
	}else{
		
		printf("%d found %d time.\n",num,cnt);
		printf("second occuraance in Node  %d :\n",secondocc);
	}
}

void main(){
	char choice=0;
	do{
		printf("1.addNode\n");
		printf("2.printLL\n");
		printf("3.count\n");
		printf("4.findNode\n");

		int ch=0;
		printf("Enter your choice:\n");
		scanf("%d",&ch);
		switch(ch){

			case 1:
				addNode();
				break;
			case 2:
				printLL();
				break;
			case 3:
				count();
				break;
			case 4:
				{
				int num=0;
				printf("Enter number:");
				scanf("%d",&num);
				findNode(num);
				}
				break;
			default:
				printf("wrong input:\n");
		}
		getchar();
		printf("Do you want to continue [y/n]:");
		scanf("%c",&choice);
	}while(choice=='y'||choice=='Y');
}
