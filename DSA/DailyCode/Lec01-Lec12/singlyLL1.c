#include <stdio.h>
#include <stdlib.h>

struct Node{

	int data;
	struct Node *next;
};
void main(){

	struct Node *head = NULL;
	struct Node *newnode = (struct Node*)malloc(sizeof(struct Node));

	newnode->data =10;
	newnode->next = NULL;
	head = newnode;

	newnode = (struct Node*)malloc(sizeof(struct Node));

	newnode->data =20;
	newnode->next = NULL;
	head -> next = newnode;

	newnode = (struct Node*)malloc(sizeof(struct Node));

	newnode->data =30;
	newnode->next = NULL;
	head -> next -> next = newnode;

	struct Node *Temp = head;

	while(Temp != NULL){
		printf("%d ",Temp->data);
		Temp = Temp->next;
	}

}
