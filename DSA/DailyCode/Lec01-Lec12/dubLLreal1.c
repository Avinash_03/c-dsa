#include <stdio.h>
#include <stdlib.h>

typedef struct Belding{
	struct Belding *prev;
	int FloarNo;
	struct Belding *next;
}belding;

belding *head=NULL;
belding *createNode(){
	belding *newNode = (belding*)malloc(sizeof(belding));

	newNode->prev = NULL;

	printf("Enter Floar Number:\n");
	scanf("%d",&newNode->FloarNo);

	newNode->next = NULL;

	return newNode;
}
void addNode(){
	belding *newNode = createNode();

	if(head==NULL){
		head=newNode;
		head->next=head;
		head->prev=head;
	}else{
		head->prev->next=newNode;
		newNode->next=head;
		newNode->prev=head->prev;
		head->prev=newNode;
	}
}

void addfirst(){
	belding *newNode = createNode();

	if(head==NULL){
		head=newNode;
		head->next=head;
		head->prev=head;
	}else{
		newNode->prev=head->prev;
		newNode->next=head;
		head->prev->next=newNode;
		head->prev=newNode;
		head=newNode;
	}
}
int countNode(){
	int cnt=0;

	if(head==NULL){
		return 0;
	}else{
		belding *temp=head;
		while(temp->next != head){
			temp=temp->next;
			cnt++;
		}
		cnt++;
	}
	return cnt;
}
int addatpos(int pos){
	int cnt = countNode();

	if(pos <= 0 || pos > cnt+1){
		printf("Invalid position:\n");
		return -1;
	}else{
		if(pos==1){
			addfirst();
		}else if(pos == cnt+1){
			addNode();
		}else{
			belding *newNode=createNode();
			belding *temp=head;
			while(pos-2){
				temp=temp->next;
			}
			newNode->next=temp->next;
			newNode->prev=temp;
			temp->next->prev=newNode;
			temp->next=newNode;
		}
	}
}

int delfirst(){

	if(head==NULL){
		printf("Empty node:\n");
		return -1;
	}else{ 
		if(head->next == head){
			free(head);
			head=NULL;
		}else{
			head->next->prev=head->prev;
			head=head->next;
			free(head->prev->next);
			head->prev->next=head;
		}
		return 0;
	}
}

int dellast(){

	if(head==NULL){
		printf("Empty node:\n");
		return -1;
	}else{
		if(head->next == head){
			free(head);
			head=NULL;
		}else{
			head->prev=head->prev->prev;
			free(head->prev->next);
			head->prev->next=head;
		}
		return 0;
	}
}

int delatpos(int pos){

	int cnt = countNode();

	if(pos <= 0 || pos > cnt){
		printf("Invalid position:\n");
	}else{
		if(pos == 1){
			delfirst();
		}else if(pos == cnt){
			dellast();
		}else{
			belding *temp=head;
			while(pos-2){
				temp=temp->next;
			}
			temp->next=temp->next->next;
			free(temp->next->prev);
			temp->next->prev=temp;
		}
		return 0;
	}

}

void printLL(){

	if(head==NULL){
		printf("Empty Node:");
	}else{
		belding *temp=head;
		while(temp->next != head){
			printf("|%d|->",temp->FloarNo);
			temp=temp->next;
		}
		printf("|%d|",temp->FloarNo);
	}
	printf("\n");
}
void main(){
	char choice;
	do{
		printf("1.addNode\n");
		printf("2.addfirst\n");
		printf("3.addatpos\n");
		printf("4.delfirst\n");
		printf("5.dellast\n");
		printf("6.delatpos\n");
		printf("7.printfLL\n");

		int ch;
		printf("Entre you'r choice:");
		scanf("%d",&ch);

		switch(ch){
			case 1:
				addNode();
				break;
			case 2:
				addfirst();
				break;
			case 3:
				{
				int pos;
				printf("Enter Position:");
				scanf("%d",&pos);
				addatpos(pos);
				}
				break;
			case 4:
				delfirst();
				break;
			case 5:
				dellast();
				break;
			case 6:
				{
				int pos;
				printf("Enter Position:");
				scanf("%d",&pos);
				delatpos(pos);
				}
				break;
			case 7:
				printLL();
				break;
		}
		getchar();
		printf("Do you want continue[y/n]:");
		scanf("%c",&choice);
	}while(choice == 'y' || choice == 'Y');
}

