#include <stdio.h>
#include <stdlib.h>

typedef struct Employee{
	char eName[20];
	int eid;
	struct Employee *next;
}Emp;

Emp *head = NULL;

Emp* creatNode(){
	Emp *newNode = (Emp*) malloc (sizeof(Emp));
	getchar();
	printf("Enter employee name:");
	char ch;
	int i = 0;

	Emp *temp = head;
	while((ch=getchar()) != '\n'){
		(*newNode).eName[i]=ch;
		i++;
	}
	printf("Enter employee id:");
	scanf("%d",&newNode -> eid);

	newNode -> next = NULL;

	return newNode;
}

void addNode(){

	Emp *newNode = creatNode();
	if(head == NULL){
		head = newNode;
	}else{
		Emp *temp = head;
		while(temp->next != NULL){

			temp = temp->next;
		}
		temp->next =newNode;
	}
}

void printLL(){
	Emp *temp = head;
	while(temp != NULL){
		printf("|%s->",temp->eName);
		printf("%d| ",temp->eid);
		temp = temp->next;
	}
	printf("\n");
}

void count(){

	int count = 0;
	Emp *temp = head;

	while(temp != NULL){
		temp= temp->next;
		count++;
	}
	printf("count is %d\n",count);
}

void addfirst(){
	Emp *newNode = creatNode();
	Emp *temp = head;

	if(head == NULL){
		head = newNode;
	}else{
		newNode->next = head;
		head = newNode;
	}
}

void main(){

	int node =0;
	printf("Enter node:");
	scanf("%d",&node);

	for(int i = 1; i <= node; i++){
		addNode();
	}

	printLL();
	addfirst();
	printLL();
	count();

}
