#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Company{

	char Cname[20];
	int ecount;
	float revenue;
};
void main(){

	struct Company *ptr1 = (struct Company*)malloc(sizeof(struct Company));
	strcpy(ptr1 -> Cname, "veritas");
	(*ptr1).ecount = 700;
	ptr1->revenue = 150.00;

	printf("Company name %s\n",ptr1->Cname);
	printf("Emp count %d\n",ptr1->ecount);
	printf("revenue %f\n",ptr1->revenue);
}
