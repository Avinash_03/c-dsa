#include <stdio.h>
#include <stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
}node;

node *head = NULL;
node *createNode(){
	node *newNode =(node*)malloc(sizeof(node));

	printf("Enter data:");
	scanf("%d",&newNode->data);

	newNode->next=NULL;

	return newNode;
}
void addNode(){
	node *newNode=createNode();

	if(head==NULL){
		head=newNode;
		newNode->next=head;
	}else{
		node *temp = head;
		while(temp->next != head){
			temp=temp->next;
		}
		temp->next=newNode;
		newNode->next=head;
	}
}

void addfirst(){
	node *newNode=createNode();

	if(head==NULL){
		head=newNode;
		newNode->next=head;
	}else{
		node *temp = head;

		while(temp->next != head){
			temp = temp->next;
		}
		temp->next=newNode;
		newNode->next=head;
		head=newNode;
	}
}

void addlast(){
	node *newNode=createNode();
	if(head==NULL){
		head=newNode;
		newNode->next=head;
	}else{
		node *temp = head;
		while(temp->next != head){
			temp=temp->next;
		}
		temp->next=newNode;
		newNode->next=head;

	}
}
int count(){

	int count=0;
	node *temp = head;

	if(head==NULL){
		count=0;
		return count;
	}else{
		while(temp->next != head){
			count++;
			temp=temp->next;
		}
		count++;
		return count;
	}
}
int delfirst(){
	if(head==NULL){
		printf("empty Node:\n");
		return -1;
	}else{
		node *temp = head;

		while(temp->next != head){
			temp= temp->next;
		}
		temp->next= head->next;

		temp=head;
		head=head->next;
		free(temp);

		return 0;
	}
}

int dellast(){

	if(head==NULL){
		printf("empty Node:\n");
		return -1;
	}else{
		 node *temp=head;
		 while(temp->next->next != head){
			 temp=temp->next;
		 }
		 free(temp->next);
		 temp->next=head;
		 return 0;
	}

}

int delatpos(int pos){

	int cnt = count();
	if(pos <= 0 || pos > cnt){
		printf("wrong position:\n");
		return -1;
	}else{
		if(pos == 1){
			delfirst();
		}else if(pos == cnt){
			dellast();
		}else{
			node *temp =head;
			node *temp1 =head;

			while(pos -2){
				temp=temp->next;
			}
			temp1=temp->next;
			temp->next=temp1->next;
			free(temp1);
		}
		return 0;
	}
}

int addatpos(int pos){

	int cnt = count();
	if(pos <= 0 || pos > cnt){
		printf("wrong position:\n");
	}else{
		if(pos==1){
			addfirst();
		}else if(pos==cnt){
			addlast();
		}else{
			node *newNode=createNode();
			node *temp = head;
			while(pos-2){
				temp=temp->next;
			}
			newNode->next=temp->next;
			temp->next=newNode;
		}
		return 0;
	}
}
int printLL(){

	if(head==NULL){
		printf("empty Node:\n");
		return -1;
	}
	node *temp= head;
	while(temp->next != head){
		printf("|%d|->",temp->data);
		temp=temp->next;
	}
	printf("|%d|\n",temp->data);

}
void main(){

	char choice = 0;
	do{
		printf("1.addNode\n");
		printf("2.addfirst\n");
		printf("3.addlast\n");
		printf("4.addatpos\n");
		printf("5.delfirst\n");
		printf("6.dellast\n");
		printf("7.delatpos\n");
		printf("8.count\n");
		printf("9.printLL\n");
		int ch=0;
		printf("Enter your choice:");
		scanf("%d",&ch);
		switch(ch){

			case 1:
				addNode();
				break;
			case 2:
				addfirst();
				break;
			case 3:
				addlast();
				break;
			case 4:{
				int pos=0;
				printf("enter position:");
				scanf("%d",&pos);
				addatpos(pos);
			       }
				break;
			case 5:
				delfirst();
				break;
			case 6:
				dellast();
				break;
			case 7:{
				int pos=0;
				printf("enter position:");
				scanf("%d",&pos);
				delatpos(pos);
			       }
				break;
			case 8:{
				int cnt=count();
				printf("%d\n",cnt);
			       }
				break;
			case 9:
				printLL();
				break;
			default :
				printf("invalide input:");
		}
		getchar();
		printf("Do yo want contineu[y/n]:");
		scanf("%c",&choice);
	}while(choice =='y'||choice=='Y');
}
