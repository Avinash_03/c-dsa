//tail recursion
#include <stdio.h>

int tailDemo(int x){
	if(x==1){
		return 1;
	}
	return(--x);
}
void main(){
	tailDemo(5);
}
