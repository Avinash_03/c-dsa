//Non tail recursion
#include <stdio.h>

int Nontail(int x){

	if(x==1){
		return 1;
	}
	return 3+Nontail(--x);

}
void main(){
	int sum=Nontail(5);
	printf("%d\n",sum);
}
