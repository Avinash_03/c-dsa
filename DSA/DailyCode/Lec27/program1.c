#include <stdio.h>

int factorial(int N){

	if(N==1){
		return 1;
	}
	return factorial(N-1)*N;
}
void main(){

	int x=0;
	printf("Enter no:");
	scanf("%d",&x);

	int ret = factorial(x);

	printf("%d is a factorial of %d\n",ret,x);
}
