#include <stdio.h>
#include <stdlib.h>

struct Node{
	 int data;
	 int priority;
	 struct Node *next;
 };
struct Node *head = NULL;

struct Node* createNode(){

	struct Node *newNode = (struct Node*)malloc(sizeof(struct Node));

	printf("Enter data:");
	scanf("%d",&newNode->data);

	printf("Enter priority:");
        scanf("%d",&newNode->priority);

	newNode->next = NULL;

	return newNode;
}

void printLL(){
	if(head==NULL){
		printf("wrong input:\n");
	}else{
		struct Node* temp = head;
		while(temp != NULL){
			printf("|%d|->",temp->data);
			printf("|%d| ",temp->priority);
			temp=temp->next;
		}
		printf("\n");
	}
}

void addlast(struct Node *newNode){

	if(head==NULL){
		head=newNode;
	}else{
		struct Node* temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next=newNode;
	}
}

void addfirst(struct Node* newNode){

	if(head==NULL){
		head=newNode;
	}else{
		struct Node* temp = head;
		newNode->next=head;
		head=newNode;
	}
}

int count(){

	int count=0;
	struct Node* temp = head;

	while(temp != NULL){
		temp=temp->next;
		count++;
	}
	return count;
}

void addatpos(int pos,struct Node *newNode){

	int cnt = count();
	if(pos <= 0 || pos >= cnt+2){
		printf("wrong input:\n");
	}else if(pos == cnt+1){
		addlast(newNode);
	}else if(pos == 1){
		addfirst(newNode);
	}else{
		struct Node *temp1 = head;
		while(pos-2){
			temp1=temp1->next;
			pos--;
		}
		newNode->next=temp1->next;
		temp1->next=newNode;
	}
}

void priority(){

	struct Node *newNode=createNode();

	struct Node *temp=head;
	int cnt=0;

	while(temp != NULL){

		if(temp->priority < newNode->priority)
			break;
		cnt++;
		temp=temp->next;
	}
	addatpos(cnt+1,newNode);
}
void main(){

	char choice=0;
	do{
		printf("1.priority\n");
		printf("2.printLL\n");

		int ch = 0;
		printf("Enter you choice:");
		scanf("%d",&ch);
		switch(ch){

			case 1:
				priority();
				break;

			case 2:
				printLL();
				break;

			default:
				printf("wrong input:\n");
		}
		getchar();
		printf("do you want continue:");
		scanf("%c",&choice);
	}while(choice=='y'||choice=='Y');
}
