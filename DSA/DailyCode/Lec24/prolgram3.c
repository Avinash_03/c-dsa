#include <stdio.h>
#include <stdlib.h>

struct Node{
	int data;
	int priority;
	struct Node* next;
};

int flag=1;
int size;
struct Node *front=NULL;
struct Node *rear=NULL;

int countNode(){
	if(front==NULL){
		flag=0;
		return -1;
	}else{
		int cnt=0;
		flag=1;
		struct Node *temp=front;
		while(temp->next != NULL){
			cnt++;
			temp=temp->next;
		}
		cnt++;

		return cnt;
	}
}
int enqueue(){
	if(countNode()==size)
		return -1;
	struct Node *newNode=(struct Node *)malloc(sizeof(struct Node));

	printf("Enter data:\n");
	scanf("%d",&newNode->data);

	printf("Enter priority:\n");
	scanf("%d",&newNode->priority);

	while(newNode->priority > 5 || newNode->priority < 0){

		getchar();
		printf("Enter Piority between 0 to 5:\n");

		printf("Enter priority:\n");
 	        scanf("%d",&newNode->priority);
	}
	newNode->next=NULL;

	if(front==NULL){
		front=newNode;
		rear=newNode;
	}else{
		if(front->priority > newNode->priority){
			newNode->next=front;
			front=newNode;
		}else if(rear->priority < newNode->priority || rear->priority == newNode->priority){
			rear->next=newNode;
			rear=newNode;
		}else{
			struct Node* temp = front;
			struct Node* temp1 = NULL;
			while( temp-> next != NULL && temp->priority <= newNode->priority) {
				temp1=temp;
				temp=temp->next;
			}
			newNode->next=temp;
			temp1->next=newNode;
		}
	}
}

int dequeue(){

	if(front==NULL){
		flag=0;
		return -1;
	}else{
		flag=1;
		int data=front->data;
		if(front==rear){
			front=NULL;
			rear=NULL;
		}else{
			struct Node *temp = front;
			front=front->next;
			free(temp);
		}

		return data;
	}
}

int printQueue(){
	if(front==NULL){
		flag=0;
		return -1;
	}else{
		flag=1;
		struct Node *temp=front;
		while(temp->next != NULL){
			printf("|%d|->",temp->data);
			printf("|%d|->",temp->priority);
			temp=temp->next;
		}
		printf("|%d|->",temp->data);
		printf("|%d|\n",temp->priority);
	}
}

int frontt(){
	if(front==NULL){
		flag=0;
		return -1;
	}else{
		flag=1;
		return front->data;
	}
}
void main(){
	printf("Enter Queue size:\n");
	scanf("%d",&size);


	char choice;

	do{
		printf("1.enqueue\n");
		printf("2.dequeue\n");
		printf("3.front\n");
		printf("4.printQueue\n");

		int ch;
		printf("Enter choice:\n");
		scanf("%d",&ch);

		switch(ch){
			case 1:
				{
				int ret=enqueue();
				if(ret==-1)
				printf("Queue overflow:\n");
				}
				break;
			case 2:
				{
				int ret=dequeue();
				if(flag==1)
				printf("%d dequeued:\n",ret);
				else
				printf("Empty Queue:\n");
				}
				break;
			case 3:
				{
				int ret=frontt();
				if(flag==1)
				printf("%d is data of front:\n",ret);
				else
				printf("Empty Queue:\n");
				}
				break;
			case 4:
				{
				int ret=printQueue();
				if(flag==0)
				printf("Empty Queue:\n");
				}
				break;
			default:
				printf("Wrong input:\n");
		}
		getchar();
		printf("Do you want continue:\n");
		scanf("%c",&choice);
	}while(choice=='y'||choice=='Y');
}
