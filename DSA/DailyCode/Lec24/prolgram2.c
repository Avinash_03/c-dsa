#include <stdio.h>
#include <stdlib.h>

struct Node{
	 int data;
	 int priority;
	 struct Node *next;
 };
struct Node *head = NULL;

struct Node* createNode(){

	struct Node *newNode = (struct Node*)malloc(sizeof(struct Node));

	printf("Enter data:");
	scanf("%d",&newNode->data);
	
	printf("Enter priority:");
	scanf("%d",&newNode->priority);

	newNode->next = NULL;

	return newNode;
}

void printLL(){
	if(head==NULL){
		printf("wrong input:\n");
	}else{
		struct Node* temp = head;
		while(temp != NULL){
			printf("|%d|--> ",temp->data);
			printf("|%d| ",temp->priority);
			temp=temp->next;
		}
		printf("\n");
	}
}

void addNode(){

	struct Node* newNode = createNode();

	if(head==NULL){
		head=newNode;
	}else{
	}
}

void addfirst(){

	struct Node* newNode= createNode();
	if(head==NULL){
		head=newNode;
	}else{
		struct Node* temp = head;
		newNode->next=head;
		head=newNode;
	}
}

void addlast(){
	addNode();
}

int count(){

	int count=0;
	struct Node* temp = head;

	while(temp != NULL){
		temp=temp->next;
		count++;
	}
	printf("Node count is %d\n",count);
	return count;
}
void addatpos(int pos){

	int cnt = count();
	if(pos <= 0 || pos >= cnt+2){
		printf("wrong input:\n");
	}else if(pos == cnt){
		addlast();
	}else if(pos == 1){
		addfirst();
	}else{
		struct Node *newNode = createNode();
		struct Node *temp = head;
		while(pos-2){
			temp=temp->next;
			pos--;
		}
		newNode->next=temp->next;
		temp->next=newNode;
	}
}

void delfirst(){

	if(head==NULL){
		printf("wrong input:\n");
	}else if(head->next==NULL){
		free(head);
		head=NULL;
	}else{
		struct Node* temp = head;
		head=temp->next;
		free(temp);
	}
}
void dellast(){
	if(head==NULL){
		printf("wrong input:\n");
	}else if(head->next==NULL){
		free(head);
		head=NULL;
	}else{
		struct Node *temp = head;
		while(temp->next->next != NULL){
			temp=temp->next;
		}
		free(temp->next);
		temp->next=NULL;
	}
}
void delatpos(int pos){
	if(head==NULL){
		printf("wrong input:\n");
	}else if(head->next==NULL){
		free(head);
		head=NULL;
	}else{
		int cnt=count();
		if(pos<=0 || pos>cnt){
			printf("wrong input:\n");
		}else if(pos==1){
			delfirst();
		}else if(pos==cnt){
			dellast();
		}else{
			struct Node *temp1=head;
			struct Node *temp =head;
			while(pos-2){
				temp=temp->next;
				temp1=temp1->next;
				pos--;
			}
			temp1=temp1->next;
			temp->next=temp->next->next;
			free(temp1);
		}
	}
}
void main(){

	char choice=0;
	do{
		printf("1.addNode\n");
		printf("2.printLL\n");
		printf("3.addfirst\n");
		printf("4.addlast\n");
		printf("5.addatpos\n");
		printf("6.count\n");
		printf("7.delfirst\n");
		printf("8.dellast\n");
		printf("9.delatpos\n");


		int ch = 0;
		printf("Enter you choice:");
		scanf("%d",&ch);
		switch(ch){

			case 1:
				addNode();
				break;
			case 2:
				printLL();
				break;
			case 3:
				addfirst();
				break;
			case 4:
				addlast();
				break;
			case 5:
				{
				int pos=0;
				printf("Enter position:");
				scanf("%d",&pos);
				addatpos(pos);
				}
				break;
			case 6:
				count();
				break;
			case 7:
				delfirst();
				break;
			case 8:
				dellast();
				break;
			case 9:
				{
				int pos=0;
				printf("Enter position:");
				scanf("%d",&pos);
				delatpos(pos);
				}
				break;
			default:
				printf("wrong input:\n");
		}
		getchar();
		printf("do you want continue:");
		scanf("%c",&choice);
	}while(choice=='y'||choice=='Y');
}
