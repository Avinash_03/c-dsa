#include <stdio.h>
#include <stdbool.h>

int top=-1;
int size;
int flag=1;

bool isfull(){
	if(top==size-1){
		return true;
	}else{
		return false;
	}
}
bool isempty(){
	if(top != size-1){
		return true;
	}else{
		return false;
	}
}
int push(int *stack){
	if(isfull()){
		return -1;
	}else{
		int data;
                printf("Enter Data:\n");
                scanf("%d",&data);
		top++;
		stack[top]=data;
		return 0;
	}
}
int pop(int *stack){
	if(isempty()){
		flag=0;
		return -1;
	}else{
		flag=1;
		int ret = stack[top];
		top--;
		return ret;
	}
}
int peek(int *stack){
	if(isempty()){
		flag=0;
		return -1;
	}else{	
		flag=1;
		int data=stack[top];
		return data;
	}
}
void main(){
	printf("Enter stack size:\n");
	scanf("%d",&size);

	int stack[size];

	char choice;
	do{
		printf("1.push\n");
		printf("2.pop\n");
		printf("3.peek\n");

		int ch=0;
		printf("Enter your choice:\n");
		scanf("%d",&ch);

		switch(ch){
			case 1:
				{
				int ret=push(stack);
				if(ret==-1)
				printf("stack overflow:\n");
				}
				break;
			case 2:
				{
				int ret=pop(stack);
				if(flag==1)
				printf("%d popped value:\n",ret);
				else
				printf("stack underflow:\n");
				}
				break;
			case 3:
				{
				int data=peek(stack);
				if(flag==1)
				printf("%d Top value:\n",data);
				else
				printf("stack underflow:\n");
				}
				break;
			default:
				printf("Invalide input:\n");
		}
		getchar();
		printf("Do you want continue:\n");
		scanf("%c",&choice);
	}while(choice=='y' || choice=='Y');
}
