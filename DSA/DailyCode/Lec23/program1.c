//implementing queue using singlY LL:
#include <stdio.h>
#include <stdlib.h>

struct Node{
	int data;
	struct Node* next;
};

int flag=1;
int size;
struct Node *front=NULL;
struct Node *rear=NULL;

int countNode(){
	if(front==NULL){
		flag=0;
		return -1;
	}else{
		int cnt=0;
		flag=1;
		struct Node *temp=front;
		while(temp->next != front){
			cnt++;
			temp=temp->next;
		}
		cnt++;

		return cnt;
	}
}
int enqueue(){
	if(countNode()==size)
		return -1;
	struct Node *newNode=(struct Node *)malloc(sizeof(struct Node));

	printf("Enter data:\n");
	scanf("%d",&newNode->data);

	newNode->next=NULL;

	if(front==NULL){
		front=newNode;
		rear=newNode;
		newNode->next=front;
	}else{
		rear->next=newNode;
		newNode->next=front;
		rear=newNode;
	}
}

int dequeue(){

	if(front==NULL){
		flag=0;
		return -1;
	}else{
		flag=1;
		int data=front->data;
		if(front==rear){
			front=NULL;
			rear=NULL;
		}else{
			front=front->next;
			free(rear->next);
			rear->next=front;
		}

		return data;
	}
}

int printQueue(){
	if(front==NULL){
		flag=0;
		return -1;
	}else{
		flag=1;
		struct Node *temp=front;
		while(temp->next != front){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
		printf("|%d|\n",temp->data);
	}
}

int frontt(){
	if(front==NULL){
		flag=0;
		return -1;
	}else{
		flag=1;
		return front->data;
	}
}
void main(){
	printf("Enter Queue size:\n");
	scanf("%d",&size);


	char choice;

	do{
		printf("1.enqueue\n");
		printf("2.dequeue\n");
		printf("3.front\n");
		printf("4.printQueue\n");

		int ch;
		printf("Enter choice:\n");
		scanf("%d",&ch);

		switch(ch){
			case 1:
				{
				int ret=enqueue();
				if(ret==-1)
				printf("Queue overflow:\n");
				}
				break;
			case 2:
				{
				int ret=dequeue();
				if(flag==1)
				printf("%d dequeued:\n",ret);
				else
				printf("Empty Queue:\n");
				}
				break;
			case 3:
				{
				int ret=frontt();
				if(flag==1)
				printf("%d is data of front:\n",ret);
				else
				printf("Empty Queue:\n");
				}
				break;
			case 4:
				{
				int ret=printQueue();
				if(flag==0)
				printf("Empty Queue:\n");
				}
				break;
			default:
				printf("Wrong input:\n");
		}
		getchar();
		printf("Do you want continue:\n");
		scanf("%c",&choice);
	}while(choice=='y'||choice=='Y');
}
