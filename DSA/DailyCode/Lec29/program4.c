//recursion code of Count Step
#include <stdio.h>

int CountStep(int N,int cnt){
	if(N==0)
		return cnt;
	if(N%2==0)
		N=N/2;
	else
		N=N-1;	
	return CountStep(N,cnt+1);
}
void main(){
	int num=0,cnt=0;
	printf("Enter Number:\n");
	scanf("%d",&num);

	int ret = CountStep(num,cnt);
	printf("%d is a count of Steps:\n",ret);
}
