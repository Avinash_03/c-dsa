//simple code of Count zero
#include <stdio.h>

int CountZero(int N){
	int cnt=0;
	int last=0;

	while(N){
		if(N==0)
			return 0+cnt;
		last=N%10;
		if(last==0)
			cnt++;
		N=N/10;
	}
	return cnt;
}
void main(){
	int num=0;
	printf("Enter Number:\n");
	scanf("%d",&num);

	int ret = CountZero(num);
	printf("%d is a count of Zeros:\n",ret);
}
