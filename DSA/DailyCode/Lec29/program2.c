//recursion code of Count zero
#include <stdio.h>

int CountZero(int N,int cnt){
	int last=0;
		if(N==0)
			return 0;
		last=N%10;
		if(last==0)
			cnt++;
		if(N>=0 && N<=9)
			return cnt;
	return CountZero(N/10,cnt);
}
void main(){
	int num=0;
	int cnt=0;
	printf("Enter Number:\n");
	scanf("%d",&num);

	int ret = CountZero(num,cnt);
	printf("%d is a count of Zeros:\n",ret);
}
