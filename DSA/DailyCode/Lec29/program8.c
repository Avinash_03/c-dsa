//simple code of even array (if more than 2 even No)
#include <stdio.h>
#include <stdbool.h>

bool EvenArr(int *arr,int size,int cnt){
	if(size==0)
		return false;

	if(arr[size-1]%2==0)
		cnt++;
	if(cnt>=2)
		return true;
	return EvenArr(arr,size-1,cnt);
}
void main(){
	int size,cnt=0;
	printf("Enter Array size:");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array element:\n");
	for(int i =0; i<=size-1;i++){
		scanf("%d",&arr[i]);
	}

	bool ret=EvenArr(arr,size,cnt);
	if(ret)
		printf("Enter array valid:\n");
	else
		printf("Enter array invalid:\n");
}
