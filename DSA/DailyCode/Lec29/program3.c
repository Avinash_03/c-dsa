//simple code of Count Step
#include <stdio.h>

int CountStep(int N){
	int cnt=0;

	while(N){
		if(N%2==0)
			N=N/2;
		else
			N=N-1;
		cnt++;
	}
	return cnt;
}
void main(){
	int num=0;
	printf("Enter Number:\n");
	scanf("%d",&num);

	int ret = CountStep(num);
	printf("%d is a count of Steps:\n",ret);
}
