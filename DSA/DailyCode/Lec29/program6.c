//recursion code of reverse NO
#include <stdio.h>

int Rev(int N,int rev){

	if(N==0)
		return rev;
	int last=0;
	last=N%10;
	return Rev(N/10,rev*10+last);
}
void main(){
	int num=0,rev=0;
	printf("Enter Number:\n");
	scanf("%d",&num);

	int ret = Rev(num,rev);
	printf("%d is a Reverse No:\n",ret);
}
