//recursion code of even array (if more than 2 even No)
#include <stdio.h>
#include <stdbool.h>

bool EvenArr(int *arr,int size){
	int cnt=0;
	while(size){
		if(arr[size-1]%2==0)
			cnt++;
		size--;
	}
	if(cnt>=2)
		return true;
	else
		return false;
}
void main(){
	int size;
	printf("Enter Array size:");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array element:\n");
	for(int i =0; i<=size-1;i++){
		scanf("%d",&arr[i]);
	}

	bool ret=EvenArr(arr,size);
	if(ret)
		printf("Enter array valid:\n");
	else
		printf("Enter array invalid:\n");
}
