//sort array or not using recursion
#include <stdio.h>
#include <stdbool.h>

bool SortArr(int *arr,int size){
	if(size==0)
		return true;
	if(arr[size-1] > arr[size])
		return false;
	return SortArr(arr,size-1);
}
void main(){
	int size;
	printf("Enter Array size:");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array element:\n");
	for(int i =0; i<=size-1;i++){
		scanf("%d",&arr[i]);
	}

	bool ret=SortArr(arr,size-1);
	if(ret)
		printf("Enter array valid:\n");
	else
		printf("Enter array invalid:\n");
}
