// N compare two str using recursion
#include <stdio.h>
#include <stdbool.h>

bool SortArr(char *str1,char *str2,int size){
	if(size==0)
		return true;
	if(str1[size-1] != str2[size-1])
		return false;
	return SortArr(str1,str2,size-1);
}
int Mystrlen(char *str){
	int cnt=0;
	char *ch=&str[0];
	while(*ch!='\0'){
		cnt++;
		ch++;
	}
	return cnt;
}
void main(){

	int N=0;

	char arr1[20];
	char arr2[20];

	printf("Enter First String:\n");
	scanf("%s",arr1);

	printf("Enter Second String:\n");
	scanf("%s",arr2);

	printf("Enter Size to compare String:\n");
	scanf("%d",&N);

	int size1= Mystrlen(arr1);
	int size2= Mystrlen(arr2);

	if(size1 >= N &&size2 >= N ){
		bool ret=SortArr(arr1,arr2,N);
		if(ret)
			printf("string matched:\n");
		else
			printf("string not matched:\n");
	}else
			printf("String size not match:\n");

}
