// using Linear search fined last occurance
#include <stdio.h>

int IfPresent(int *arr,int size,int num){
	int store=-1;
	for(int i=0; i<= size-1; i++){
		if(arr[i]==num){
			store=i;
		}
	}
	return store;
}

void main(){
	int size,num;
	printf("Enter array size:");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array:\n");
	for(int i =0; i<size; i++){
		scanf("%d",&arr[i]);
	}
	printf("Enter Number to cheack:");
	scanf("%d",&num);

	int ret = IfPresent(arr,size,num);

	if(ret>=0)
		printf("%d is present at %d index\n",num,ret);
	else
		printf("%d is not present \n",num);

}
