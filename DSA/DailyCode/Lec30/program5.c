// interpolation Search
#include <stdio.h>

int IfPresent(int *arr,int size,int num){
	int start=0,end=size-1,index=-1;
	index=num-arr[start]/arr[end]-arr[start]*(end-start);
	return index;
}

void main(){
	int size,num;
	printf("Enter array size:");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array:\n");
	for(int i =0; i<size; i++){
		scanf("%d",&arr[i]);
	}
	printf("Enter Number to cheack:");
	scanf("%d",&num);

	int ret = IfPresent(arr,size,num);

	if(ret>=0)
		printf("%d is present at %d index\n",num,ret);
	else
		printf("%d is not present \n",num);

}
