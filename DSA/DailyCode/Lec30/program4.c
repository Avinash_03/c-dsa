// Binary Search
#include <stdio.h>

int IfPresent(int *arr,int size,int num){
	int start=0,end=size-1,mid;
	while(start <= end){
		mid=(start+end)/2;
		if(arr[mid]==num)
			return mid;
		if(arr[mid]<num)
			start=mid+1;
		if(arr[mid]>num)
			end=mid-1;
	}
	return -1;
}

void main(){
	int size,num;
	printf("Enter array size:");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array:\n");
	for(int i =0; i<size; i++){
		scanf("%d",&arr[i]);
	}
	printf("Enter Number to cheack:");
	scanf("%d",&num);

	int ret = IfPresent(arr,size,num);

	if(ret>=0)
		printf("%d is present at %d index\n",num,ret);
	else
		printf("%d is not present \n",num);

}
