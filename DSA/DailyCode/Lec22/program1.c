//implementing Queque in linked List:
#include <stdio.h>
#include <stdlib.h>
struct Node{
	int data;
	struct Node *next;
};

int flag=0;
struct Node *front=NULL;
struct Node *rear=NULL;

void enqueue(){
	struct Node *newNode=(struct Node *)malloc(sizeof(struct Node));

	if(newNode==NULL){
		exit(0);
	}
	printf("Enter data:\n");
	scanf("%d",&newNode->data);

	newNode->next=NULL;

	if(front==NULL){
		front=newNode;
		rear=newNode;
	}else{
		rear->next=newNode;
		rear=newNode;
	}
}

int dequeue(){
	if(front==NULL){
		flag=0;
		return -1;
	}else{
		flag=1;

		struct Node *temp=front;
		int ret=temp->data;
		front=front->next;
		free(temp);

		return ret;
	}
}

int frontt(){
	if(front==NULL){
		flag=0;
		return -1;
	}else{
		flag=1;
		return front->data;
	}
}

int printQueue(){
	if(front==NULL){
		flag=0;
		return -1;
	}else{
		flag=1;
		struct Node *temp=front;
		while(temp != NULL){
			printf("%d ",temp->data);
			temp=temp->next;
		}
		printf("\n");
		return 0;
	}
}
void main(){
	char choice;

	do{
		printf("1.enqueue\n");
		printf("2.dequeue\n");
		printf("3.front\n");
		printf("4.printQueue\n");

		int ch;
		printf("Enter choice:\n");
		scanf("%d",&ch);

		switch(ch){
			case 1:
				enqueue();
				break;
			case 2:
				{
				int ret=dequeue();
				if(flag==1)
				printf("%d dequeued:\n",ret);
				else
				printf("Empty Queue:\n");
				}
				break;
			case 3:
				{
				int ret=frontt();
				if(flag==1)
				printf("%d is data of front:\n",ret);
				else
				printf("Empty Queue:\n");
				}
				break;
			case 4:
				{
				int ret=printQueue();
				if(flag==0)
				printf("Empty Queue:\n");
				}
				break;
			default:
				printf("Wrong input:\n");
		}
		getchar();
		printf("Do you want continue:\n");
		scanf("%c",&choice);
	}while(choice=='y'||choice=='Y');
}
