#include <stdio.h>

int diff(int *arr,int size){

	int diff1=0;
	int odd=0;
	int even=0;

	for(int i= 0;i<size;i++){
		if(arr[i]%2==0 && arr[i]>even){
			even=arr[i];
		}else{
			if(arr[i]<odd)
				odd=arr[i];
		}
	}
	diff1=even-odd;

	return diff1;
}
void main(){
	int size=0;
	printf("Enter array size:\n");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array element:\n");
	for(int i = 0; i<size;i++){
		scanf("%d",&arr[i]);
	}
	int diff1=diff(arr,size);

	printf(" %d is a diff of odd and even no:\n",diff1);
}
