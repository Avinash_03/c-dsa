//prime no
#include <stdio.h>
#include <math.h>
#include <stdbool.h>

bool prime(int x){
	int cnt=0;
	for(int i=1; i<=x/2;i++){
		if(x%i==0)
			cnt++;
	}
	if(cnt<=2)
		return true;
	else
		return false;
}
void main(){
	int num=0;
	printf("Enter number:\n");
	scanf("%d",&num);

	bool x=prime(num);
	if(x)
		printf("%d is a prime number:\n",num);
	else
		printf("%d is not prime number:\n",num);
}
