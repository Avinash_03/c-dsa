// two element addition of array is same as targate if same then count of pairs.
#include <stdio.h>

int add(int *arr,int size,int targate){

	int n = size-1,cnt=0;
	for(int i = 0; i<=size/2-1;i++){
		if(arr[i]+arr[n]==targate){
			cnt++;
		}
		n--;
	}
	return cnt;
}

void main(){
	int size=0;
	printf("Enter array size:\n");
	scanf("%d",&size);
	int arr[size];

	printf("Enter array element:\n");
	for(int i = 0; i < size; i++){
		scanf("%d",&arr[i]);
	}

	int targate=0;
	printf("Enter targete:\n");
	scanf("%d",&targate);
	
	int pairs = add(arr,size,targate);

	printf("%d no of addition match",pairs);
}
