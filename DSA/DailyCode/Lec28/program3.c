//fibonacci without recoursion
#include <stdio.h>

int fibo(int N){
	int a=0,b=1,ans=0;
	while(N != 1){
		ans=a+b;
		a=b;
		b=ans;
		N--;
	}
	return ans;
}

void main(){
	int num=0;
	printf("Enter No:\n");
	scanf("%d",&num);

	int ret = fibo(num);
	printf("%d\n",ret);
}
