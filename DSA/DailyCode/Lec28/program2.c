//fibonacci series
#include <stdio.h>

int fibo(int N){
	if(N==1){
		return 1;
	}
	if(N==0){
		return 0;
	}
	return fibo(N-2)+fibo(N-1);
}
void main(){
	int num=0;
	printf("Enter Number:\n");
	scanf("%d",&num);
	int ret=fibo(num);
	printf("%d\n",ret);
}
