//sum of array element using recursion

#include <stdio.h>

int sumArray(int *arr,int size){

	if(size == 1){
		return arr[0];
	}
	return sumArray(arr,size-1)+arr[size-1];
}
void main(){
	 int size;
        printf("Enter Array size:\n");
        scanf("%d",&size);

        int arr[size];
        int N = size-1;
        printf("Enter Array Element:\n");
        while(N != -1){
                scanf("%d",&arr[N]);
                N--;
        }
        int ret = sumArray(arr,size);
        printf("%d is a sum of array element:\n",ret);

}
