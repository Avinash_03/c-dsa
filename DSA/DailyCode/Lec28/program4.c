//addition of array element
#include <stdio.h>

int sumArray(int *arr,int size){

	int sum=0;
	while(size){
		sum+=arr[size-1];
		size--;
	}
	return sum;
}
void main(){
	int size;
	printf("Enter Array size:\n");
	scanf("%d",&size);

	int arr[size];
	int N = size-1;
	printf("Enter Array Element:\n");
	while(N != -1){
		scanf("%d",&arr[N]);
		N--;
	}
	int ret = sumArray(arr,size);
	printf("%d is a sum of array element:\n",ret);
}
