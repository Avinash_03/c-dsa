#include <stdio.h>
#include <stdlib.h>

struct TreeNode{
	int data;
	struct TreeNode *left;
	struct TreeNode *right;
};

struct TreeNode* createNode(int level){
	char ch;
	struct TreeNode *newNode=(struct TreeNode*)malloc(sizeof(struct TreeNode));
	printf("Enter data Node ");
	scanf("%d",&newNode->data);
	
	level=level+1;
	printf("\nwant to add node at left for level %d (y/n) :",level);
	scanf(" %c",&ch);

	if(ch=='y' || ch=='Y'){
		newNode->left=createNode(level);
	}else{
		newNode->left=NULL;
	}

	printf("\nwant to add node at right for level %d (y/n) :",level);
	scanf(" %c",&ch);

	if(ch=='y' || ch=='Y'){
		newNode->right=createNode(level);
	}else{
		newNode->right=NULL;
	} 
	return newNode; 
}

void preOrder(struct TreeNode *root){

	if(root==NULL){
		return;
	}
	printf("%d ",root->data);
	preOrder(root->left);
	preOrder(root->right);
}
void inOrder(struct TreeNode *root){

	if(root==NULL){
		return;
	}
	inOrder(root->left);
	printf("%d ",root->data);
	inOrder(root->right);
}
void postOrder(struct TreeNode *root){

	if(root==NULL){
		return;
	}
	postOrder(root->left);
	postOrder(root->right);
	printf("%d ",root->data);
}
void printNode(struct TreeNode *root){
	char ch;
	do{
		printf("1.preOrder\n");
		printf("2.inOrder\n");
		printf("3.postOrder\n");

		int x;
		printf("Enter you choice: ");
		scanf("%d",&x);
		switch(x){
			case 1:
				preOrder(root);
				break;
			case 2:
				inOrder(root);
				break;
			case 3:
				postOrder(root);
				break;
			default:
				printf("Invalid input:\n");
		}
		printf("\nDo you want continue ? ");
		scanf(" %c",&ch);
	}while(ch=='Y'|| ch=='y');
}
void main(){
	char ch;
	struct TreeNode *root=(struct TreeNode*)malloc(sizeof(struct TreeNode));
	printf("Enter root Node ");
	scanf("%d",&root->data);

	printf("\t\t\tthe node rooted with %d",root->data);

	printf("\nwant to add node at left for rootNode (y/n) :");
	scanf(" %c",&ch);

	if(ch=='y' || ch=='Y'){
		root->left=createNode(0);
	}else{
		root->left=NULL;
	}

	printf("\nwant to add node at right for rootNode (y/n) :");
	scanf(" %c",&ch);

	if(ch=='y' || ch=='Y'){
		root->right=createNode(0);
	}else{
		root->right=NULL;
	}

	printNode(root);

}
