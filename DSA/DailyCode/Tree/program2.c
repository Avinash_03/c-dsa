//itrative of inOrder
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
struct TreeNode{
	int data;
	struct TreeNode *left;
	struct TreeNode *right;
};

struct Stackframe{
	struct TreeNode* btTree;
	struct Stackframe* next;
};
struct Stackframe* Top=NULL;
bool isEmpty(){
	if(Top==NULL){
		return true;
	}else{
		return false;
	}
}

void push(struct TreeNode* temp){
	struct Stackframe *newNode=(struct Stackframe*)malloc(sizeof(struct Stackframe));
	newNode->next=Top;
	newNode->btTree=temp;
	Top=newNode;
}

struct TreeNode* pop(){
	if(isEmpty()){
		printf("Empty Stack:");
		return NULL;
	}else{
		struct Stackframe* temp=Top;
		struct TreeNode* temp1=Top->btTree;
		Top=Top->next;
		free(temp);
		return temp1;
	}
}
void itrativeInOrder(struct TreeNode* root){
	
	struct TreeNode* temp = root;

	while(!isEmpty() || temp!=NULL){
		if(temp != NULL){
			push(temp);
			temp=temp->left;
		}else{
			struct TreeNode* item = pop();
			printf("%d\t",item->data);
			temp = item->right;
		}
	}
}
struct TreeNode* createNode(int level){
	char ch;
	struct TreeNode *newNode=(struct TreeNode*)malloc(sizeof(struct TreeNode));
	printf("Enter Data : ");
	scanf("%d",&newNode->data);
	level=level+1;
	printf("\nDo you want to add subtree in left side at level %d",level);
	scanf(" %c",&ch);
	if(ch=='Y'||ch=='y'){
		newNode->left=createNode(level);
	}else{
		newNode->left=NULL;
	}
	printf("\nDo you want to add subtree in right side at level %d",level);
	scanf(" %c",&ch);
	if(ch=='Y'||ch=='y'){
		newNode->right=createNode(level);
	}else{
		newNode->right=NULL;
	}
	return newNode;
}
void main(){
	char ch;
	struct TreeNode *root=(struct TreeNode*)malloc(sizeof(struct TreeNode));
	printf("Enter Data : ");
	scanf("%d",&root->data);

	printf("\t\t\trooted with %d\n",root->data);
	
	printf("Do you want to add subtree in left side");
	scanf(" %c",&ch);
	if(ch=='Y'||ch=='y'){
		root->left=createNode(0);
	}else{
		root->left=NULL;
	}
	printf("\nDo you want to add subtree in right side");
	scanf(" %c",&ch);
	if(ch=='Y'||ch=='y'){
		root->right=createNode(0);
	}else{
		root->right=NULL;
	}
	itrativeInOrder(root);
}
