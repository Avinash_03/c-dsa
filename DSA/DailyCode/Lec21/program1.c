//implementing Queue using array:

#include <stdio.h>
int size=0;
int front =-1;
int rear=-1;
int flag=1;


int enqueue(int *arr){
	if(rear >= size-1){
		return -1;
	}else{
		if(front==-1)
			front++;
		rear++;
		printf("Enter data:\n");
		scanf("%d",&arr[rear]);
	}
}
int dequeue(int *arr){
	if(front==-1){
		flag=0;
		return -1;
	}else{
		flag=1;
		int data=arr[front];
		front++;
		if(front >= rear){
			front=-1;
			rear=-1;
		}
		return data;
	}
}
int front1(int *arr){
	if(front==-1){
		flag=0;
		return -1;
	}else{
		flag=1;
		return arr[front];
	}
}
int display(int *arr){
	if(rear==-1){
		flag=0;
		return -1;
	}else{
		flag=1;
		for(int i = rear; i>=0;i--)
			printf("%d ",arr[i]);
		printf("\n");
		return 0;
	}
}
void main(){

	printf("Enter array size:\n");
	scanf("%d",&size);

	int arr[size];
	char choice;

	do{
		printf("1.enqueue\n");
		printf("2.dequeue\n");
		printf("3.front\n");
		printf("4.display\n");

		int ch;
		printf("Enter choice:\n");
		scanf("%d",&ch);

		switch(ch){
			
			case 1:
				{
				int data=enqueue(arr);
				if(data==-1)
				printf("Queue overflow:\n");
				}
				break;
			case 2:
				{
				int ret=dequeue(arr);
				if(flag==1)
				printf("%d dequeued\n",ret);
			       	else
				printf("Queue underflow:\n");		
			        }
				break;
			case 3:
				{
				int ret=front1(arr);
				if(flag==1)
				printf("%d \n",ret);
				else
				printf("Queue underflow:\n");		
				}
				break;
			case 4:
				{
				int ret=display(arr);
				if(flag==0)
				printf("Queue underflow:\n");		
				}
				break;
		}
		getchar();
		printf("Do you want continue:\n");
		scanf("%c",&choice);
	}while(choice=='y'||choice=='Y');
}
